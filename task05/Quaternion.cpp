/**
 *  @file Quaternion.cpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "Quaternion.hpp"

namespace asteroids
{

Quaternion::Quaternion()
{
	// Default Quaternion
	x = 1.0;
	y = 0.0;
	z = 0.0;
	w = 0.0;
}

Quaternion::~Quaternion()
{
	// Do nothing
}

Quaternion::Quaternion(const Vector& vec, float angle)
{
	// Calculate the quaternion
	fromAxis(vec, angle);
}

Quaternion::Quaternion(float* vec, float _w)
{
	// Set the values
	x = vec[0];
	y = vec[1];
	z = vec[2];
	w = _w;
}

void Quaternion::operator=(const Quaternion q2)
{
	x = q2.getX();
	y = q2.getY();
	z = q2.getZ();
	w = q2.getW();
}


void Quaternion::fromAxis(const Vector& axis, float angle)
{
	float sinAngle;
	angle *= 0.5f;

	// Create a copy of the given vector and normalize the new vector
	Vector vn(axis.x, axis.y, axis.z);
	vn.normalize();

	// Calculate the sinus of the given angle
	sinAngle = sin(angle);

	// Get the quaternion
	x = (vn.x * sinAngle);
	y = (vn.y * sinAngle);
	z = (vn.z * sinAngle);
	w = cos(angle);
}

Quaternion Quaternion::operator*( const Quaternion& q ) const
{
    float w;
    Vector v1 = Vector( this->x, this->y, this->z );
    Vector v2 = Vector( q.x, q.y, q.z );

    w = this->w * q.w - v1 * v2;
    Vector v3 = v2 * this->w + v1 * q.w - v1.crossProduct( v2 );

    return Quaternion( v3.x, v3.y, v3.z, w );
}

Vector Quaternion::operator*( const Vector& v ) const
{
    Vector v1 = v;
    Quaternion v1q, midOp, result;
    v1.normalize();
    v1q = Quaternion( v1.x, v1.y, v1.z, 0.0f );
    midOp = v1q * getConjugate();
    result = *this * midOp;
    return Vector( result.x, result.y, result.z );
}

}
