/**
 *  @file Quaternion.hpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */



#ifndef __QUATERNION_HPP__
#define __QUATERNION_HPP__

#include "Vector.hpp"

#include <iostream>
#include <cmath>

const float TOLERANCE = 0.00001;

namespace asteroids
{

/**
 * @brief   Quaternion representation for OpenGL. Based on:
 *          http://gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation
 *
 */
class Quaternion {

public:

	/**
	 * @brief   Construcs a default quaternion object
	 */
	Quaternion();

	/**
	 * @brief   Destructor
	 */
	~Quaternion();

	/**
	 * @brief   Construcs (with fromAxis()) a quaternion with a given Vector and an angle
	 * @param vec vector
	 * @param angle angle
	 */
	Quaternion(const Vector& , float angle);

	/**
	 * @brief   Constructs a quaternion with three given values and an angle
	 * @param x x-value
	 * @param y y-value
	 * @param z z-value
	 * @param w angle
	 */
	Quaternion(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

	/**
	 * @brief   Constructs a quaternion with a given float-pointer and an angle
	 * @param vec vector(pointer)
	 * @param w angle
	 */
	Quaternion(float* vec, float w);

	void operator=(const Quaternion q2);

    /**
     * @brief       multiplies two quaternions. Returns the product quaternion
     *
     * @param q     the multiplying quaternion
     * @return Quaternion   the product quaternion
     */
    Quaternion operator*( const Quaternion& q ) const;

	/**
	 * @brief multiplies a quaternion with a vector.
	 * 
	 * @param v the multiplying vector
	 * @return Vector the product vector
	 */

    Vector operator*( const Vector& v ) const;

	/**
	 * @brief   Calculates a quaternion with a given vector and an angle
	 * @param axis vector
	 * @param angle angle
	 */
	void fromAxis(const Vector& axis, float angle);

	float getX() const { return x; }
	float getY() const { return y; }
	float getZ() const { return z; }
	float getW() const { return w; }

    /**
     * COPY-PASTED from the Website
     *
     * http://moddb.wikia.com/wiki/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation
     *
     * @brief normalises the quaternion's values to avoid errors
     *
     */
    void normalise() {
        // Don't normalize if we don't have to
        float mag2 = w * w + x * x + y * y + z * z;
        if (fabs(mag2 - 1.0f) > TOLERANCE)
        {
            float mag = sqrt(mag2);
            w /= mag;
            x /= mag;
            y /= mag;
            z /= mag;
        }
    }

    /**
     * @brief returns the complex conjugate of this quaternion
     *
     * copy-pasted like normalise()
     *
     * @return Quaternion the complex conjugate of this quaternion
     */
    Quaternion getConjugate() const { return Quaternion( -this->x, -this->y, -this->z, this->w ); }

private:

	/**
	 * @brief   Value of angle, x, y and z
	 */
	float x, y, z, w;
};

} // namespace asteroids

#endif
