/*
 * Matrix.hpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include <iostream>
#include <fstream>
#include <iomanip>

#include "Vector.hpp"

#define _USE_MATH_DEFINES
#include <cmath>

#ifndef M_PI
#define M_PI 3.141592654
#endif
using namespace std;

namespace asteroids{

/**
 * @brief	A 4x4 matrix class implementation for use with the provided
 * 			Vector types.
 */

class Matrix {
public:

	/**
	 * @brief 	Default constructor. Initializes a identity matrix.
	 */
    Matrix();

    /**
     * @brief default copy-constructor
     *
     * @param mat Matrix to be copied
     */
    Matrix( const Matrix& mat ); //Dangerous, but [] wouldn't work otherwise

	/**
	 * @brief	Constructs a matrix from given axis and angle. Tries to
	 * 			avoid a gimbal lock.
	 */
    Matrix(Vector axis, float angle);

    /**
     * @brief Construct scaling-matrix based on a vector
     *
     * @param scal  vector whichs values in x, y, z will create a scaling matrix that scales in
     *              those values respectively
     */
    Matrix( Vector scal ) : Matrix() { m[0] = scal.x; m[5] = scal.y; m[10] = scal.z; m[15] = 1.0; }

    /**
     * @brief Construct a new Matrix based on the multiplication
     * of two Vectors, one of which (v2) is transposed
     *
     * @param v1    first Vector
     * @param v2t   second Vector (will be transposed)
     */
    Matrix( const Vector& v1, const Vector& v2t );
////////////// ROW CLASS ///////////////////////////////////////////
    /**
     * @brief small class representing a reference to a
     * block of memory inside the array of values of our
     * matrix, so we can access it as a two dimensional
     * array. (Because, what Thomas showed, didn't work at
     * all in a sure way)
     */
    class Row
    {
    public:

        Row( float* index ) { rIndex = index; }

        float& operator[]( const int& index ) const
        {
            if( index < 0 || index > 3 )
            {
                cout << "Error: index column out of bounds." << endl;
                return rIndex[0];
            }
            return rIndex[index];
        }

    private:
        float* rIndex;
    };

    /**
     * @brief get a reference to the row posted.
     * Shows error if Index out of bounds, and gives ref.
     * to the first row
     *
     * @param i     the index to be accessed
     * @return Row  the row inside the matrix to be accessed
     */
    Row operator[]( const int& i ) const;

    Row operator[]( const int& i );

////////////// ROW CLASS ///////////////////////////////////////////
    /**
     * @brief Multiplies -this- Matrix times another matrix
     *
     * @param m2        The multiplying matrix
     * @return Matrix   the product matrix
     */
   /*  float* operator[]( const int& i ); */

    /**
     * @brief           Multiplies all elements of a matrix times a scalar. The resulting
     *                  matrix is given back.
     *
     * @param i         the scalar
     * @return Matrix   the resulting product matrix
     */
    Matrix operator*( const float& i ) const;

    /**
     * @brief       same as with the * operator, but the calling matrix becomes the result
     *
     * @param i     the multiplying scalar
     */
    Matrix& operator*=( const float& i );

    /**
     * @brief           divides a matrix by a scalar (element-wise)
     *
     * @param i         the dividend
     * @return r        the resulting Matrix
     */
    Matrix operator/( const float& i ) const;

    /**
     * @brief same as with the / operator, but the calling matrix becomes the result
     *
     * @param i the dividend
     */
    Matrix& operator/=( const float& i );

    /**
     * @brief           Multiplies two matrices, and gives out the resulting Matrix
     *
     * @param m2        the multiplier matrix
     * @return Matrix   the resulting matrix
     */
    Matrix operator*( const Matrix& m2 ) const; // Also dangerous, but [] wouldn't otherwise work

    /**
     * @brief       Same as with the * operator, but turns the calling matrix into the product
     *              of the both
     *
     * @param m2    The multiplying matrix
     */
    Matrix& operator*=( const Matrix& m2 ); // This [] is really not worth the risk, but I don't want to risk any points

    /**
     * @brief           Adds two matrices (element-wise) and returns the resulting matrix
     *
     * @param m2        the addend matrix
     * @return Matrix   the resulting matrix
     */
    Matrix operator+( const Matrix& m2 ) const;

    /**
     * @brief       Same as with the + operator, but turns the calling matrix into the result
     *
     * @param m2    the addend matrix
     */
    Matrix& operator+=( const Matrix& m2 );

    /**
     * @brief           same as with +, but the second matrix will be multiplied by -1
     *
     * @param m2        the subtracting matrix
     * @return Matrix   the resulting matrix
     */
    Matrix operator-( const Matrix& m2 ) const;

    /**
     * @brief       Same as with the - operator, but the calling Matrix becomes the result
     *
     * @param m2    the subtracting matrix
     */
    Matrix& operator-=( const Matrix& m2 );

    /**
     * @brief multiplies this Matrix times a Vector
     *
     * @param v
     * @return Vector
     */
    Vector operator*( const Vector& v ) const; // Stranger danger

    /**
     * @brief           (for points being represented as Vectors) translate them using
     *                  the part of the matrix (the last column) to translate it
     *
     * @param v         The Vector (point) to be translated
     * @return Vector   the translated point (as vector)
     */
    Vector translate( const Vector& v ) const { return Vector( v.x + m[3], v.y + m[7], v.z + m[11] ); }

    /**
     * @brief       copies all the elements of m2 into the calling matrix (this)
     *
     * @param m2    the matrix to be copied
     */
    Matrix& operator=( const Matrix& m2 );

	/// Destructor
    ~Matrix();

	/**
	 * @brief	Returns the internal data array. Unsafe. Will probably
	 * 			removed in one of the next versions.
	 */
	float* getData() { return m; }

private:

    /// Internal data array
	float m[16];

};

} // namespace asteroids
#endif /* MATRIX_H_ */
