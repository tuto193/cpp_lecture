/**
 *  @file Vector.cpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "Vector.hpp"

namespace asteroids {

Vector::Vector()
{
	// Default values
	x = y = z = 0.0;
}


Vector::Vector(float _x, float _y, float _z)
{
	// Set the given values
	x = _x;
	y = _y;
	z = _z;
}

Vector Vector::operator+( const Vector& v ) const
{
    float x, y, z;
    x = this->x + v.x;
    y = this->y + v.y;
    z = this->z + v.z;
    return Vector( x, y, z );
}


Vector& Vector::operator+=( const Vector& v )
{
    *this = *this + v;
    return *this;
}

float Vector::operator*( const Vector& v ) const
{
    float x, y, z;
    x = this->x * v.x;
    y = this->y * v.y;
    z = this->z * v.z;
    return x + y + z;
}

Vector& Vector::operator=( const Vector& v )
{
    if( this == &v )
    {
        return *this;
    }
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
    return *this;
}

Vector Vector::crossProduct( const Vector& v ) const
{
    float x, y, z;
    x =     this->y * v.z - v.y * this->z;
    y =   -(this->x * v.z - v.x * this->z);
    z =     this->x * v.y - v.x * this->y;
    return Vector( x, y, z );

}

void Vector::normalize()
{
	// Normalize the vector
	float mag2 = x * x + y * y + z * z;
	if (fabs(mag2 - 1.0f) > 0.00001)
	{
		float mag = sqrt(mag2);
		x /= mag;
		y /= mag;
		z /= mag;
	}
}

} // namespace asteroids
