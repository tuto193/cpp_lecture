/*
 * Matrix.cpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "Matrix.hpp"

namespace asteroids
{

Matrix::Matrix()
{
    for(int i = 0; i < 16; i++) m[i] = 0;
    m[0] = m[5] = m[10] = m[15] = 1;
}

Matrix::Row Matrix::operator[]( const int& i ) const
{
    Matrix proxy( *this );
    if( i < 0 || i > 3 )
    {
        cout << "Error: index of row out of bounds" << endl;
        return Row( proxy.getData() );
    }
    return Row( proxy.getData() + (i * 4) );
}

Matrix::Row Matrix::operator[]( const int& i )
{
    if( i < 0 || i > 3 )
    {
        cout << "Error: index of row out of bounds" << endl;
        return Row( getData() );
    }
    return Row( getData() + (i * 4) );
}

/* float* Matrix::operator[]( const int& i )
{
    if( i < 0 || i > 3 )
    {
        cout << "Error: index of row out of bounds" << endl;
        return m;
    }
    return &(m[i]);
} */

Matrix::Matrix( const Matrix& mat ) //Dangerous, but it wouldn't otherwise work
{
    int i, j;
    for( i = 0; i < 4; i++ )
    {
        for( j = 0; j < 4; j++ )
        {
            m[i * 4 + j] = mat[i][j];
        }
    }
}

/* Matrix::Matrix( Vector axis, float angle )
{

} */

Matrix::Matrix( const Vector& v1, const Vector& v2 )
{
    m[0] = v1.x * v2.x;
    m[1] = v1.x * v2.y;
    m[2] = v1.x * v2.z;
    m[3] = 0.0;
    m[4] = v1.y * v2.x;
    m[5] = v1.y * v2.y;
    m[6] = v1.y * v2.z;
    m[7] = 0.0;
    m[8] = v1.z * v2.x;
    m[9] = v1.z * v2.y;
    m[10] = v1.z * v2.z;
    m[11] = 0.0;
    m[12] = 0.0;
    m[13] = 0.0;
    m[14] = 0.0;
    m[15] = 1.0;
}

Matrix& Matrix::operator=( const Matrix& m2 )
{
    int i, j;
    if( this == &m2 )
    {
        return *this;
    }

    for( i = 0; i < 4; i++ )
    {
        for( j = 0; j < 4; j++ )
        {
            m[i * 4 + j] = m2[i][j];
        }
    }
    return *this;
}

Matrix Matrix::operator*( const float& i ) const
{
    int j, k;
    Matrix r;
    for( j = 0; j < 4; j++ )
    {
        for( k = 0; k < 4; k++ )
        {
            r[j][k] = m[j * 4 + k] * i;
        }
    }
    return r;
}

Matrix Matrix::operator*( const Matrix& m2 ) const
{
    Vector a1, a2, a3;
    Vector b1, b2, b3, b4;
    float c1, c2,  c3,  c4,
          c5, c6,  c7,  c8,
          c9, c10, c11, c12;
    Matrix product;

    // Left Matrix (ROWS) The bottom row is all 0s and a 1
    a1 = Vector( m[0], m[1], m[2] );
    a2 = Vector( m[4], m[5], m[6] );
    a3 = Vector( m[8], m[9], m[10] );

    // Right Matrix (COLUMNS)
    b1 = Vector( m2[0][0], m2[1][0], m2[2][0] );
    b2 = Vector( m2[0][1], m2[1][1], m2[2][1] );
    b3 = Vector( m2[0][2], m2[1][2], m2[2][2] );
    b4 = Vector( m2[0][3], m2[1][3], m2[2][3] );

    c1 = a1 * b1;
    c2 = a1 * b2;
    c3 = a1 * b3;
    c4 = (a1 * b4) + m[3] * 1;
    c5 = a2 * b1;
    c6 = a2 * b2;
    c7 = a2 * b3;
    c8 = (a2 * b4) + m[7] * 1;
    c9 = a3 * b1;
    c10 = a3 * b2;
    c11 = a3 * b3;
    c12 = a3 * b4 + m[11] * 1;

    product[0][0] = c1;
    product[0][1] = c2;
    product[0][2] = c3;
    product[0][3] = c4;
    product[1][0] = c5;
    product[1][1] = c6;
    product[1][2] = c7;
    product[1][3] = c8;
    product[2][0] = c9;
    product[2][1] = c10;
    product[2][2] = c11;
    product[2][3] = c12;
    product[3][0] = 0.0;
    product[3][1] = 0.0;
    product[3][2] = 0.0;
    product[3][3] = 1.0;

    return product;
}

Matrix& Matrix::operator*=( const float& i )
{
    *this = *this * i;
    return *this;
/*
    for( j = 0; j < 4; j++ )
    {
        for( k = 0; k < 4; k++ )
        {
            m[j * 4 + k] = m[j * 4 + k] * i;
        }
    }
    return *this; */
}

Matrix& Matrix::operator*=( const Matrix& m2 )
{
    *this = *this * m2;
    return *this;
/*     for( i = 0; i < 4; i++ )
    {
        for( j = 0; j < 4; j++ )
        {
            *this[i][j] = result[i][j];
        }
    } */
}

Vector Matrix::operator*( const Vector& v ) const
{
    Vector a1, a2, a3;
    Vector result;
    // Left Matrix (ROWS) The bottom row is all 0s and a 1
    a1 = Vector( m[0], m[1], m[2] );
    a2 = Vector( m[4], m[5], m[6] );
    a3 = Vector( m[8], m[9], m[10] );
    // Multiply the rows times the columns
    result = Vector( a1 * v, a2 * v, a3 * v );
    // Don't forget the fourth element of each row of the matrix times 1
    result.x += (1 * m[3] );
    result.y += (1 * m[7]);
    result.z += (1 * m[11]);
    return result;
}

Matrix Matrix::operator+( const Matrix& m2 ) const
{
    int i, j;
    Matrix result;
    for( i = 0; i < 4; i++ )
    {
        for( j = 0; j < 4; j++ )
        {
            result[i][j] = m[i * 4 + j] + m2[i][j];
        }
    }
    return result;
}

Matrix& Matrix::operator+=( const Matrix& m2 )
{
    *this = *this + m2;
    return *this;
    /* for( i = 0; i < 4; i++ )
    {
        for( j = 0; j < 4; j++ )
        {
            s[i][j] = result[i][j];
        }
    } */
}

Matrix Matrix::operator-( const Matrix& m2 ) const
{
    Matrix result;
    Matrix m3 = ( m2 * -1.0 );
    result = *this + m3;
    return result;
}

Matrix& Matrix::operator-=( const Matrix& m2 )
{
    *this = *this - m2;
    return *this;
/*     for( i = 0; i < 4; i++ )
    {
        for( j = 0; j < 4; j++ )
        {
            *this[i][j] = result[i][j];
        }
    } */
}

Matrix Matrix::operator/( const float& i ) const
{
    int j, k;
    Matrix r;
    for( j = 0; j < 4; j++ )
    {
        for( k = 0; k < 4; k++ )
        {
            r[j][k] = m[j * 4 + k] / i;
        }
    }
    return r;
}

Matrix& Matrix::operator/=( const float& i )
{
    *this = *this / i;
    return *this;
/*     for( j = 0; j < 4; j++ )
    {
        for( k = 0; k < 4; k++ )
        {
            *this[j][k] = *this[j][k] / i;
        }
    } */
}

Matrix::~Matrix()
{
}


} // namespace asteroids
