/**
 *  @file Vector.hpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef __Vector_HPP__
#define __Vector_HPP__

#include <iostream>
#include <cmath>

namespace asteroids
{

/**
 * @brief   Vector representation with three floats for OpenGL
 *
 */
class Vector {

public:

    /**
     * @brief default copy constructor
     *
     * @param v the vector to be copied
     */
    Vector( const Vector& v ): Vector( v.x, v.y, v.z )  {}

	/**
	 * @brief   Construcs a default Vector object
	 */
	Vector();

	/**
	 * @brief   Construcs a Vector object with given values
	 * @param x x-value
	 * @param y y-value
	 * @param z z-value
	 */
	Vector(float x, float y, float z);

    /**
     * @brief add the x, y and z values of this and v's Vector respectively
     *
     * @param v         The summand Vector
     * @return Vector   The resulting sum of the Vectors
     */
    Vector operator+( const Vector& v ) const;

    /**
     * @brief same as with "+", but the the calling Vector(this) becomes
     * the resulting sum of the Vectors
     *
     * @param v         The summand Vector
     */
    Vector& operator+=( const Vector& v );

    /**
     * @brief Get the scalar product of two vectors
     *
     * @param v         The multiplicating vector
     * @return float    The resulting scalar
     */
    float operator*( const Vector& v ) const;

    /**
     * @brief       multiplies a vector times a scalar
     *
     * @param s     the multiplying scalar
     * @return Vector the resulting product vector
     */
    Vector operator*( const float& s ) const { return Vector( this->x * s, this->y * s, this->z * s ); }

    /**
     * @brief   same as with the operator *, but the calling vector becomes the product of the multiplication
     *
     * @param s the multiplying scalar
     */
    Vector& operator*=( const float& s ) { *this = *this * s; return *this; }

    /**
     * @brief          divides a vector by a scalar
     *
     * @param s        the dividend scalar
     * @return Vector  the resulting vector
     */
    Vector operator/( const float& s ) const { return *this * ( 1.0/s ); }

    /**
     * @brief       same as with the operator /, but the calling vector becomes the result of the division
     *
     * @param s     the dividend scalar
     */
    Vector& operator/=( const float& s ) { *this = *this / s; return *this; }

    /**
     * @brief           same as with the + operator, but the subtracting vector is multiplied before by -1
     *
     * @param v         the subtracting vector
     * @return Vector   the result of the subtraction
     */
    Vector operator-( const Vector& v ) const { Vector result = *this + (v * -1.0); return result; }

    /**
     * @brief       same as with the - operator, but the calling vector becomes the result
     *
     * @param v     the subtracting vector
     */
    Vector& operator-=( const Vector& v ) { *this = *this - v; return *this; }

    /**
     * @brief   copies the values of the right vector into the left vector
     *
     * @param v the vector to be copied
     */
    Vector& operator=( const Vector& v );

    /**
     * @brief return the cross product of 2 Vectors: this and v
     *
     * @param v         the second vector
     * @return Vector   the resulting cross_product
     */
    Vector crossProduct( const Vector& v ) const;

	/**
	 * @brief   Normalize a Vector
	 */
	void normalize();

	/**
	 * @brief   The three values of a vector
	 */
	float x, y, z;
};

} // namespace asteroids

#endif
