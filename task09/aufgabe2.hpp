#ifndef SHARED_ARRAY
#define SHARED_ARRAY

#include <memory> // std::shared_ptr
#include <functional>

namespace asteroids
{

/**
 * @brief       simple class to represent the implementation of statically allocated arrays with smart pointers
 *
 * @tparam T    the type of array to be allocated
 * @tparam L    the length of the array to be allocated
 */
template <typename T> class shared_array : public std::shared_ptr<T>
{
public:

    /**
     * @brief using the papa-constuctor as seen on stackoverflow...
     *
     */
    using std::shared_ptr<T>::shared_ptr;

    /**
     * @brief The default constructor for our array. It shouldn't need any arguments apart from those given on the
     *        template declaration
     *
     */
    shared_array<T>(int size);

    /**
     * @brief De-allocate all the memory used using shared_ptr's memeber function: reset.
     *
     */
    ~shared_array<T>();

    /**
     * @brief           the access (READ) operator, so we can get elements from within our array.
     *
     * @param index     the index from within the array to be used
     * @return T        the value at the index given
     */
    T operator[]( const int& index ) const;

    /**
     * @brief           the access (WRITE) operator, so we can get elements from within our array.
     *
     * @param index     the index from within the array to be used
     * @return T        the value at the index given
     */
    T& operator[]( const int& index );

    /**
     * @brief   operator which should be formally used to free memory used to allocate one of these shared_arrays
     *
     */
    void operator delete[](void* ptr); /// I still have no idea whether this shit works or not ¯\_(ツ)_/¯

private:

    /**
     * The elements here below are used exclusively within the class for a proper use of the delete operator,
     * in the case that we need to use an iterator
     *
     */
    /// The element at index 0 in our array
    T* arr;

    /// The size of the array
    int size;

}; // shared_array

} // namespace asteroids

#include "aufgabe2.tcc" // Uncomment after done

#endif ///SHARED_ARRAY
