/*
 * PLYElement.hpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */


#ifndef PLYELEMENT_HPP_
#define PLYELEMENT_HPP_

#include <vector>
#include <iostream>
#include <memory>
using std::vector;
using std::ostream;
using std::endl;

#include "PLYProperty.hpp"

namespace asteroids
{

/**
 * @brief Representation of an element in a ply file.
 */
class PLYElement
{
public:

/////// AUFGABE 2 /////////////////
    using Ptr = std::shared_ptr<PLYElement>;
////// AUFGABE 2    ///////////////
	/**
	 * @brief Default ctor.
	 */
	PLYElement() : m_name(""), m_count(0) {};

	/**
	 * @brief Creates an element description with given name and count
	 *
	 * @param name			The name element
	 * @param count			The number of elements in the file
	 */
	PLYElement(string name, size_t count) : m_name(name), m_count(count){};

	/**
	 * @brief Adds a new property with the given name into the element.
	 *
	 * @param name			The name of the new property.
	 * @param elementType	The element type
	 * @
	 */
	void addProperty(string name, string elementType, string listType = "", size_t count = 0)
	{
		// First create a scalar property
		Property::Ptr p;

		// Now we have to instantiate a appropriate property class
        /// Here is where the magic of static cast would come to play, but I have no idea how. There are just errors everywhere
		if(elementType == "char")
		{
			p = std::make_shared<Property> ( new ScalarProperty<char>(name) );
		}
		else if(elementType == "uchar")
		{
			p = std::make_shared<Property> ( new ScalarProperty<uchar>(name) );
		}
		else if(elementType == "short")
		{
			p = std::make_shared<Property> ( new ScalarProperty<short>(name) );
		}
		else if(elementType == "uint")
		{
			p = std::make_shared<Property> ( new ScalarProperty<uint>(name) );
		}
		else if(elementType == "int")
		{
			p = std::make_shared<Property> ( new ScalarProperty<int>(name) );
		}
		else if(elementType == "float")
		{
			p = std::make_shared<Property> ( new ScalarProperty<float>(name) );
		}
		else if(elementType == "double")
		{
			p = std::make_shared<Property> ( new ScalarProperty<double>(name) );
		}

		// Check if we have a list
		if(listType == "")
		{
			m_properties.push_back(p);
			return;
		}
		else
		{
			Property::Ptr l;
			if(listType == "char")
			{
				l = std::make_shared<Property> (new ListProperty<char>(count, p) );
			}
			else if(listType == "uchar")
			{
				l = std::make_shared<Property> (new ListProperty<uchar>(count, p) );
			}
			else if(listType == "short")
			{
				l = std::make_shared<Property> (new ListProperty<short>(count, p) );
			}
			else if(listType == "uint")
			{
				l = std::make_shared<Property> (new ListProperty<uint>(count, p) );
			}
			else if(listType == "int")
			{
				l = std::make_shared<Property> (new ListProperty<int>(count, p) );
			}
			else if(listType == "float")
			{
				l = std::make_shared<Property> (new ListProperty<float>(count, p) );
			}
			else if(listType == "double")
			{
				l = std::make_shared<Property> (new ListProperty<double>(count, p) );
			}
			m_properties.push_back(l);
		}

	}

	/***
	 * @brief Gets an iterator to the first property in the property list.
	 */
	vector<Property::Ptr>::iterator getFirstProperty() { return m_properties.begin(); }

	/**
	 * @brief Returns an iterator to the end of the property list.
	 */
	vector<Property::Ptr>::iterator getLastProperty() { return m_properties.end(); }

	/**
	 * @brief Returns the name of the element
	 */
	string getName() {return m_name;}

	/**
	 * @brief Returns the number of elements in the ply file.
	 */
	size_t getCount() {return m_count;}

	/**
	 * @brief Prints a ply conform description of each property
	 * 		  in the element to the given stream.
	 */
	void printProperties(ostream &out)
	{
		for(vector<Property::Ptr>::iterator it = m_properties.begin();
			it != m_properties.end();
			it ++)
		{
			Property::Ptr p = *it;
			out << "property ";
			if(p->isList())
			{
				out << "list ";
				out << p->getCountTypeStr() << " ";
			}
			out << p->getElementTypeStr() << " ";
			out << p->getName() << endl;
		}
	}

private:

	/// A list of properties of the current element
	vector<Property::Ptr> m_properties;

	/// The name of the element
	string 	m_name;

	/// The number of elements of this type in the ply file.
	size_t	m_count;


};

}
#endif /* PLYELEMENT_HPP_ */
