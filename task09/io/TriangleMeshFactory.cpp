/*
 * TraingleMeshFactory.cpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */
#include <iostream>
using std::cout;
using std::endl;

#include "TriangleMeshFactory.hpp"

#include "io/Read3DS.hpp"
#include "io/ReadPLY.hpp"
#include "io/ReadOBJ.hpp"

namespace asteroids
{

TriangleMesh::Ptr TriangleMeshFactory::getMesh(const string &filename) const
{
    // A mesh pointer and MeshIO pointer
    MeshReader::Ptr io = 0;
    TriangleMesh::Ptr mesh = 0;

    // Get file extension
    if(filename.substr(filename.find_last_of(".") + 1) == "ply")
    {
        io = std::make_shared<MeshReader> (new ReadPLY(filename) );
    }
    else if(filename.substr(filename.find_last_of(".") + 1) == "3ds")
    {
        io = std::make_shared<MeshReader> (new Read3DS(filename) );
    }
    else if(filename.substr(filename.find_last_of(".") + 1) == "obj")
    {
        io = std::make_shared<MeshReader> (new ReadOBJ(filename) );
    }

    // Get mesh from io
    if(io)
    {
        mesh = io->getMesh();
    }
    else
    {
        cout << "Unable to parse file " << filename << "." << endl;
    }

    io.reset();
    return mesh;
}

TriangleMeshFactory& TriangleMeshFactory::instance()
{
    // Just crate on instance
    static TriangleMeshFactory instance;
    return instance;
}

}
