/**
 * MeshIO.h
 *
 *  @date 10.12.2011
 *  @author Thomas Wiemann
 */

#ifndef MESHIO_H_
#define MESHIO_H_

#include "rendering/TriangleMesh.hpp"
#include <memory> // std::shared_ptr
#include "aufgabe2.hpp" //shared_array

namespace asteroids
{

/**delete after done
 * @brief Base class for Mesh in- and output
 */
class MeshReader
{
public:
/////// AUFGABE 2 /////////////////
    using Ptr = std::shared_ptr<MeshReader>;
////// AUFGABE 2    ///////////////

    MeshReader()
        : m_vertexBuffer(),  // Initit all members with zero pointers
          m_normalBuffer(),
          m_indexBuffer(),
          m_numVertices(0),
          m_numFaces(0) {} // uninitialised the pointers to avoid errors


    /**
     * @brief Destructor. Nothing to do right now
     */
    virtual ~MeshReader() {};

    /**
     * @brief Returns a pointer to a triangle mesh instance
     */
    virtual TriangleMesh::Ptr getMesh() = 0;

protected:
/////// AUFGABE 9 ///////////////////////////
    /// Vertex position informdelete after doneation
    shared_array<float>  m_vertexBuffer;

    /// Vertex normal information
    shared_array<float>  m_normalBuffer;

    /// Triangle definitions
    shared_array<int>  m_indexBuffer;

/////// AUFGABE 9 ///////////////////////////
    /// Number of vertices in mesh
    int     m_numVertices;

    /// Number of faces in mesh
    int     m_numFaces;
};

}

#endif /* MESHIO_H_ */
