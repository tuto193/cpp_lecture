/*
 * 1. Alle geraden Zahlen aus einem Vektor sollen geloescht werden
 * 2. std::bind ordnet einer Funktion mit mehreren Parametern mindestens einem
 *    Parameter einen festen Wert zu. Hier wird std::bind geschachtelt aufgerufen,
 *    das heißt einer Funktion, der eine Funktion uebergeben werden soll, bekommt
 *    eine Funktion mit gebundemem Parameter uebergeben. In diesem Fall wird die
 *    equal_to-Funktion ein Parameter uebergeben, der dafuer sorgt, dass immer mit
 *    0 verglichen wird und der Modulo-Operator, der den freien Parameter immer
 *    mod 2 nimmt. Zusammengefasst berechnet, die aeussere bind-Funktion also ob
 *    ein ihr uebergebener Parameter durch zwei teilbar ist oder nicht.
 * 3. s.u. remove_if gibt den iterator der Stelle zurueck, bis zu der alle nicht
 *    geloeschten Stellen des Vektors drinstehen, deshalb darf die Ausgabe nur bis
 *    zu diesem Iterator gehen.
 */

#include <vector>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <iterator>
#include <functional>

int main()
{
    std::vector<int> v = {1, 4, 2, 8, 5, 7};
    copy(v.begin(), v.end(), std::ostream_iterator<int>(std::cout," "));
    std::cout << std::endl;
    auto it = remove_if(v.begin(), v.end(),
                    std::bind(std::bind(std::equal_to<int>(), std::placeholders::_1, 0),
                    std::bind(std::modulus<int>(), std::placeholders::_1, 2)));
    copy(v.begin(), it, std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
    return 0;
}