//#include "aufgabe2.hpp" // comment out after done

#include <cassert> // static_assert
#include <iostream> // std::cerr

namespace asteroids
{

template <typename T>
shared_array<T>::shared_array(int size)
{
    this->size = size;
    arr = new T[size];
}

template <typename T>
T& shared_array<T>::operator[]( const int& index )
{
    if( index < 0 || index >= this->size)
    {
        std::cerr << "The entered index doesn't exist" << std::endl;
        return *arr;
    }

    return arr[index];
}

template <typename T>
T shared_array<T>::operator[]( const int& index ) const
{
    shared_array<T> proxy = this;
    if( index < 0 || index >= this->size)
    {
        std::cerr << "The entered index doesn't exist" << std::endl;
        return *arr;
    }

    return proxy[index];
}

template <typename T>
void shared_array<T>::operator delete[](void* ptr)
{
  /*   std::shared_ptr<T>( ptr, [&] ( ptr ) {
        for( int i = 0; i < this->size; i++ )
        {
            delete ptr[i];
        }
    }); */
}

} // namespace asteroids