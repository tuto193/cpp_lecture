/*
 * List.tcc
 *
 *  @date 02.12.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include <iostream>
#include "List.hpp" // Comment out after done

namespace asteroids
{

template<typename T> List<T>::List()
{
   m_list = nullptr;
}
template<typename T> List<T>::~List()
{
   Iterator::Node::Ptr next = m_list;
   do
   {
      List<L>::Node::Ptr to_delete = List<T>::Node::next;
      List<T>::Node::next = List<T>::Node::next->next;
      List<L>::Node::to_delete.reset();
   } while(next);
}
template<typename T> void List<T>::push_back(T item)
{
   if (m_list == nullptr)
   {
      m_list = std::make_shared<Node>(new Node(item, nullptr));
   }
   else
   {
      m_list = new Node(item, m_list);
   }
}
template<typename T> void List<T>::for_each(void (*do_something)(T& item))
{
   Node::Ptr tmp = m_list;
   while (tmp != nullptr)
   {
      do_something(tmp->data);
      tmp = tmp->next;
   }
}

} // namespace asteroids
