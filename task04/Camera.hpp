/*
 *  Camera.hpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

#define PI 3.14159265
#define PH 1.57079632

const float MOVE_SPEED = 9.0;
const float TURN_SPEED = 10.0 * (PI/360.0);
namespace asteroids
{



/**
 * @brief Class to represent 3D vertices with float coordinates
 */
class Vector
{
public:
    /**
     * @brief Construct a new Vector object
     *
     * @param ix    Initial x value (default: 0.0)
     * @param iy    Initial y value (default: 0.0)
     * @param iz    Initial z value (default: 0.0)
     */
    Vector(float ix = 0.0, float iy = 0.0, float iz = 0.0)
        : x(ix), y(iy), z(iz) {}

    /**
     * @brief returns the sum of this Vector and v2
     *
     * @param v2 a Vector to add
     * @return Vector the resulting sum of the two vectors
     */
    Vector addVector( Vector v2 );

    /**
     * @brief Returns the Vector formed out of two points that were being represented
     * each by a Vector.
     *
     * The first point is the Vector calling the function, the second point is p2
     *
     * @param p2    the second point
     * @return Vector   our Vector formed by (p2 - p1 )
     */
    Vector pointsToVector( Vector p2 );

    /**
     * @brief Multiplies this vector times a number n
     *
     * @param n the float that multiplies the whole attributes of the vector
     * @return Vector the product of the multiplication made
     */
    Vector multNumber( float n );

    /**
     * @brief returns the this Vector's normal form
     *
     * @return Vector   The normal form of the Vector
     */
    Vector normalize();

    /**
     * @brief returns the Vector perpendicular to this and the given Vector
     *
     * If this is r, and v2 is u, then it will return the Vector pointing to the right.
     * If this is u, and v2 is r, then it will return the Vector pointing to the left.
     *
     * @param v2
     * @return Vector
     */
    Vector crossProduct( Vector v2 );

    /**
     * @brief get the dot product of this Vector with another 3D Vector
     *
     * @param v3        The other vector
     * @return float    The dot product of this and the other vector
     */
    float dotProduct( Vector v3 );

    float x;
    float y;
    float z;
};

/**
 * @brief Class to represent a Matrix
 *
 */
class Matrix
{
public:
    Matrix( float angle );

    /**
     * @brief Construct a new rot y Matrix object as an Identity Matrix
     *
     */
    Matrix()
    {
        alpha = 0.0;
        a123 = Vector( 1.0, 0.0, 0.0 );
        a4 = 0.0;
        b123 = Vector( 0.0, 1.0, 0.0 );
        b4 = 0.0;
        c123 = Vector( 0.0, 0.0, 1.0 );
        c4 = 0.0;
        d123 = Vector( 0.0, 0.0, 0.0 );
        d4 = 1.0;

    }

    /**
     * @brief initialize the matrix to the angle given (useful when declaring and then defining)
     * The matrix looks like this (as seen in CG last semester)
     * Matrix = [cosß   0  sinß 0
     *            0     1   0   0
     *          -sinß   0  cosß 0
     *            0     0   0   1]
     *
     * @param angle the angle to which the matrix should work
     */
    void init_rot_mat( float angle );

    /**
     * @brief Multiplies this Matrix with a Vector (element-wise) and gives out the rotated Vector back.
     *
     * This multiplication just takes into account the top-left [3x3] Matrix inside the matrix, and ignores the rest
     * (Since it wouldn't change the remaining Vector).
     *
     * For a rotation of a point, please make sure to use the extended Matrix (4x4)
     *
     * The vector is rotated on the Y Axis, in an agle alpha
     *
     * @param v         The Vector to be rotated
     * @return Vector   The resulting rotated vector
     */
    Vector multVector( Vector v );

    Vector a123;
    float a4;
    Vector b123;
    float b4;
    Vector c123;
    float c4;
    Vector d123;
    float d4;

    float alpha;

};

/**
 * @brief Class to represent a virtual camera using gluLookAt
 *
 */
class Camera
{
public:
    /**
     * @brief Enumeration to encode types of camera movements
     */
    enum CameraMovement
    {
	    FORWARD,
	    BACKWARD,
	    LEFT,
	    RIGHT,
	    UP,
	    DOWN
    };

    /**
     * @brief Construct a new Camera object at (0, 0, 0) with
     *        upward orientation and lookAt at (0, 0, -1)
     *
     */
    Camera();

    /**
     * @brief Construct a new Camera object with upward orientation
     *        and lookAt at (0, 0, -1)
     *
     * @param position      Initial position
     * @param turnSpeed     Turning speed in radians per call
     * @param moveSpeed     Move speed in world units per call
     */
    Camera(Vector position, float turnSpeed, float moveSpeed);

    /**
     * @brief Moves the camera according to given direction
     *
     * @param dir           Moving direction
     */
    void move(CameraMovement dir);

     /**
     * @brief turns the camera according to given direction
     *
     * @param dir           Moving direction
     */
    void turn(CameraMovement dir);

    /**
     * @brief Calls gluLookAt with the internal parameters
     *
     */
    void apply();

    /**
     * @brief Set the turn speed  of the camera
     *
     * @param speed         The new turn speed
     */
    void setTurnSpeed(float speed) { m_turnSpeed = speed;}

    /**
     * @brief Set the move speed  of the camera
     *
     * @param speed         The new move speed
     */
    void setMoveSpeed(float speed) { m_moveSpeed = speed;}

private:
    /// View up vector
    Vector  m_up;

    /// Translation
    Vector  m_trans;

    /// Look at vector
    Vector  m_l;

    /// Rotation angles encoded in vector, i.e., x is the
    /// rotation around the x-axis and so on
    Vector  m_rot;

    /// Initial position of the camera
    Vector  m_initial;

    /// Turn speed in radians per call
    float   m_turnSpeed;

    /// Move speed in world units per call
    float   m_moveSpeed;
};

} // namespace asteroids

#endif
