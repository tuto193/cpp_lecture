/*
 *  Main.cpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "MainWindow.hpp"

#include <iostream>

int main(int argc, char** argv)
{
    if (argc == 2)
    {
        asteroids::MainWindow mw = asteroids::MainWindow("Asteroids", argv[1], 1024, 768);
        mw.execute();
    }
    else
    {
        std::cout << "usage: asteroids <modelfile>" << std::endl;
    }
    return 0;
}
