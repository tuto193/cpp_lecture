/*
 *  MainWindow.cpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "MainWindow.hpp"

#include <iostream>

namespace asteroids
{

MainWindow::MainWindow(
    std::string title,
    std::string plyname, int w, int h)
{
    // TODO: IMPLEMENT!
    cam = new Camera();
    m_model = new Model(plyname);
    const char* charTitle = title.c_str();

    //TODO: Error-Handling
    SDL_Init(SDL_INIT_VIDEO);

    mainWindow = SDL_CreateWindow(
        charTitle,
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        w,
        h,
        SDL_WINDOW_OPENGL);
    SDL_GLContext context = SDL_GL_CreateContext(mainWindow);

    /*  Error Handling  */
    if( !mainWindow )
    {
        printf( "Unable to create windows \n" );
        printSDLError( __LINE__ );
    }

    mainContext = &context;
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);


    SDL_GL_SetSwapInterval(1);

    glewExperimental = GL_TRUE;
	glewInit();

    SDL_GL_SwapWindow(mainWindow);

    glClearColor(0.0, 0.0, 0.0, 1.0);
	float ratio =  w * 1.0 / h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w-1, h);
	gluPerspective(45,ratio,1,10000);

    glMatrixMode(GL_MODELVIEW);

}

void MainWindow::execute()
{
    // TODO: IMPLEMENT MAIN LOOP HERE
    // FIRST IMPLEMENT BASIC FUNCTIONALITY BASED
    // ON THE PROVIDED SOLUTION OF THE 3rd ASSGINMENT.
    // AFTER THIS PORT IS DONE, ADD CAMERA HANDLING
    // AS REQUESTED IN THE SECOND EXERCISE.
    bool loop = true;

    glLoadIdentity();

    while(loop)
    {
        glClear(GL_COLOR_BUFFER_BIT );
        // Reset the values each frame to make sure that we are not spinning out of control
        cam->setMoveSpeed( 0.0 );
        cam->setTurnSpeed( 0.0 );
        cam->apply();

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch(event.type)
            {
                case SDL_QUIT:
                    loop = false;
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_w:
                            cam->setMoveSpeed( MOVE_SPEED );
                            cam->move(Camera::FORWARD);
                            break;
                        case SDLK_s:
                            cam->setMoveSpeed( MOVE_SPEED );
                            cam->move(Camera::BACKWARD);
                            break;
                        case SDLK_a:
                            cam->setMoveSpeed( MOVE_SPEED );
                            cam->move(Camera::LEFT);
                            break;
                        case SDLK_d:
                            cam->setMoveSpeed( MOVE_SPEED );
                            cam->move(Camera::RIGHT);
                            break;
                        case SDLK_UP:
                            cam->setMoveSpeed( MOVE_SPEED );
                            cam->move(Camera::UP);
                            break;
                        case SDLK_DOWN:
                            cam->setMoveSpeed( MOVE_SPEED );
                            cam->move(Camera::DOWN);
                            break;
                        case SDLK_LEFT:
                            cam->setTurnSpeed( TURN_SPEED );
                            cam->turn(Camera::LEFT);
                            break;
                        case SDLK_RIGHT:
                            cam->setTurnSpeed( TURN_SPEED );
                            cam->turn(Camera::RIGHT);
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_w:
                            cam->setMoveSpeed( 0.0 );
                            cam->move(Camera::FORWARD);
                            break;
                        case SDLK_s:
                            cam->setMoveSpeed( 0.0 );
                            cam->move(Camera::BACKWARD);
                            break;
                        case SDLK_a:
                            cam->setMoveSpeed( 0.0 );
                            cam->move(Camera::LEFT);
                            break;
                        case SDLK_d:
                            cam->setMoveSpeed( 0.0 );
                            cam->move(Camera::RIGHT);
                            break;
                        case SDLK_LEFT:
                            cam->setTurnSpeed( 0.0 );
                            cam->turn(Camera::LEFT);
                            break;
                        case SDLK_RIGHT:
                            cam->setTurnSpeed( 0.0 );
                            cam->turn(Camera::RIGHT);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
		}
        cam->apply();

		/* Render model */
		m_model->render();

		/* Bring up back buffer */
		SDL_GL_SwapWindow(mainWindow);

    }

}

void MainWindow::printSDLError( int line )
{
    std::string error = SDL_GetError();

	if( error.size() != 0 )
	{
		std::cout << "SLD Error : "<< error << std::endl;

        if(line != -1)
        {
		    std::cout << "Line : "<< line << std::endl;
        }
		SDL_ClearError();
	}
}

MainWindow::~MainWindow()
{
    // TODO: IMPLEMENT!
    if(m_model)
    {
        m_model->~Model();
    }

    if( cam )
    {
        cam->~Camera();
    }
    /* Delete our OpengL context */
	SDL_GL_DeleteContext(mainContext);

	/* Destroy our window */
	SDL_DestroyWindow(mainWindow);

	/* Shutdown SDL 2 */
	SDL_Quit();
}

} // namespace asteroids
