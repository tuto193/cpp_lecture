/*
 *  Camera.cpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include <stdio.h>
#include <math.h>
#include <GL/glu.h>

#include "Camera.hpp"

/// TODO: IMPLEMENT CAMERA HERE!
namespace asteroids
{

Vector Vector::addVector( Vector v2 )
{
    float x, y, z;
    x = this->x + v2.x;
    y = this->y + v2.y;
    z = this->z + v2.z;
    return Vector( x, y, z );
}

Vector Vector::pointsToVector( Vector p2 )
{
    float x, y, z;
    x = p2.x - this->x;
    y = p2.y - this->y;
    z = p2.z - this->z;
    return Vector( x, y, z );
}

Vector Vector::multNumber( float n )
{
    return Vector( this->x * n, this->y * n, this-> z * n );
}

Vector Vector::normalize()
{
    float n;
    // We need to divide each element of the Vector by the total Vector length
    n = ( this->x * this->x ) +( this->y * this->y ) +( this->z * this->z );
    n = sqrt( n );
    if( n == 0 )
    {
        return *this;
    }
    return Vector( this->x / n, this->y / n, this->z / n );
}

Vector Vector::crossProduct( Vector v2 )
{
    float x, y, z;
    x =     this->y * v2.z - v2.y * this->z;
    y =  -( this->x * v2.z - v2.x * this->z );
    z =     this->x * v2.y - v2.x * this->y;
    return Vector( x, y, z );
}

float Vector::dotProduct( Vector v2 )
{
    return ( this->x * v2.x ) + ( this->y * v2.y ) + ( this->z * v2.z );
}

Matrix::Matrix( float angle )
{
    alpha = angle;
    a123 = Vector( cos(alpha), 0.0, sin(alpha ) );
    a4 = 0.0;
    b123 = Vector( 0.0, 1.0, 0.0 );
    b4 = 0.0;
    c123 = Vector( sin(alpha) * -1, 0.0, cos(alpha) );
    c4 = 0.0;
    d123 = Vector( 0.0, 0.0, 0.0 );
    d4 = 1.0;
}

void Matrix::init_rot_mat( float angle )
{
    alpha = angle;
    a123 = Vector( cos(alpha), 0.0, sin(alpha ) );
    a4 = 0.0;
    b123 = Vector( 0.0, 1.0, 0.0 );
    b4 = 0.0;
    c123 = Vector( sin(alpha) * -1, 0.0, cos(alpha) );
    c4 = 0.0;
    d123 = Vector( 0.0, 0.0, 0.0 );
    d4 = 1.0;
}

Vector Matrix::multVector( Vector v )
{
    float x, y, z;
    x = this->a123.dotProduct( v );
    y = this->b123.dotProduct( v );
    z = this->c123.dotProduct( v );
    return Vector( x, y, z );
}

Camera::Camera()
{
    m_up =      Vector( 0.0, 1.0, 0.0 );
    m_l =       Vector( 0.0, 0.0, -1.0 );
    m_initial = Vector( 0.0, 0.0, 0.0 );
    m_turnSpeed = 1.0;
    m_moveSpeed = 1.0;

}

Camera::Camera( Vector position, float turnsSpeed, float moveSpeed )
{
    m_initial = position;
    m_up =      Vector( 0.0, 1.0, 0.0 );
    m_l =       Vector( 0.0, 0.0, -1.0 );
    setTurnSpeed( turnsSpeed );
    setMoveSpeed( moveSpeed );
}

void Camera::move( CameraMovement dir )
{
    // The Vector m_trans must be modified;
    this->setMoveSpeed( MOVE_SPEED );
    switch( dir )
    {
        case FORWARD:
            this->m_trans = this->m_l.multNumber( this->m_moveSpeed );
            break;

        case BACKWARD:
            this->m_trans = this->m_l.multNumber( this->m_moveSpeed * -1 );
            break;

        case LEFT:
            this->m_trans = this->m_l.crossProduct( this->m_up ).multNumber( this->m_moveSpeed * -1 );
            break;

        case RIGHT:
            this->m_trans = this->m_l.crossProduct( this->m_up ).multNumber( this->m_moveSpeed );
            break;

        case UP:
            this->m_trans = this->m_up.multNumber( this->m_moveSpeed );
            break;
        case DOWN:
            this->m_trans = this->m_up.multNumber( this->m_moveSpeed * -1 );
            break;

        default:
            break;
    }

    this->m_initial = this->m_initial.addVector(this->m_trans);
    this->m_l = this->m_l.addVector( this->m_trans );
}

void Camera::turn( CameraMovement dir )
{
    // We need to change/add the values of CameraMovement
    // We need a clear difference between turning and
    // moving sideways
    Matrix l_matrix;
    Vector a_b;
    this->setTurnSpeed( TURN_SPEED );
    switch( dir )
    {
        case LEFT:
            l_matrix.init_rot_mat( this->m_turnSpeed );
            a_b = this->m_initial.pointsToVector( this->m_l );
            a_b = l_matrix.multVector( a_b );
            this->m_initial = this->m_initial.addVector( a_b );
            break;

        case RIGHT:
            l_matrix.init_rot_mat( this->m_turnSpeed * -1 );
            a_b = this->m_initial.pointsToVector( this->m_l );
            a_b = l_matrix.multVector( a_b );
            this->m_initial = this->m_initial.addVector( a_b );
            break;

        default:
            break;
    }
    this->m_l = this->m_l.addVector(this->m_trans);
}

void Camera::apply()
{
    /*
    Our LookAt-Vector is not a point, and to get the point which we are looking at, we must
    add the LookAt to the position we are standing in. That way, it will be a point relative
    to the camera
    */
    glLoadIdentity();
    gluLookAt(  this->m_initial.x,  this->m_initial.y,  this->m_initial.z,
                this->m_l.x,        this->m_l.y,        this->m_l.z,
                this->m_up.x,       this->m_up.y,       this->m_up.z );
    this->m_trans = Vector( 0.0, 0.0, 0.0 );
    this->setMoveSpeed( 0.0 );
    this->setTurnSpeed( 0.0 );
}

} //namespace asteroids