/*
 *  MainWindow.hpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */


#include "Camera.hpp"
#ifndef __MAINWINDOW_HPP__
#define __MAINWINDOW_HPP__

#include <string>

#include <SDL2/SDL.h>

#define GL3_PROTOTYPES 1
#include <GL/glew.h>

#include "Model.hpp"

namespace asteroids
{

class MainWindow
{
public:

    /**
     * @brief Construct a new Main Window object
     *
     * @param title    The title of the window
     * @param plyname  A .ply file to render
     * @param w        The window width
     * @param h        The window heigt
     */
    MainWindow(std::string title, std::string plyname, int w, int h);

    /**
     * @brief Start the window's main loop
     */
    void execute();

    /**
     * @brief Destroys the Main Window object
     *
     */
    ~MainWindow();

    // TODO: ADD NEEDED DATA MEMBERS

    private:
        Model* m_model;
        SDL_Window* mainWindow;
        SDL_GLContext* mainContext;
        Camera* cam;
        /*
        Error handling is handled exclusively inside
        the window class
        */

       /**
        * @brief Handle errors when creating the window
        *
        * @param line the line at which the error happened
        */
       void printSDLError( int line );

};

}

#endif
