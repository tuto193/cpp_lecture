#ifndef RENDERABLE_HPP
#define RENDERABLE_HPP
#include "Vector.hpp"

namespace asteroids
{

/**
 * @brief Abstract class to represent figures that can be rendered
 */
class Renderable
{
public:
    /**
     * @brief Default constructor for our base class renderable. Makes sure
     * that the m_position of a shape is at the m_position of our Coordinates System
     *
     */
    Renderable(): Renderable( 0.0, 0.0, 0.0 ) {}

    /**
     * @brief An empty constructor to make sure that our object is created.
     * Nothing else needed here. Each renderable object must have an m_position, though
     *
     */
    Renderable( float x, float y, float z ) : m_position( Vector( x, y, z ) ) {}

    /**
     * @brief All objects that inherit from this class must have a Render function
     *
     */
    virtual void render() = 0;

    /**
     * @brief Just like with the constructor, this stays empty because our class is abstract
     *
     */
    ~Renderable() {}
protected:
    Vector m_position;
};

} // namespace asteroids


#endif