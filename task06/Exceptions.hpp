#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP
#include <iostream>

namespace asteroids
{

/**
 * @brief Our Base and simple exception class
 *
 */
class BaseException
{
public:
    /**
     * @brief Base exception doesn't need much to construct. It's a catch-all
     *
     */
    BaseException() {}

    /**
     * @brief               Gives out a message, when an exception is called.
     *                      Lets user know that there is an exception
     *
     * @return char const*  The warning message
     */
    virtual char const * what() const throw() { return "Exception met."; }

    ~BaseException() {}
};

/**
 * @brief Small Exception class to warn about unallowed divisions
 *
 */
class DivisionByZeroException : public BaseException
{
public:
    /**
     * @brief Empty constructor, because we don't need to know much to know that it is happening
     *
     */
    DivisionByZeroException() {}

    /**
     * @brief       Warn the user about the unallowed division
     *
     * @return char const*  The warning message
     */
    char const * what() const throw() { return "Error: Division by 0 exception"; }

    ~DivisionByZeroException() {}
};

/**
 * @brief Small exception class to warn the user about wrong accessing of Indexes
 *
 */
class OutOfBoundsException : public BaseException
{
public:
    /**
     * @brief Create our exception object and remember which index was being accessed.
     * If no Index is given and the exception is thrown, then 0 will be used
     * and the exception will the given out
     *
     * @param i     the index which was being wrongly accessed
     */
    OutOfBoundsException( const int& i = 0 ) : index(i) {}

    char const * what() const throw () { return getMessage().c_str(); }

    ~OutOfBoundsException() {}
private:
    std::string getMessage() const { std::string message  = "Error: Index " + std::to_string(index) + "out of bounds exception "; return message; }
    const int index;
};

} // namespace asteroids

#endif