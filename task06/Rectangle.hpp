#include "Renderable2D.hpp"
#include <GL/glew.h>

namespace asteroids
{

/**
 * @brief A small class for the 2D representation of a Rectangle
 *
 */
class Rectangle : public Renderable2D
{
public:

    /**
     * @brief All rectangles have an initial position, a width and a height
     *
     * @param mainWindow    the window in which the rectangle will be drawn
     * @param x             THe X coordinate
     * @param y             The Y coordinate
     * @param w             The width of the rectangle
     * @param h             The height of the rectangle
     */
    Rectangle(MainWindow* mainWindow, float x = 0.0, float y = 0.0, float w = 1.0, float h = 1.0) : Renderable2D(mainWindow, x, y), m_w(w), m_h(h) {}

    void render();

private:
    int m_w;
    int m_h;
};

}