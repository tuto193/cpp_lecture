#include "Renderable3D.hpp"
#include "Vector.hpp"

namespace asteroids {

/**
 * @brief Small class to represent a Sphere
 *
 */
class Sphere : public Renderable3D
{
public:

    /**
     * @brief Create a Sphere Object with default coordinates at origin and radius 100 and number of sides
     * and stacks at 10
     *
     */
    Sphere() : Sphere(Vector(0.0, 0.0, 0.0), 10) {}

    /**
     * @brief Create our Sphere Object at a designated values of its position and sides
     *
     * @param position  the desired origin position of the sphere
     * @param num       the desired ammount of sides and stacks for the sphere
     */
    Sphere(Vector position, int num) : Renderable3D( position.x, position.y, position.z ), m_radius(100.0), m_numStacks(num), m_numSides(num) {}

    void render();
private:
    // The radius of the sphere
    float m_radius;
    // The number ofn stacks that compose the sphere
    int m_numStacks;
    // The number of sides that compose the sphere
    int m_numSides;

};

}