#include "Renderable2D.hpp"
#include "MainWindow.hpp"

namespace asteroids 
{

void Renderable2D::setColor(float r, float g, float b)
{
    m_r = r;
    m_g = g;
    m_b = b;
}

void Renderable2D::prepareOrthoprojection()
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0.0f, m_mainWindow->width(), m_mainWindow->height(),0.0f, -10.0f, 10.0f);
}

void Renderable2D::endOrthoprojection()
{
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

}