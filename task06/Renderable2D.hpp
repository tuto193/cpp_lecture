#ifndef RENDERABLE2D_HPP
#define RENDERABLE2D_HPP

#include "Renderable.hpp"
#include <GL/glew.h>

namespace asteroids
{

class MainWindow;

/**
 * @brief Small class to represent figures that only possess two dimensions.
 * The origin of this is a 2D coordinate, with an aditional z value made to be fixed
 * (so that it can be rendered on a screen/3D world)
 *
 */
class Renderable2D : public Renderable
{
public:

    /**
     * @brief An object that can be rendered in 2D space requires X and Y coordinates
     *
     * @param mainWindow    the window in which the figure will be rendered
     * @param x             the x coordinate
     * @param y             the y coordinate
     */
    Renderable2D( MainWindow* mainWindow, float x = 0.0, float y = 0.0 ) : Renderable( x, y, 0.0 ), m_mainWindow(mainWindow), m_r(1.0), m_g(0.0), m_b(0.0) {}

    /**
     * @brief A simpler constructor, with default values set to 0 and 0 for the m_position
     *
     * @param mainWindow
     */
    Renderable2D( MainWindow* mainWindow ) : Renderable2D( mainWindow, 0.0, 0.0 ) {}

    ~Renderable2D() {}

    virtual void render() = 0;

    /*
     * @brief Sets the color of the renderable in RGB
     *
     * @param r the red value of the color
     * @param g the green value of the color
     * @param b the blue value of the color
     **/

    void setColor(float r, float g, float b);

protected:

    /*
     * @brief A function that prepares OpenGL to be able to render 2D object
     **/

    void prepareOrthoprojection();

    /*
     * @brief A function that enables to render a 3D object again
     **/

    void endOrthoprojection();

    //the window in which the Renderable is rendered
    MainWindow* m_mainWindow;

    //color values of the Renderable
    float m_r;
    float m_g;
    float m_b;
};

} //namepsace asteroids

#endif