#include "Renderable2D.hpp"

namespace asteroids
{

/**
 * @brief A small class to represent the 2D representation of a circle
 *
 */
class Circle : public Renderable2D
{
public:

    /**
     * @brief   Create a Circle object, and give it coordinates for its position and the segments in which it should be split in
     *
     * @param mainWindow    The window where the circle will be drawin in
     * @param x             The X coordinate
     * @param y             The Y coordinate
     * @param radius        The radius of the circle
     * @param segments      The segments in which we split the circle in order to draw it (since we cannot draw a PERFECT cirlce)
     */
    Circle(MainWindow* mainWindow, float x = 0.0, float y = 0.0, float radius = 1.0, int segments = 10) : Renderable2D(mainWindow, x, y), m_radius(radius), m_segments(segments){}

    /**
     * @brief The basic render functionality so we can see our circle
     *
     */
    void render();

private:
    /**
     * @brief The circle's radius
     *
     */
    float m_radius;

    /**
     * @brief The segments that compose the circle
     *
     */
    int m_segments;
};

}