#include "Rectangle.hpp"
#include <GL/glew.h>

namespace asteroids
{

void Rectangle::render()
{
    prepareOrthoprojection();

    glBegin(GL_LINE_LOOP);
    glVertex2d(m_position.x, m_position.y);
    glVertex2d(m_position.x + m_w, m_position.y);
    glVertex2d(m_position.x + m_w, m_position.y + m_h);
    glVertex2d(m_position.x, m_position.y + m_h);
    glEnd();

    endOrthoprojection();
}

}