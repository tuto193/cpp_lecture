#ifndef RENDERABLE3D_HPP
#define RENDERABLE3D_HPP

#include "Renderable.hpp"

namespace asteroids
{

/**
 * @brief A small abstract class that represents all (?) figures that have 3 Dimensions
 *
 */
class Renderable3D : public Renderable
{
public:

    /**
     * @brief the default constructor makes sure that we have an object with it's X, Y
     * and Z coordinates
     *
     * @param x the x coordinate
     * @param y the y coordinate
     * @param z the z coordinate
     */
    Renderable3D( float x = 0.0, float y = 0.0, float z = 0.0 ) : Renderable( x, y, z ) {}

    ~Renderable3D() {}

    virtual void render() = 0;
};

} //namepsace asteroids

#endif