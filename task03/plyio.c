/*
 *  plyio.c
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */
#include "plyio.h"

#define BUFF_SIZE 128
#define N_ELEMENTS 2

/**
 * @brief returns 1 if the file header read is valid. 0 if it doesn't match the loadply format for
 * reading
 *
 * @param file name of the file to be checked
 * @return int the validity of the check. 1 if ok, 0 if not.
 */
int parse_header( char *file )
{
    int element_count = N_ELEMENTS;
    char elementCheck[] = "element";
    char end_header_check[] = "end_header";
    char readString[BUFF_SIZE];

    FILE *model_file = fopen( file, "rb" );
    if( model_file != NULL )
    {

        /*  Make sure that we are dealing with a proper PLY File*/
        /*  First line: Title  */
        fgets( readString, BUFF_SIZE, model_file );
        if( strncmp( readString, "ply", 3 ) != 0 )
        {
            printf( "File format error: not a PLY File\n" );
            fclose( model_file );
            return 0;
        }
        /*  Second Line: Format specifier*/
        fgets( readString, BUFF_SIZE, model_file );
        if( strncmp( readString, "format", 6 ) != 0 )
        {
            printf( "File format error: no format specified\n" );
            fclose( model_file );
            return 0;
        }
        /*  Get the number of vertices  */
        do
        {
            /*  Read one line at the time*/
            fgets( readString, BUFF_SIZE, model_file );
        }
        while( strncmp(readString, elementCheck, 7) != 0 && *readString != EOF );
        if( *readString == EOF )
        {
            printf( "Error: No property found\n" );
            fclose( model_file );
            return 0;
        }

        /*  Check next element  */
        do
        {
            /*  Read one word at the time again */
            fgets( readString, BUFF_SIZE, model_file );
            element_count--;
        }
        while( strncmp( readString, elementCheck, 7 ) != 0 && *readString != EOF && element_count > 0 );
        if( *readString == EOF )
        {
            printf( "Error: no next property was found\n" );
            fclose( model_file );
            return 0;
        }
        if( element_count != 0 )
        {
            printf( "Error: amount of elements not maching\n" );
        }

        /*  Check until the end of our header    */
        do
        {
            fgets( readString, BUFF_SIZE, model_file );
        }
        while( strncmp( readString, end_header_check, 7) != 0 && *readString != EOF );

        if( *readString == EOF )
        {
            printf( "Error: header ended, and no values read\n" );
            fclose( model_file );
            return 0;
        }

        if( element_count != 0 )
        {
            printf( "Amount of elements to read doesn't match at end of header\n" );
            fclose( model_file );
            return 0;
        }
        fclose(model_file);
        return 1;
    }
    /*  This is just here so there are no warnings, since this function will be
        called only within another function that already checks that the file indeed exists
        */
    return 0;
}

void loadply(char *file, Model *model)
{
	/*++++++++++++++++++++++++++++++++++++++++++*/
	/* Implement code to load ply file here     */
	/*++++++++++++++++++++++++++++++++++++++++++*/
    float *vertices;
    int n_faces, n_vertices, i;
    int *faces;
    int header;
    char readString[BUFF_SIZE];
    char *pFaceVertices;
    char *pDummy;
    char elementCheck[] = "element";
    char end_header_check[] = "end_header";

    header = parse_header( file );
    FILE *model_file = fopen( file, "rb" );
    if( model_file != NULL && header )
    {
        /*  Get the number of vertices  */
        do
        {
            /*  Read one line at the time*/
            fgets( readString, BUFF_SIZE, model_file );
        }
        while( strncmp(readString, elementCheck, 7) != 0 && *readString != EOF );

        /*  Read the number of vertices needed  */
        pDummy = strtok( readString, " " );
        /*  The last lap of this loop ensures that we get the int at the end    */
        while( pDummy != NULL )
        {
            n_vertices = atoi( pDummy );
            pDummy = strtok( NULL, " " );
        }

        /*  Get the number of faces */
        do
        {
            /*  Read one word at the time again */
            fgets( readString, BUFF_SIZE, model_file );
        }
        while( strncmp( readString, elementCheck, 7 ) != 0 && *readString != EOF );

        /*  Read the number of faces    */
        pDummy = strtok( readString, " " );
        /*  The last lap of this loop ensures that we get the int at the end    */
        while( pDummy != NULL )
        {
            n_faces = atoi( pDummy );
            pDummy = strtok( NULL, " " );
        }

        /*  Read until the end of our header    */
        do
        {
            fgets( readString, BUFF_SIZE, model_file );
        }
        while( strncmp( readString, end_header_check, 7) != 0 && *readString != EOF );

        /*  Initialize our both arrays with the values we got   */
        /*  Each Vertex has 3 coordinates   */
        vertices = (float*) malloc(3*n_vertices *sizeof(float) );
        /*  Each Triangle-face consists of 3 vertices    */
        faces = (int*) malloc(3*n_faces *sizeof(int) );

        /*  Read all our vertices into our array    */
        /*  I'm 0% sure that this is going to work  */
        fread( vertices, sizeof(float), 3*n_vertices, model_file );

        /*  Read all our faces into the array, but taking into account the uchar
            that is the amunt of vertices that there is per face   */
        pFaceVertices = (char*)malloc(1*sizeof(char));
        for( i = 0; i < n_faces * 3; i += 3 )
        {
            /*  This rows start with a number that gives the amount of
            indices per row */
            fread( pFaceVertices, sizeof(char), 1, model_file );
            fread( faces + i, sizeof(int), *pFaceVertices, model_file );
            /*  Flip the values of the ints that we read separately */
        }

        free(pFaceVertices);
        model->vertexBuffer = vertices;
        model->indexBuffer  = faces;
        model->numFaces     = n_faces;
        model->numVertices  = n_vertices;
        fclose(model_file);
    }
    else
    {
        /*  The File couldn't be opened*/
        model->vertexBuffer = NULL;
        model->indexBuffer = NULL;
        model->numFaces = 0;
        model->numVertices = 0;
    }
}
