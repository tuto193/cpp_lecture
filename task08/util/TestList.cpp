#include "List.hpp"

int main(){

    asteroids::List<int> l;

    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);
    l.push_back(3);


    for(auto i: l) {
        std::cout << i << std::endl;
    }

    return 0;
}