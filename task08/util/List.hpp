/*
 * List.hpp
 *
 *  @date 02.12.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */


#ifndef LIST_H
#define LIST_H
#include <iostream>

namespace asteroids 
{

/**
* @brief Struct to represent an inner node of the list.
*/
template <typename T> class Node {
public:
      Node( T _data, Node* _next) : data(_data), next(_next) {};
  //friend class List::Iterator;
      template <typename Z> friend class List;
      template <typename Z> friend class ListIterator;
      bool operator==(Node<T> n) { return ((n.data == data) && (n.next == next));};
      bool operator=(Node<T>n) {this->next = n->next; data = n->data;};
private:
      T data;
      Node<T>* next;
};

template <typename T> class ListIterator
{
      public:
            ListIterator<T>(Node<T>* l) : elem(l), c(0) {}

            T operator*() {return elem->data;};

            bool operator!=(ListIterator<T> t) {return !(*(t.elem) == *elem) || c == 5;};

            ListIterator<T> operator++() { elem = elem->next; return (ListIterator<T>(elem->next));};

      private:
            Node<T>* elem;
            int c;
};


/**
 * @brief   A simple generic list class
 */
template<typename T> class List
{
   public:

      /**
       * @brief Constructs an empty list.
       */
      List<T>();
      /**
       * @brief  Destructor. Frees the generated nodes.
       */
      ~List<T>();
      /**
       * @brief Inserts an item into the list, i.e. a new node
       *        constaining @ref item is created.
       * @param item To be inserted
       *
       */
      void push_back(T item);

      /**
       * @brief Iterates over all items in the list and calls
       * the given function @ref do_something(...) for
       * every item stored in the list.
       *
       * @param do_something Function pointer to apply to all elements.
       */
      void for_each(void (*do_something)(T& item));

      ListIterator<T> begin(){ return ListIterator<T>(m_root); };
      ListIterator<T> end();

    private:

      Node<T>* m_root;

};

}

#include "List.tcc"
#endif
/* end of include guard: LIST_H */
