#include "TextureFactory.hpp"
#include "util/Util.hpp"
#include <iterator>
#include "io/ReadTGA.hpp"
#include "io/ReadTIFF.hpp"
#include "io/ReadPPM.hpp"
#include "io/ReadJPG.hpp"


namespace asteroids
{

TextureFactory* TextureFactory::m_instance = 0;
std::string TextureFactory::basepath = "";
std::map<std::string, Texture*> TextureFactory::textures;

TextureFactory* TextureFactory::getInstance()
{
    if( TextureFactory::m_instance == 0 )
    {
        TextureFactory::m_instance = new TextureFactory();
    }
    return TextureFactory::m_instance;
}

void TextureFactory::setBasePath(const std::string& base)
{
    basepath = base;
}

Texture* TextureFactory::getTexture(const std::string& filename)
{
    std::string fullpath = basepath + filename;
    std::map<std::string, Texture*>::iterator it = textures.find(fullpath);
    if(it == textures.end())
    {
        std::string extension = GetExtensionFromFileName(filename);
        BitmapReader* bmp;
        if(extension.compare("tga") == 0)
        {
            bmp = new ReadTGA(fullpath);
        }
        //else if(extension.compare("tif") == 0 || extension.compare("tiff") == 0)
        //{
        //    bmp = new ReadTIFF(fullpath);
        //}
        else if(extension.compare("ppm") == 0)
        {
            bmp = new ReadPPM(fullpath);
        }
        else if(extension.compare("jpg") == 0)
        {
            bmp = new ReadJPG(fullpath);
        }

        Texture* newTexture = new Texture(bmp->getPixels(), bmp->getWidth(), bmp->getHeight());
        textures[fullpath] = newTexture;
        return newTexture;
    }
    else
    {
        return it->second;
    }
}

} // namespace asteroids