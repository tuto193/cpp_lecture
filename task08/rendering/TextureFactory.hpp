#ifndef TextureFactory_hpp
#define TextureFactory_hpp
#include <string>
#include <map>
#include "Texture.hpp"

namespace asteroids
{

class TextureFactory
{
public:

    /**
     * @brief       Set the basepath where to search for pic files for our factory
     *
     * @param base  the path to an existing directory
     */
    void setBasePath(const std::string& base);

    /**
     * @brief               Returns a Texture. If it was already loaded, return the loaded one,
     *                      if it wasn't, create a new one, and save it in our map for the future
     *
     * @param filename      the name of the texture
     * @return Texture*     the loaded texture
     */
    Texture* getTexture(const std::string& filename);

    static TextureFactory* getInstance();

private:

    /// Private Constructor to ensure singleton behaviour
    TextureFactory() {}

    /// Same as with the constructor
    ~TextureFactory() {}

    static TextureFactory* m_instance;

    /// The Basepath so search for pic files
    static std::string basepath;

    /// The Map conaitning all of our textures
    static std::map<std::string, Texture*> textures;

}; // TextureFactory

} // namespace asteroids

#endif // TextureFactory_hpp