/*
 *  AsteoridField.cpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "AsteroidField.hpp"
#include "math/Randomizer.hpp"
#include "rendering/Asteroid.hpp"
#include "TriangleMeshFactory.hpp"

namespace asteroids
{



AsteroidField::AsteroidField(int n, const std::string& filename, float rangemax, float sizemin, float sizemax)
{
    float scale_size;
    float x, y, z;
    float scale_back;
    TriangleMeshFactory* m_instance = TriangleMeshFactory::getInstance();
 	// Generate asteroids
	for(int i = 0; i < n; i++)
	{
        //  Scale the Asteroids so they are a bit different
        scale_size = 0;
        srand( time(0) );
        scale_size = float( std::rand() % (int)sizemax) + sizemin;

        // Generate Starting positions at random
        x = std::rand() % (int)rangemax/3;
        y = std::rand() % (int)rangemax/3;
        z = std::rand() % (int)rangemax/3;

        Vector3f start_position = Vector3f( x, y, z );
        scale_back = sqrt( x*x + y*y + z*z) / rangemax;
        if( scale_back > 1 )
        {
            start_position * (1/scale_back);
        }

	   /// TODO: Get mesh from class TriangleMeshFactory and add new
	   /// Asteroid to internal list

		TriangleMesh* newA = m_instance->getTriangleMesh(filename);
        Asteroid to_add = Asteroid( newA, start_position, scale_size );
		field.push_back( to_add );
	}
}

AsteroidField::~AsteroidField()
{
	//asteroids.for_each(deleteAsteroid);
/*     for( auto t : field )
    {
        if( t )
        {
            delete t;
        }
    } */
}

void AsteroidField::render()
{
	for(auto& t : field)
	{
		t.render();
	}
}

} // namespace asteroids
