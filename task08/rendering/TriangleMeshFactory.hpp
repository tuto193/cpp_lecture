#ifndef TriangleMeshFactory_hpp
#define TriangleMeshFactory_hpp

#include <string>
#include <map>
#include "TriangleMesh.hpp"
#include "util/Util.hpp"
#include "io/MeshReader.hpp"
#include "io/Read3DS.hpp"
#include "io/ReadPLY.hpp"
#include <iterator>

namespace asteroids
{

class TriangleMeshFactory
{
public:

    /**
     * @brief       set the basepath where to look for our Picture files
     *
     * @param base  the name of the directory for the path
     */
    void setBasePath(const std::string& base);

    /**
     * @brief                   get a TriangeMesh from our map. Create a new one
     *                          if it didn't exist before
     *
     * @param filename          The name of the Mesh to find.
     * @return TriangleMesh*    the Reference to the found/created mesh
     */
    TriangleMesh* getTriangleMesh(const std::string& filename);

    static TriangleMeshFactory* getInstance();

private:

    /// our private constructor for singleton behaviour
    TriangleMeshFactory() {}

    /// a private destructor to deal with the deliting as well as singleton
    ~TriangleMeshFactory() {}

    /// The basepath for our files
    static std::string basepath;

    /// The map where all our meshes are located
    static std::map<std::string, TriangleMesh*> meshes;

    static TriangleMeshFactory* m_instance;

}; // TriangleMeshFactory

} //namespace asteroids

#endif