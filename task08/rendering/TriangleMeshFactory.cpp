#include "TriangleMeshFactory.hpp"


namespace asteroids
{

TriangleMeshFactory* TriangleMeshFactory::m_instance = 0;

std::string TriangleMeshFactory::basepath = "";

std::map<std::string, TriangleMesh*> TriangleMeshFactory::meshes;

TriangleMeshFactory* TriangleMeshFactory::getInstance()
{
    if( TriangleMeshFactory::m_instance == 0 )
    {
        TriangleMeshFactory::m_instance = new TriangleMeshFactory();
    }

    return TriangleMeshFactory::m_instance;
}


void TriangleMeshFactory::setBasePath(const std::string& base)
{
    basepath = base;
}

TriangleMesh* TriangleMeshFactory::getTriangleMesh(const std::string& filename)
{
    std::string fullpath = TriangleMeshFactory::basepath + filename;
    std::map<std::string, TriangleMesh*>::iterator it = TriangleMeshFactory::meshes.find(fullpath);
    if(it == TriangleMeshFactory::meshes.end())
    {
        std::string extension = GetExtensionFromFileName(filename);
        MeshReader* mr;
        if(extension.compare("3ds") == 0)
        {
            mr = new Read3DS(fullpath);
        }
        else if(extension.compare("ply") == 0)
        {
            mr = new ReadPLY(fullpath);
        }

        TriangleMesh* newMesh = new TriangleMesh( *mr->getMesh() );
        TriangleMeshFactory::meshes[fullpath] = newMesh;
        return newMesh;
    }
    else
    {
        return it->second;
    }
}

} // namespace asteroids