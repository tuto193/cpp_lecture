#include <vector>
#include <random>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <iterator>

struct adder : public std::unary_function<int, void >
{
    int sum;
    adder() : sum(0) {}
    void operator() (int x) { sum += x; }
};

int random_int(){
    return (std::rand() % 100 + 1);
}

std::vector<int> generate_int_vector( int length )
{
    srand(time(0));
    std::vector<int> vec(length);

    std::generate(vec.begin(), vec.end(), random_int);
    return vec;
}

int main()
{
    int size = 50;
    std::vector<int> values = generate_int_vector(size);
    adder result = for_each( values.begin(), values.end(), adder() );

    std::copy(values.begin(), values.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    std::cout << result.sum << std::endl;

    return 0;
}