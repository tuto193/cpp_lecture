#include <vector>
#include <random>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <iterator>

char random_char(){
   /*
     This should aready work for creating those random chars, since we are always
     dealing with values in ranges 32-126

     Values from 0 to 32 (the first 33) are "invisible" and the 127th value is DEL,
     so we just need the ones in between
     */
    int newChar = (std::rand() % 95) + 32;
    return (char) newChar;
    /* char char_array[] =
    {'0','1','2','3','4',
    '5','6','7','8','9',
    'A','B','C','D','E','F',
    'G','H','I','J','K',
    'L','M','N','O','P',
    'Q','R','S','T','U',
    'V','W','X','Y','Z',
    'a','b','c','d','e','f',
    'g','h','i','j','k',
    'l','m','n','o','p',
    'q','r','s','t','u',
    'v','w','x','y','z'
    }; */
}

int main(int argc, char** argv)
{

    srand(time(0));
    std::vector<std::string> vec;
    int i, ii;
    std::string to_add;
    char r_char;
    for(i = 0; i < 100; i++)
    {
        to_add = "";
        int length = rand() % 5 + 5;
        for( ii = 0; ii < length; ii++ )
        {
            to_add += random_char();
        }
        vec.push_back(to_add);
    }

    std::copy(vec.begin(), vec.end(), std::ostream_iterator<std::string>(std::cout, " "));
    std::cout << std::endl;
}
