#include <vector>
#include <random>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <iterator>

int random_int(){
    return (std::rand() % 100 + 1);
}

std::vector<int> generate_int_vector( int length )
{
    srand(time(0));
    std::vector<int> vec(length);

    std::generate(vec.begin(), vec.end(), random_int);
    return vec;
}

int main(int argc, char** argv)
{
    std::vector<int> vec = generate_int_vector( 100 );
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    return 0;
}