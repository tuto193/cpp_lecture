#include "Bullet.hpp"
#include <chrono>
#include <iostream>

namespace asteroids
{

    void Bullet::run()
    {
        int i;
        for(i = 0; i < m_lifetime; i++)
        {
            this->m_sphere.setPosition(m_sphere.getPosition() - m_direction);
            this->render();
            std::this_thread::sleep_for(std::chrono::duration<int, std::micro>(1000));
        }
        std::lock_guard<std::mutex> guard( death_mutex );
        m_alive = false;
    }

    void Bullet::start()
    {
        m_alive = true;
        m_thread = new std::thread( &Bullet::run, this );
    }

    void Bullet::stop()
    {
        try
        {
            m_thread->join();
        } catch( ... )
        {
            std::cout << "Thread " << m_thread->get_id() << " couln't terminate properly" << std::endl;
        }
    }

    bool Bullet::isAlive()
    {
        return m_alive;
    }

    void Bullet::render()
    {
        m_sphere.render();
    }

    Bullet::~Bullet()
    {
        delete m_thread;
    }

} // namespace asteroids