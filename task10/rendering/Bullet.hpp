/*
 *  Bullet.hpp
 *
 *  Created on: Jan. 06 2019
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2019 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef BULLET_HPP_
#define BULLET_HPP_

#include "../math/Vector.hpp"
#include "Sphere.hpp"
#include <thread>
#include <mutex>
#include <atomic>

namespace asteroids
{

/**
 * @brief Renders a Bullet
 */
class Bullet
{

public:

    using Ptr = std::shared_ptr<Bullet>;

    /**
     * @brief Contructor. Build a bullet on the given Fighter's
     *                      position. The bullet will move on the
     *                      given axis.
     * @param   fighter_position   Position of the fighter that shoots this bullet
     * @param   fighter_axis   Axis the bullet will move on
     */
    Bullet(const Vector3f& fighter_position, const Vector3f fighter_axis)
        : m_direction(fighter_axis), m_alive(false), m_sphere(Sphere(fighter_position, 10)) {};

    ~Bullet();

    /**
     * @brief Moves the bullet until it's lifetime is over.
     */
	void run();

	/**
	 * @brief Starts bullet movement
	 */
	void start();


	/*
	 * @brief Stops bullet movement
     */
	void stop();

    /**
     * @brief Renders the bullet via glutSolidSphere.
     */
	void render();

    /**
     * @brief Returns the status of this bullet.
     * @return false, if the bullet's lifetime is over and true otherwise
     */
	bool isAlive();

private:

    // TODO: ADD NEEDED CLASS ELEMENTS TO IMPLEMENT
    // THE REQUIRED FUCTIONALITIES

    /// Lifetime
    const int m_lifetime = 1000;

    /// Direction
    const Vector3f m_direction;

    /// Alive status
    std::atomic<bool> m_alive;

    /// Sphere objet to render the bullet
    Sphere m_sphere;

    /// Internal thread
    std::thread* m_thread;

    // Our internal mutex to handle death of the threads
    std::mutex death_mutex;

};

} // namespace asteroids

#endif /* BULLET_HPP_ */
