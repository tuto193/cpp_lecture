#!/usr/bin/env bash

OUTPUT=$($1)
ITER=1

while [ "$OUTPUT" == "Sum auf squares up to 20 is = 2870" ]
do 
    if [ $(( $ITER % 100)) == 0 ]; then echo "(Run $ITER) No errors so far ..."; fi
    ITER=$(( $ITER + 1))
    OUTPUT=$($1)
done
echo "(Run $ITER) Output was: $OUTPUT"