#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
using namespace std;

std::mutex m;

void pow2(int& square_sum, int x)
{   
    std::lock_guard<std::mutex> lock_sum(m);
    square_sum += x * x;
}

int main() 
{
    int square_sum = 0;
    vector<thread> threads;
    for (int i = 1; i <= 20; i++)
    {
        threads.push_back(thread(&pow2, std::ref(square_sum), i));
    }

    vector<thread>::iterator it;
    for (it = threads.begin(); it != threads.end(); it++)
    {
       (*it).join();
    }
    cout << "Sum auf squares up to 20 is = " << square_sum << endl;
    return 0;
}
