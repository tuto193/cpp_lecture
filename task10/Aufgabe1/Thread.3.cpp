#include <iostream>
#include <vector>
#include <future>
using namespace std;

int pow2(int x)
{   
    return x * x;
}

int main() 
{
    int square_sum = 0;
    vector<future<int>> threads;
    for (int i = 1; i <= 20; i++)
    {
        threads.push_back(std::async(&pow2, i));
    }

    vector<future<int>>::iterator it;
    for (it = threads.begin(); it != threads.end(); it++)
    {
       square_sum += (*it).get();
    }
    cout << "Sum auf squares up to 20 is = " << square_sum << endl;
    return 0;
}
