/**
 *  @file Vector.tcc
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include<cassert>

namespace asteroids
{

template<typename T, int L> Vector<T, L>::Vector( T x, T y, T z )
{
	// Default values
    static_assert( L == 2 || L == 3, "L is not a valid value " );
    this->m[0] = T(x);
    this->m[1] = T(y);

    if( L == 3 ) {
        this->m[2] = T(z);
    }
}


template<typename T, int L>
Vector<T, L>::~Vector()
{
}

template<typename T, int L>
void Vector<T, L>::normalize()
{
	// Normalize the Vector<T, L>
	float mag2 = 0;
    int i;
    for( i = 0; i < L; i++ )
    {
        mag2 += (m[i] * m[i] );
    }

	if (fabs(mag2 - 1.0f) > 0.00001)
	{
		float mag = sqrt(mag2);
        for( i = 0; i < L; i++ )
        {
            m[i] =T((m[i] / mag) );
        }
	}
}

template<typename T, int L>
Vector<T, L> Vector<T, L>::operator+(const Vector<T, L>& vec) const
{
    T values[] = { 0.0, 0.0, 0.0 };
    int i;
	// Add value to value
    for( i = 0; i < L; i++ )
    {
        values[i] = m[i] + vec[i];
    }
	return Vector(values[0], values[1], values[2]);
}

template<typename T, int L>
Vector<T, L> Vector<T, L>::operator-(const Vector<T, L>& vec) const
{
    T values[] = { 0.0, 0.0, 0.0 };
    int i;
	// Add value to value
    for( i = 0; i < L; i++ )
    {
        values[i] = m[i] - vec[i];
    }
	return Vector(values[0], values[1], values[2]);
}

template<typename T, int L>
T Vector<T, L>::operator[](const int& index) const
{
	// Get the wanted value
	if(index == 0)
	{
		return m[0];
	}

	if(index == 1)
	{
		return m[1];
	}

    if( L == 3 )
    {
        if(index == 2)
        {
            return m[2];
        }
    }

    throw std::invalid_argument( "Error: Index out of bounds" );

	return 0;
}

template<typename T, int L>
T& Vector<T, L>::operator[](const int &index)
{

    if(index == 0)
    {
        return this->m[0];
    }

    if(index == 1)
    {
        return this->m[1];
    }

    if( L == 3 )
    {
        if(index == 2)
        {
            return this->m[2];
        }
    }

    throw std::invalid_argument( "Error: Index out of bounds");

    // Return a defualt reference -> later throw exception!!
    return this->m[0];
}

template<typename T, int L>
T Vector<T, L>::operator*(const Vector<T, L>& vec) const
{
    int i;
    T result = 0;
    // Calculate the result
    for( i = 0; i < L; i++ )
    {
        result += (m[i] * vec[i] );
    }
    return result;
}

template<typename T, int L>
Vector<T, L> Vector<T, L>::operator*(T scale) const
{
    T result[] = { 0.0, 0.0, 0.0 };
    int i;
	// Calculate the result
    for( i = 0; i < L; i++ )
    {
        result[i] = m[i] * scale;
    }
	return Vector(result[0], result[1], result[2]);
}

template<typename T, int L>
void Vector<T, L>::operator+=(const Vector<T, L>& v)
{
    int i;
	// Add value to value
    for( i = 0; i < L; i++ )
    {
        m[i] += v[i];
    }
}

} // namespace asteroids
