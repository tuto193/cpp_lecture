#include "List.hpp"

void printInt(int& i) {
    std::cout << i << std::endl;
}

int main(int argc, char** argv)
{
    asteroids::List<int> intList;

    int i;
    for(i = 0; i < 10; i++) {
        intList.insert(i);
    }
    void (*ptr)(int&);
    ptr = &printInt;
    intList.for_each(ptr);

    return 0;
}