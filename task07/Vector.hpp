/**
 *  @file Vector.hpp
 *
 *  @date 18.11.2018
 *  @author Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
#include <cmath>

namespace asteroids
{

/**
 * @brief       class representing a 2d or 3d vector of any type
 *
 * @tparam T    the type of Vector
 * @tparam L    the length of the Vector
 */
template<typename T, int L> class Vector
{

public:

    /**
     * @brief       Initialize our Vector of Defined length, with it's first 3 Values set to x, y and z.
     *              If the Vector has just 2 dimensions, z will just be ignored and set to 0 instead
     *
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param z     the z coordinate (if at all)
     */
    Vector<T, L>( T x, T y, T z );

    /**
     * @brief Construct a 2D Vector and set the 3rd value to 0.0
     *
     * @param x     the x coordinate
     * @param y     the y coordinate
     */
    Vector<T, L>( T x, T y ) : Vector( x, y, T(0) ) {}

    /**
     * @brief Initializes a Vector of length L, with all it's values set to 0;
     *
     */
    Vector<T, L>() : Vector( T(0), T(0) ) {}

    /**
     * @brief Destroy the Vector< T,  L> object, and the array where all the values where being stored at
     *
     */
    ~Vector<T, L>();

    /**
     * @brief Normalize the Vector
     *
     */
    void normalize();

    /**
     * @brief                   Add two vectors (value-wise)
     *
     * @param vec               the other vector to be added to this one
     * @return Vector<T, L>     the resulting vector
     */
    Vector<T, L> operator+(const Vector<T, L>& vec ) const;

    /**
     * @brief               same as the addition, but with the other vector multiplied by -1
     *
     * @param vec           the other vector
     * @return Vector<T, L> the resulting vector
     */
    Vector<T, L> operator-(const Vector<T, L>& vec ) const;

    /**
     * @brief                   Scale a Vector's values
     *
     * @param scale             the scalar with which to scale the vector
     * @return Vector<T, L>     the resulting scaled vector
     */
    Vector<T, L> operator*(const T scale ) const;

    /**
     * @brief       get the scalar product of two vectors
     *
     * @param vec
     * @return T
     */
    T operator*( const Vector<T, L>& vec ) const;

    /**
     * @brief           get the value inside the vector at the desired index.
     *                  if it's not a valid index, throw an invalid_argument Exception
     *
     * @param index     the index of the desired value
     * @return T        the value of at the desired index
     */
    T operator[]( const int& index ) const;

    /**
     * @brief           Same as with the const version of this, but returns an actual
     *                  reference to the value, which can be modified
     *
     * @param index     the index of the desired value
     * @return T&       a reference to the desired value
     */
    T& operator[]( const int& index );

    /**
     * @brief       the fast version of the sum of two vectors
     *
     * @param v     the other vector to be added
     */
    void operator+=( const Vector<T, L>& v );

private:

    /// The array, where the coordinates of the vectors are kept
    T m[L];
};

// /**
//  * @brief   Vector representation with three floats for OpenGL.
//  *
//  * @todo	Replace this float-specific implementation for three coordinates
//  * 			with a generic version called 'Vector' that supports 2 to 3 coordinates
//  * 			with different types. Here we will use float and int for space
//  * 			and screen coordinates respectively.
//  */
// class Vector3f {

typedef Vector<float, 3> Vector3f;

typedef Vector<int, 2> Vector2i;

// public:

// 	/**
// 	 * @brief   Construcs a default Vector3f object
// 	 */
// 	Vector3f();

// 	/**
// 	 * @brief   Construcs a Vector object with given values
// 	 * @param x x-value
// 	 * @param y y-value
// 	 * @param z z-value
// 	 */
// 	Vector3f(float x, float y, float z);

// 	/**
// 	 * @brief   Normalize a Vector
// 	 */
// 	void normalize();

// 	/**
// 	 * @brief   Defines the vector addition
// 	 * @param vec vector
// 	 * @return vector
// 	 */
// 	Vector3f operator+ (const Vector3f& vec) const;

// 	/**
// 	 * @brief   Defines the vector subtraction
// 	 * @param vec vector
// 	 * @return vector
// 	 */
// 	Vector3f operator- (const Vector3f& vec) const;

// 	/**
// 	 * @brief   Construcs the scalar multiplication
// 	 * @param scale scalar
// 	 * @return vector
// 	 */
// 	Vector3f operator* (const float scale) const;

// 	/**
// 	 * @brief   Defines the vector multiplication
// 	 * @param vec vector
// 	 * @return result (as a float)
// 	 */
// 	float  operator* (const Vector3f& vec) const;

// 	/**
// 	 * @brief   Defines the access to a Vector value
// 	 * @param index wanted value
// 	 * @return vectorentry (as a float)
// 	 */
// 	float operator[] (const int& index) const;

//     /**
//      * @brief   Defines the access to a Vector value
//      * @param index wanted value
//      * @return vectorentry (as a float)
//      */
//     float& operator[] (const int& index);

// 	/**
// 	 * @brief   Defines the fast notation of vector addition
// 	 * @param v vector
// 	 */
// 	void operator+= (const Vector3f& v);

// 	/**
// 	 * @brief   The three values of a vector
// 	 */
// 	float x, y, z;
// };

// /***
//  * TODO: 	This is just a plain simple replacement for
//  * 			the generic version you are going to implement
//  * 			in this exercise that just gives us the needed index operator
//  * 		    based coordinate access. Replace it with a suitable
//  * 			typedef of your generic implementation.
//  **/
// struct Vector2i
// {
// 	Vector2i(int x, int y)
// 	{
// 		m[0] = x;
// 		m[1] = y;
// 	}

// 	int& operator[](const int& index)
// 	{
// 		if(index >= 0 && index <= 2)
// 		{
// 			return m[index];
// 		}
// 		throw std::invalid_argument("");
// 	}

// 	int operator[](const int& index) const
// 	{
// 		if(index >= 0 && index <= 2)
// 		{
// 			return m[index];
// 		}
// 		throw std::invalid_argument("");
// 	}

// 	int m[2];
// };

} // namespace asteroids

#include "Vector.tcc"

#endif
