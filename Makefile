# Makefile template
CC=gcc
# Compiler flags
CFLAGS=-Wall #-static -DNDEBUG#-I

# Dedicated directory for header files
#IDIR=../include

DEPS = # headers separated with spaces

# object files
OBJ =
LOBJ =

# Libraries
LIBS=
LFLAG=

# Program Name
PROG_NAME =

#our desired program name
$(PROG_NAME): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LFLAG) $(LIBS)

# Make object files out of every source file
%.o: %.c %.h
	$(CC) -c -o $@ $< $(CFLAGS)

# Remove all object, buffer and program files
clean:
	rm -f *.o *~ core $(PROG_NAME)