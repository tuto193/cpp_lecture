#ifndef Mine_hpp
#define Mine_hpp

#include "Building.hpp"
#include "Enums.hpp"
#include <iostream>
#include <memory>

namespace asteroids
{

/**
 * @brief a simple class to represent a mine-building
 *
 */
class Mine : public Building
{
public:

    using Ptr = std::shared_ptr<Mine>;

    /**
     * @brief Construct a new Mine object
     *
     * @param p the plane that the Mine is being built on
     * @param t the type of buiding this is (a Mine)
     */
    Mine( ) : Building( BUILDINGTYPE::MINE ), t_rate(10) { }

    /**
     * @brief produce ore for the planet and save it on the planet
     *
     * @param rate at which ore is to be produced
     * @return int  the amount of ore produced
     */
    int produce(Planet* onPlanet) const;

    /**
     * @brief Destroy the Mine object
     *
     */
    ~Mine() { }

private:

    /// The rate at which this mine produces
    int t_rate;

};

}

#endif