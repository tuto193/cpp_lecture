/*
 * Planet.hpp
 *
 *  @date 18.03.2018
 *  @author Aeilko Luebsen
 */

#ifndef Planet_hpp
#define Planet_hpp

#include <string>
#include <list>
#include <memory>
#include "../math/Vector.hpp"
#include "Building.hpp"
#include "Ship.hpp"
#include "Enums.hpp"

namespace asteroids {

/*
 * @brief A class implementation that represents a planet in the asteroids game.
 * It manages the ore on it, empties the mine and builds the ships in the yard.
 */

class Player;
class Ship;
class Building;

class Gameworld;

class Planet
{

public:

    using Ptr = std::shared_ptr<Planet>;

    /*
     * @brief Creates a planet with a name and a random amount of ore on it.
     */
    Planet(std::string name, int x, int y, int startOre);

    /**
     * @brief default constructor
     *
     */
    Planet() : Planet( "Default" , 0, 0, 0) {}

    /*
     * @brief Builds a ship or mines the ore on the planet.
     */
    void produce();

    /*
     * @brief Builds a building on the planet.
     */
    void build(BUILDINGTYPE type);

    /*
     * @brief Tears down the building on the planet.
     */

    void tearDown();

    /*
     * @brief Returns the amount of ore that is already mined an on the planet.
     */
    int getOreMined() const { return oreMined; }

    /*
     * @brief Returns the amount of ore that is still inside the mines.
     */
    int getOreToMine() const { return oreToMine; }

    /*
     * @brief Returns all Ships currently on the planet.
     */
    const std::list<std::shared_ptr<Ship>>& getShips() const { return ships; }

    /*
     * @brief Returns the number of transporters on the planet.
     */
    int getTransporterNumber() const {
        return transporterNumber;
    }

    /*
     * @brief Returns the number of fighters on the planet.
     */
    int getFighterNumber() const {
        return fighterNumber; }

    /*
     * @brief Transports a fighter from this planet to the planet toPlanet.
     */
    void transportShip(std::shared_ptr<Planet> toPlanet);

    /*
     * @brief Transports amount of ore from this planet to the planet toPlanet.
     */
    void transportOre(std::shared_ptr<Planet> toPlanet, int amount);

    /*
     * @brief Receives a ship sent from another planet.
     */
    void receiveShip(std::shared_ptr<Ship> receiveShip);

    /*
     * @brief Receives ore from another planet.
     */
    void receiveOre(int amount);

    /*
     * @brief Returns the name of this planet.
     */

    const std::string& getName() const { return name; }

    /*
     * @brief Returns a pointer to the current owner of the planet.
     */
    std::shared_ptr<Player> getOwner() const { return owner; }

    /*
     * @brief Sets the owner of this planet.
     */
    void setOwner(std::shared_ptr<Player> newowner) { owner = newowner; }

    /*
     * @brief Captures the planet for a player.
     */
    void capture(std::shared_ptr<Player> capturer);

    /*
     * @brief Adds an edge from this planet to another planet.
     */

    void addAdjacent(std::shared_ptr<Planet> other);

    /*
     * @brief Returns all the adjacent planets to this one.
     */

    const std::list<std::shared_ptr<Planet>>& getAdjacents() const { return adjacents; }

    /*
     * @brief Returns the position of this planet
     */

    Vector2i getPosition() { return position; }

    /*
     * @brief Sets the position of this planet.
     */

    void setPosition(Vector2i& newpos) { position = Vector2i(newpos); }

    /*
     * @brief Sets the position of this planet.
     */

    void setPosition(int x, int y) { position = Vector2i(x, y); };

    /*
     * @brief Sets the Gameworld this planet is part of
     */

    void setGameWorld(std::shared_ptr<Gameworld> gameWorld) { gw = gameWorld; }

    /*
     * @brief Returns wether this planet is the homeplanet of its owner
     */

    bool isHomePlanet() const
    {
        return m_homePlanet;
    }

    /*
     * @brief Sets this planet as the owners homeplanet 
     */

    void setHomePlanet(bool homePlanet);

    /**
     * @brief Get the Building attached to this planet
     *
     * @return std::shared_ptr<Building>  the building that stands on this planet at the moment
     */

    std::shared_ptr<Building> getBuilding() { return building; }

    /*
     * @brief Returns the Gameworld this planet belongs to
     */

    std::shared_ptr<Gameworld> getGameWorld() { return gw; }

    /*
     * @brief Returns the id of this planet
     */

    int getId() { return id; }

    /*
     * @brief Returns the name of this planet
     */

    std::string getName() { return name; }

    /*
     * @brief 
     */

    void wipe();

    void destroyTransporter();

    void destroyFighter();

    /**
     * @brief Destroy the Planet object
     *
     */
    ~Planet();


private:

    /// The current owner of the planet
    std::shared_ptr<Player> owner;

    /// true if this planet is the first planet of the owner, e.g. the home planet
    bool m_homePlanet;

    std::shared_ptr<Gameworld> gw;

    /// The current building on the planet. Either a shipyard or a mine.
    std::shared_ptr<Building> building;

    /// The rest of the natural ore still on the planet.
    int oreToMine;

    /// The already mined ore that is currently stored on the planet.
    int oreMined;

    /// A list of all the ships currently on the planet.
    std::list<std::shared_ptr<Ship>> ships;

    /// The name of the planet.
    std::string name;

    /// The number of transporters currently on the planet
    int transporterNumber;

    /// The number of fighters on the planet
    int fighterNumber;

    /// The id-number of this planet
    int id;

    /// The ID number generator counting up from zero
    static int lastID;

    /// The list of adjacent planets to this planet
    std::list<std::shared_ptr<Planet>> adjacents;

    /// The position of the planet
    Vector2i position;
};

}
#endif