#ifndef Enums_hpp
#define Enums_hpp

namespace asteroids

{

/**
 * @brief An enum to represent the different types of ships in this game
 */
enum SHIPTYPE
{
    FIGHTER,
    TRANSPORTER
};

/*
 * @brief An enum to represent the different types of buildings in this game
 */

enum BUILDINGTYPE
{
    MINE,
    SHIPYARD
};

/*
 * @brief An enum to represent the different states of who has won
 */

enum WINTYPE
{
    ME,
    ENEMY,
    NONE
};

}

#endif