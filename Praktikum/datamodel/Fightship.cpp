#include "FightShip.hpp"

namespace asteroids
{

int FightShip::getDamage()
{
    return damage;
}

int FightShip::getCurrHealth()
{
    return currHealth;
}

void FightShip::setCurrHealth( int newhealth )
{
    currHealth = newhealth;
}

int FightShip::getMaxHealth()
{
    return maxHealth;
}

}