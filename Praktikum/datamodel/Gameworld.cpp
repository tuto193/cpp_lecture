/*
 * Planet.hpp
 *
 *  @date 20.03.2018
 *  @author Aeilko Luebsen
 */

#include "Gameworld.hpp"
#include "Planet.hpp"
#include "Player.hpp"
#include "Shipyard.hpp"
#include "Mine.hpp"
#include <iostream>
#include <map>
#include <math.h>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <boost/filesystem.hpp>
#include "boost/filesystem/fstream.hpp"
#include "math/Randomizer.hpp"
#include <QDebug>
#include <QThread>

namespace asteroids
{

Gameworld::Gameworld(bool pl1) : isPlayerOne(pl1), winner(NONE)
{
    t_state = pl1;

    nextRound = false;
    // read the map file
    readMapFile("../models/01.map");

    connect(&NetworkInterface::instance(), &NetworkInterface::newPlanetUpdate, this, &Gameworld::gotNewPlanetUpdate);

    connect(&NetworkInterface::instance(), &NetworkInterface::turnSkipped, this, &Gameworld::turnSkip);

    connect(&NetworkInterface::instance(), &NetworkInterface::signalTransportOre, this, &Gameworld::gotNewOreTransport);

    connect(&NetworkInterface::instance(), &NetworkInterface::signalTransportFighter, this, &Gameworld::gotNewFighterTransport);

    connect(&NetworkInterface::instance(), &NetworkInterface::signalChangeProduction, this, &Gameworld::gotProductionChangeMode);

}

void Gameworld::readMapFile(std::string mapfile)
{
    boost::filesystem::path file(mapfile);
    boost::filesystem::ifstream stream(file);

    if (!stream)
    {
        throw "Mapfile not found or readable!";
    }

    // line var for reading each line in mapfile. reused multiple times!
    std::string line;
    int number_of_vertices;
    // indices of start planets of both players
    int startPlanet1, startPlanet2;

    // get first line and save to local variable
    std::getline(stream, line);
    std::istringstream in(line);

    in >> number_of_vertices;

    planets = std::vector<Planet::Ptr>(number_of_vertices);

    // read all positions
    for (int i = 0; i < number_of_vertices; i++)
    {
        std::getline(stream, line);

        std::string name;
        int x, y, startOre;

        // read name and coords of position
        std::istringstream in(line);
        in >> name >> x >> y >> startOre;

        // add to graph poperty_map
        Planet* to_add = new Planet( name, x, y, startOre );
        to_add->setGameWorld(Gameworld::Ptr(this));
        planets[i] = std::shared_ptr<Planet>( to_add ) ;

    }

    // shadow in for easier reading
    {
        std::getline(stream, line);
        std::istringstream in(line);
        in >> startPlanet1 >> startPlanet2;
    }

    // read edges
    while (std::getline(stream, line))
    {
        std::istringstream in(line);
        unsigned int start, end;

        in >> start >> end;

        planets[start]->addAdjacent( planets[end] );
        edges.push_back( edge( start, end ) );
    }

    planets[startPlanet1]->setHomePlanet(true);
    planets[startPlanet2]->setHomePlanet(true);
    if(isPlayerOne){
        me = std::make_shared<Player>(planets[startPlanet1]);
        enemy = std::make_shared<Player>(planets[startPlanet2]);    
    }
    else
    {
        enemy = std::make_shared<Player>(planets[startPlanet1]);
        me = std::make_shared<Player>(planets[startPlanet2]);  
    }
    
    
}

std::shared_ptr<Player> Gameworld::getP1()
{
    return me;
}

std::shared_ptr<Player> Gameworld::getP2()
{
    return enemy;
}

Gameworld::~Gameworld()
{
//    delete fight;
}

void Gameworld::turnSkip()
{
    nextTurn();
}

void Gameworld::turnChangeMode( Planet::Ptr p )
{
    if( p->getBuilding()->getType() != SHIPYARD )
    {
        return;
    }
    std::static_pointer_cast<Shipyard>( p->getBuilding() )->changeMode();
    nextTurn();
}

void Gameworld::turnBuildShipyard( Planet::Ptr p )
{
    p->build( SHIPYARD );

    nextTurn();

}

void Gameworld::turnBuildMine( Planet::Ptr p )
{
    p->build( MINE );

    nextTurn();

}

void Gameworld::turnTransportShip(Planet::Ptr from, Planet::Ptr to)
{

    if(to->getOwner() != nullptr)
    {
        if(from->getOwner() == to->getOwner())
        {
            from->transportShip(to);
        }
        else
        {
            if(to->getFighterNumber() > 0)
            {
                std::cout << "EngageFight" << std::endl;
                if(t_state)
                {
                    // wait some small time amount so we dont loose any other send packages
                    QThread::msleep(300);
                    emit signalStartFight();
                }
                fight = new Fight(from, to);
                return;
            }
            else
            {
                to->capture(from->getOwner());

                from->transportShip(to);
                from->getOwner()->planets().push_back(to);
            }

        }

    }
    else
    {
        to->capture(from->getOwner());

        from->transportShip(to);
        from->getOwner()->planets().push_back(to);
    }
    nextTurn();
}

void Gameworld::turnTransportOre(Planet::Ptr from, Planet::Ptr to, int amount)
{
    if(to->getOwner() != nullptr)
    {
        if(from->getOwner() == to->getOwner())
        {
            from->transportOre(to, amount);
        }
        else
        {
            from->transportOre(to, amount);
        }

    }
    else
    {
        //from->getOwner()->takePlanet(to);
        to->capture(from->getOwner());
        from->getOwner()->planets().push_back(to);
        from->transportOre(to, amount);
    }

    nextTurn();
}

void Gameworld::nextTurn()
{

    switch(winCheck())
    {
        case ME:
            qDebug() << "emit win";
            emit signalWinGame();
            return;
        case ENEMY:
            qDebug() << "emit lose";
            emit signalLoseGame();
            return;
        case NONE:
            break;
        default:
            break;
    }
    if(nextRound) {

        nextRound = false;
        emit signalRoundCountUp();

        std::vector < std::shared_ptr < Planet >> ::iterator
        i;

        for (i = planets.begin(); i != planets.end(); i++) {
            if ((*i)->getBuilding() != nullptr) {
                (*i)->produce();
            }

        }
        emit signalChangePlayerOwningsLabels(getP1()->totalTransporters(),
                                             getP1()->totalFightships(),
                                             getP1()->totalOre());
    }
    else
    {
        nextRound = true;
    }
    emit signalNextTurn(t_state);
    t_state = !t_state;
}


WINTYPE Gameworld::winCheck()
{
    if(me->getHomePlanet()->getOwner() != me)
    {
        return ENEMY;
    }
    if(enemy->getHomePlanet()->getOwner() != enemy)
    {
        return ME;
    }
    return NONE;
}

void Gameworld::gotNewPlanetUpdate(int id, asteroids::BUILDINGTYPE type)
{
    switch (type)
    {
        case MINE:
            turnBuildMine(planets[id]);
            break;
        case SHIPYARD:
            turnBuildShipyard(planets[id]);
            break;
        default:
            break;
    }
}

void Gameworld::gotNewOreTransport(int p1, int p2, int amount)
{
    turnTransportOre(planets[p1], planets[p2], amount);
}

void Gameworld::gotNewFighterTransport(int p1, int p2)
{
    turnTransportShip(planets[p1], planets[p2]);
}

void Gameworld::gotFightFinishInformation(bool win)
{
    if(fight == nullptr)
    {
        qDebug() << "No fight registered.";
        return;
    }
    Planet::Ptr from = fight->getP1();
    Planet::Ptr to = fight->getP2();
    if(win)
    {
        if (t_state)
        {
            to->wipe();
            //from->getOwner()->takePlanet(to);
            to->capture(from->getOwner());
            from->getOwner()->planets().push_back(to);
            from->transportShip(to);
        }
        else {
            from->destroyTransporter();
            from->destroyFighter();
        }
    }
    else
    {
        if (t_state)
        {
            from->destroyTransporter();
            from->destroyFighter();
        }
        else {
            to->wipe();
            //from->getOwner()->takePlanet(to);
            to->capture(from->getOwner());
            from->getOwner()->planets().push_back(to);
            from->transportShip(to);
        }
    }

    delete fight;
    fight = nullptr;

    nextTurn();
}

void Gameworld::gotProductionChangeMode(int p)
{
    turnChangeMode(planets[p]);
}

std::string Gameworld::getPlanetNameById(int id)
{
    return planets[id]->getName();
}

}
