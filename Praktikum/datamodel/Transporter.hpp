#ifndef Transporter_hpp
#define Transporter_hpp
/*
 *  Transporter.hpp
 *
 *  @date 19.03.2018
 *  @author Aeilko Luebsen
 */

#include "Ship.hpp"
#include <memory>

namespace asteroids {

    /*
     * @brief A class that represents a transport ship
     */
class Transporter : public Ship {
public:

    /*:

     * @brief Constructs a transporter
     */
    Transporter( std::shared_ptr<Planet> p) : Ship(TRANSPORTER, p), capacity(50) {}

    /*
     * @brief Returns the capacity of this ship
     */
    int getCapacity() { return capacity; }

    /**
     * @brief Destroy the Transporter object
     *
     */
    ~Transporter() = default;

private:

    int capacity;

};

}

#endif