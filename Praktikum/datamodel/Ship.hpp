#ifndef Ship_hpp
#define Ship_hpp
/*
 *  Ship.hpp
 *
 *  @date 18.03.2018
 *  @author Aeilko Luebsen
 */

#include <memory>
#include "Enums.hpp"


namespace asteroids {

    /*
     * @brief Class to represent a ship.
     */

class Planet;

class Ship {

public:

    using Ptr = std::shared_ptr<Ship>;

    /*
     * @brief Constructs a Ship and stores the type it's given. Automatically
     *        generates an ID for the ship.
     */
    Ship(SHIPTYPE type, std::shared_ptr<Planet> h_planet) : shiptype(type), id(lastID++), s_planet(h_planet) {}

    /*
     * @brief Returns the type of the ship
     */
    SHIPTYPE getType() { return shiptype; }

    std::shared_ptr<Planet> getHomePlanet() { return s_planet; }

    void setHomePlanet( std::shared_ptr<Planet> p ) { s_planet = p; }

    /**
     * @brief Destroy the Ship object
     *
     */
    ~Ship() = default;

protected:

    /// The type of the ship
    SHIPTYPE shiptype;

    /// ID
    const int id;

    /// LastID
    static int lastID;

    /// Home Planet
    std::shared_ptr<Planet> s_planet;

};

}

#endif
