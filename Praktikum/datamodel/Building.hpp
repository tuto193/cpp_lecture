#ifndef Building_hpp
#define Building_hpp

#include <memory>
#include "Enums.hpp"
#include <iostream>

namespace asteroids
{

/**
 * @brief Abstract Class to represent a building on a planet.
 *
 */

class Planet/* {
public:
    using Ptr = std::shared_ptr<Planet>;
} */;

class Building
{
public:

    using Ptr = std::shared_ptr<Building>;

    /**
     * @brief Construct a new Building object
     *
     * @param p the planet to which the building belongs to
     * @param type the type of building that is to be created
     */
    explicit Building( BUILDINGTYPE t ) : t_type(t) {  }

    /**
     * @brief   general function that should add (mined) ore
     *          to the planet that it be
    Ship::Ptr t;longs to
     * @return int  the amount of product being made
     */
    virtual int produce(Planet* onPlanet) const = 0 ;

    /**
     * @brief Get the Type of building this is
     *
     * @return TYPE the type of building being used
     */
    BUILDINGTYPE getType() const { return t_type; }

    /**
     * @brief Destroy the Building object
     *
     */
    ~Building() = default;

protected:
    /// the type of building this is
    BUILDINGTYPE t_type;
};

}

#endif
