/*
 *  Planet.cpp
 *
 *  @date 19.03.2018
 *  @author Aeilko Luebsen
 */

#include "Planet.hpp"
#include "Player.hpp"
#include "Ship.hpp"
#include "Transporter.hpp"
#include "Mine.hpp"
#include "Shipyard.hpp"
#include "Enums.hpp"
#include <stdlib.h>
#include <iterator>
#include <iostream>

namespace asteroids
{

int Planet::lastID = 0;

Planet::Planet(std::string name, int x, int y, int startOre) : m_homePlanet(false), oreToMine(startOre)
{

    position[0] = x;
    position[1] = y;

    owner = nullptr;
    building = nullptr;
    oreMined = 0;
    transporterNumber = 0;
    fighterNumber = 0;
    this->name = name;
    id = lastID++;

    ships = std::list<std::shared_ptr<Ship>>();

}

void Planet::tearDown()
{
    if(!building)
    {
        throw "There is no building on this planet to tear down";
    }
    building = nullptr;
}

void Planet::produce()
{
    if(!building)
    {
        return;
    }
    int produced = 0;
    produced = building->produce(this);
    switch ( building->getType() )
    {
        case MINE:
            oreMined += produced;
            oreToMine -= produced;
            break;
        case SHIPYARD:
            oreMined -= produced;

            break;
        default:
            break;
    }

}

void Planet::build(BUILDINGTYPE type)
{

    switch( type )
    {
        case MINE:
        {
            try
            {
                if(oreMined > 30)
                {
                    building = std::make_shared<Mine>( );
                    oreMined -= 30;
                }
            }
            catch( ... )
            {
                std::cerr << "Error: Mine could not be built by planet" << std::endl;
            }
            break;
        }
        case SHIPYARD:
        {
            try
            {
                if(oreMined > 50)
                {
                    building = std::make_shared<Shipyard>( );
                    oreMined -= 50;
                }
            }
            catch( ... )
            {
                std::cerr << "Error: Shipyard could not be built by planet" << std::endl;
            }
            break;
        }
        default:
        {
            std::cerr << "Error: Type of building not recognised" << std::endl;
            break;
        }
    }

}

void Planet::transportShip(Planet::Ptr toPlanet)
{
    //TODO UPDATE GAMEPLANET IN VIEW
    if(toPlanet->getOwner() == nullptr)
    {
        toPlanet->capture(this->owner);
    }

    if(transporterNumber > 0 && fighterNumber > 0)
    {
        std::list<std::shared_ptr<Ship>>::iterator it;
        for(it = ships.begin(); (*it)->getType() != FIGHTER; it++);
        Ship::Ptr send = (*it);
        toPlanet->receiveShip(send);
        ships.remove(*it);

        for(it = ships.begin(); (*it)->getType() != TRANSPORTER; it++);
        send = (*it);
        toPlanet->receiveShip(send);
        ships.remove(*it);
        transporterNumber--;
        fighterNumber--;
    }
    else
    {
        throw "No fighter on the planet to send";
    }

}

void Planet::transportOre(Planet::Ptr toPlanet, int amount)
{
    if(amount > oreMined)
    {
        amount = oreMined;
    }
    if(transporterNumber > 0)
    {
        int maxCap = 0;
        std::list<std::shared_ptr<Ship>>::iterator it;
        std::shared_ptr<Transporter> biggest;
        for(it = ships.begin(); it != ships.end(); it++)
        {
            if( (*it)->getType() == TRANSPORTER )
            {
                if( std::static_pointer_cast<Transporter>(*it)->getCapacity() > maxCap)
                {
                    biggest = std::static_pointer_cast<Transporter>(*it);
                    maxCap = biggest->getCapacity();
                }
            }
        }
        toPlanet->receiveShip(std::dynamic_pointer_cast<Ship>(biggest));
        ships.remove(std::dynamic_pointer_cast<Ship>(biggest));
        transporterNumber--;
        toPlanet->receiveOre(amount);
        oreMined -= amount;

    }
    else
    {
        throw "No transporter on the planet to send.";
    }
}

void Planet::receiveShip(Ship::Ptr receiveShip)
{
    SHIPTYPE type = receiveShip->getType();
    if(type == FIGHTER)
    {
        fighterNumber++;
    }
    else if(type == TRANSPORTER)
    {
        transporterNumber++;
    }
    else
    {
        std::cerr << "Error: ship arriving/created at planet " << this->name << " not of "
        "recognised type -> " << type << std::endl;
    }
    ships.push_back(receiveShip);
}

void Planet::receiveOre(int amount)
{
    oreMined += amount;
}

void Planet::capture(Player::Ptr capturer)
{
    if (owner)
    {
        owner.reset();
    }

    owner = std::shared_ptr<Player>(capturer);
}

void Planet::setHomePlanet(bool homePlanet)
{
    m_homePlanet = homePlanet;
    if(homePlanet)
    {
        this->receiveOre(80);
        this->receiveShip(std::make_shared<Transporter>(std::shared_ptr<Planet>(this)));
        this->receiveShip(std::make_shared<FightShip>(std::shared_ptr<Planet>(this)));
        this->build(MINE);
    }
}

void Planet::addAdjacent(Planet::Ptr other)
{
    other->adjacents.push_back(std::shared_ptr<Planet>(this));
    adjacents.push_back(std::shared_ptr<Planet>(other));
}

void Planet::destroyFighter()
{
    fighterNumber--;
}

void Planet::destroyTransporter()
{
    transporterNumber--;
}

void Planet::wipe()
{
    fighterNumber = 0;
    transporterNumber = 0;
    building.reset();
}

Planet::~Planet()
{
    if( building )
    {
        building.reset();
    }

    if( !ships.empty() )
    {
        std::list<Ship::Ptr>::iterator it;
        for( it = ships.begin(); it != ships.end(); it++ )
        {
            (*it).reset();
        }
    }

    /// TODO (?): also relinquish ownership of adjacent planet pointers

}

}
