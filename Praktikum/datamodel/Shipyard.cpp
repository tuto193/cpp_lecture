#include "Shipyard.hpp"
#include "Planet.hpp"
#include "Ship.hpp"
#include <iostream>

namespace asteroids
{

int Shipyard::produce(Planet* onPlanet) const
{
    if(onPlanet->getOreMined() < ship_cost)
    {
        return 0;
    }
    std::shared_ptr<Ship> t;
    switch( mode )
    {
        case TRANSPORTER:
            t = std::make_shared<Transporter>( std::shared_ptr<Planet>(onPlanet));
            onPlanet->receiveShip( t );
            break;
        case FIGHTER:
            t = std::make_shared<FightShip>(  std::shared_ptr<Planet>(onPlanet)  );
            onPlanet->receiveShip( t );
            break;
        default:
            std::cerr << "Error: Shipyard on planet " + onPlanet->getName() + " tried to produce without "
            "having a mode assigned to it." << std::endl;
    }

    return ship_cost;
}

void Shipyard::changeMode()
{
    if(mode == TRANSPORTER)
    {
        mode = FIGHTER;
    }
    else
    {
        mode = TRANSPORTER;
    }


}

}// namespace asteroids