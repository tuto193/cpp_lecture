#ifndef FightShip_hpp
#define FightShip_hpp

/*
 *  Fighter.hpp
 *
 *  @date 18.03.2018
 *  @author Aeilko Luebsen
 */

#include "Ship.hpp"
#include "Enums.hpp"
#include <memory>

namespace asteroids
{

/*
 * @brief A class that represents a fighter in the 2D-View of the game.
 */
class FightShip : public Ship
{

public:

    using Ptr = std::shared_ptr<FightShip>;

    /*
     * @brief Constructs a fightship
     */
    FightShip( std::shared_ptr<Planet> p ) : Ship(FIGHTER, p), maxHealth(50), currHealth(maxHealth), damage(10) {}

    /*
     * @brief Returns the damage this ship deals in a fight with one shot.
     */
    int getDamage();

    /*
     * @brief Returns the current health of the ship
     */
    int getCurrHealth();

    /*
     * @brief Sets the current health of this ship
     */
    void setCurrHealth(int newhealth);

    /*
     * @brief Returns the maximum health of this ship
     */
    int getMaxHealth();

    /**
     * @brief Destroy the Fight Ship object
     *
     */
    ~FightShip() = default;

private:

    /// Maximum health
    int maxHealth;

    /// Current health
    int currHealth;

    /// Damage this ship deals with a shot
    int damage;

};

}

#endif