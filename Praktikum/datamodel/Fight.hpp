//
// Created by Krämer on 26.03.2019.
//

#ifndef PRAKTIKUM2_FIGHT_HPP
#define PRAKTIKUM2_FIGHT_HPP

#include "Planet.hpp"
namespace asteroids {

class Fight {
public:

    Fight(Planet::Ptr p1, Planet::Ptr p2);

    Planet::Ptr getP1() { return m_p1; }

    Planet::Ptr getP2() { return m_p2; }

private:
    Planet::Ptr m_p1;
    Planet::Ptr m_p2;

};

} //namespace asteroids


#endif //PRAKTIKUM2_FIGHT_HPP
