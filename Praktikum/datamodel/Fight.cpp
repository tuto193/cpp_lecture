#include <utility>

//
// Created by Krämer on 26.03.2019.
//

#include "Fight.hpp"

namespace asteroids {

    Fight::Fight(Planet::Ptr p1, Planet::Ptr p2) {
        m_p1 = std::move(p1);
        m_p2 = std::move(p2);
    }

}