#ifndef Player_hpp
#define Player_hpp

#include <list>
#include <memory>
#include "Enums.hpp"

/**
 * @brief class for the reprensentation of a player within the game
 *
 */
namespace asteroids
{

class Planet;

class Player
{
public:

    using Ptr = std::shared_ptr<Player>;
    /**
     * @brief Construct a new Player object
     *
     */
    Player(std::shared_ptr<Planet> home);

    /**
     * @brief get a list of planets, which the player is at the moment in control of.
     *
     * @return Planet*  the list of planet objects that the player in control of
     */
    std::list<std::shared_ptr<Planet>>& planets();

    /**
     * @brief   the total number of planets under control of the player
     *
     * @return int  the sum of all planets that are currently being controlled by this player
     */
    int totalPlanets() const;

    /**
     * @brief get the total ore that the player has currently available for themselves
     *
     * @return int  the sum of all the ore available for the player amongst their planets
     */
    int totalOre() const;

    /**
     * @brief   get the total amount of transporters that the player has currently available to themselves
     *
     * @return int  the sum of all available transporters that the player has (not counting the ones currently being built)
     */
    int totalTransporters() const;

    /**
     * @brief   get the total amount of fighter ships that the player has currently available to themselves
     *
     * @return int  the sum of all available fighter ships that the player has (not counting th ones currently being built)
     */
    int totalFightships() const;

    /**
     * @brief   try to take a planet (it not already taken) and add it to the list of planets taken
     *        if( !produced )

     * @param p the planet to be taken
     * @return true when the planet was taken and saved successfully
     * @return false    when the plPlanet::PtrPlanet::Ptranet was already taken
     */
    bool takePlanet( std::shared_ptr<Planet> p );
    
    /**
     * @brief Get the Home Planet from the this player
     *
     * @return std::shared_ptr<Planet>  this player's home planet
     */
    std::shared_ptr<Planet> getHomePlanet() { return homePlanet; }

    /**
     * @brief Destroy the Player object
     *
     */
    ~Player() = default;

private:
    /// the list of all planets in posession by the player
    std::list<std::shared_ptr<Planet>> taken_planets;

    /// The starting planet of this player

    std::shared_ptr<Planet> homePlanet;

};

} // namespace asteroids

#endif