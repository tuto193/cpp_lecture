#ifndef Gameworld_hpp
#define Gameworld_hpp

#include "Player.hpp"
#include "../math/Vector.hpp"
#include "Enums.hpp"
#include "../view/Mapwidget.hpp"
#include <string>
#include <vector>
#include <memory>
#include <QObject>
#include "networking/NetworkInterface.hpp"
#include "datamodel/Fight.hpp"

namespace asteroids
{

class Planet;

class Gameworld : public QObject
{
    Q_OBJECT
public:

    typedef std::pair<int, int> edge;
    using Ptr = std::shared_ptr<Gameworld>;

    /**
     * @brief Construct a new Gameworld object
     *
     */
    explicit Gameworld(bool pl1 = false);

    /**
     * @brief Returns all the planets in the universe
     */
    std::vector< std::shared_ptr<Planet> >& getPlanets() { return planets; }

    /**
     * @brief Get a particular planet from the game using it's ID (INDEX)
     *
     * @param id    the id/index of the planet to be returned
     * @return Planet&  a reference to the ID-ed planet
     */
    std::shared_ptr<Planet> getPlanet( int id ) { return planets[id]; }

    /**
     * @brief   get the player1
     *
     * @return std::shared_ptr<Player>  the player1
     */
    std::shared_ptr<Player> getP1();

    /**
     * @brief get the player2
     *
     * @return std::shared_ptr<Player>      player2
     */
    std::shared_ptr<Player> getP2();

    /**
     * @brief Get the edges from the read data
     *
     * @return std::vector<edge>    the vector of edges
     */
    std::vector<edge>& getEdges() { return edges; }

    /**
     * @brief let the active player change the type of ship that is being built in his shipyard
     *
     * @param p the planet the shipyard is on.
     */
    void turnChangeMode( std::shared_ptr<Planet> p );


    /**
     * @brief let the player build a Shipyard on the given planet
     *
     * @param p     the planet where the shipyard shall be built on
     */
    void turnBuildShipyard( std::shared_ptr<Planet> p );

    /**
     * @brief let the active player build a mine on a given planet
     *
     * @param p     the planet the player is building a mine on
     */
    void turnBuildMine( std::shared_ptr<Planet> p );

    /**
     * @brief let the active player transport a ship from one planet to another
     *
     * @param from  the origin planet
     * @param to    the destination planet
     */
    void turnTransportShip(std::shared_ptr<Planet> from, std::shared_ptr<Planet> to);

    /**
     * @brief   let the active player transport a certain amount of Ore from one planet to another
     *
     * @param from  the origin planet
     * @param to    the destiantion planet
     * @param amount    the amount of ore to be transported
     */
    void turnTransportOre(std::shared_ptr<Planet> from, std::shared_ptr<Planet> to, int amount);

    /*
     * @brief Returns if and what player has won
     */

    WINTYPE getWon() { return winner; }

    bool getIsPlayerOne() { return isPlayerOne; }

    bool getState() { return t_state; }

    std::shared_ptr<Player> getMe() { return me; }

    /**
     * @brief Destroy the Gameworld object
     *
     */
    ~Gameworld() override;

signals:
    void signalStartFight();

    /**
     * @brief signalChangePlayerOwningsLabels   Sends a signal to MainWindow if the number has changed to
     *                                          update the Labels
     * @param trans     number of Transporters owned by the Player
     * @param ship      number of Fighters owned by the Player
     * @param ore       number of Ore owned by the Player
     */
    void signalChangePlayerOwningsLabels(int trans, int ship, int ore);

    void signalRoundCountUp();

    /**
     * @brief Simple signal which is emitted if one ended its turn.
     */
    void signalNextTurn(bool state);

    void signalWinGame();

    void signalLoseGame();

public slots:
    void gotNewPlanetUpdate(int id, BUILDINGTYPE type);

    /**
     * @brief Let the active player skip his turn,
     *        called if turnSkipped is emitted
     *
     */
     void turnSkip();

     /**
      * @brief Accesses the planets via id and transports Ore
      * @param p1 From
      * @param p2 To
      * @param amount Ore
      */
     void gotNewOreTransport(int p1, int p2, int amount);

     /**
      * @brief Accesses the planets via id and transports Fighter
      * @param p1 From
      * @param p2 To
     */
     void gotNewFighterTransport(int p1, int p2);

     /**
      * @brief Get Winning information of 3D fight
      * @param win
      */
     void gotFightFinishInformation(bool win);

    /**
     * @brief Changes production from mine to shipyard and
     *        vice versa.
     * @param p Planet
     */
     void gotProductionChangeMode(int p);

     std::string getPlanetNameById(int id);


private:

    /**
     * @brief parse the map-file to be used
     *
     * @param mapfile   the file with the coordinates of the planets and the corresponding adjacencies
     */
    void readMapFile(std::string mapfile);

    /**
     * @brief Calculates the next turn
     */
    void nextTurn();

    /**
     * @brief Checking win
     * @return wintype: win or lose
     */
    WINTYPE winCheck();

    /// Edge List for the 2D-GUI
    std::vector<edge> edges;

    /// player playing in this client
    std::shared_ptr<Player> me;

    /// player playing in the other client
    std::shared_ptr<Player> enemy;

    /// saves wether this player is player one
    const bool isPlayerOne;

    /// The vector with all planets in the game
    std::vector< std::shared_ptr<Planet> > planets;

    /// The status of current Player. TRUE = your turn
    bool t_state;

    /// To check if nextTurn starts a new Round
    bool nextRound;

    /// Winner
    WINTYPE winner;

    /// Fight Information
    Fight* fight;
};

}

#endif
