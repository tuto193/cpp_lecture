#ifndef Shipyard_hpp
#define Shipyard_hpp

#include "Building.hpp"
#include "Ship.hpp"
#include "FightShip.hpp"
#include "Transporter.hpp"
#include <memory>
#include <iostream>

namespace asteroids
{

/**
 * @brief A small class representing a shipyard
 *
 */
class Shipyard : public Building
{
public:

    using Ptr = std::shared_ptr<Shipyard>;

    /**
     * @brief Construct a new Shipyard object
     *
     * @param p the planet on which the shipyard is to be built
     * @param t the type of building this is (a SHIPYARD)
     */
    Shipyard( ) : Building( BUILDINGTYPE::SHIPYARD ), mode(SHIPTYPE::TRANSPORTER), ship_cost(50) { }

    /**
     * @brief produce a ship per turn and add it to the planet's ship-list
     *
     * @param rate  at which ships shall be produced
     * @return int  the amount of ships that were produced
     */
    int produce(Planet* onPlanet) const;

    /**
     * @brief
     *
     */
    void changeMode();

    SHIPTYPE getMode() { return mode; }

    /**
     * @brief Destroy the Shipyard object
     *
     */
    ~Shipyard() {  }

  private:

    SHIPTYPE mode;

    int ship_cost;
    
};

}

#endif