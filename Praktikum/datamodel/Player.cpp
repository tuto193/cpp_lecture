#include "Player.hpp"
#include "Planet.hpp"

namespace asteroids
{

Player::Player(std::shared_ptr<Planet> home)
{
    homePlanet = home;
    
    takePlanet(home);
}

std::list<Planet::Ptr>& Player::planets()
{
    return taken_planets;
}

int Player::totalPlanets() const
{
    return static_cast<int>(taken_planets.size());
}

int Player::totalOre() const
{
    int sum = 0;

    for (auto& planet : taken_planets)
    {
        sum += planet->getOreMined();
    }

    return sum;
}

int Player::totalTransporters() const
{
    int sum = 0;
    for (auto& planet : taken_planets)
    {
        sum += planet->getTransporterNumber();
    }

    return sum;
}

int Player::totalFightships() const
{
    int sum = 0;
    for (auto& planet : taken_planets)
    {
        sum += planet->getFighterNumber();
    }
    return sum;
}

bool Player::takePlanet( Planet::Ptr p )
{
    taken_planets.push_back(p);
    p->capture( std::shared_ptr<Player>(this) );
    return true;
}

}