/*
 *  Main.cpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "view/MainWindow.hpp"
#include "networking/Connection.hpp"
#include "networking/NetworkInterface.hpp"
#include <string>
#include <memory>
#include <iostream>
#include <QThread>

Q_DECLARE_METATYPE(Vector3f)
Q_DECLARE_METATYPE(Quaternion)
Q_DECLARE_METATYPE(SpaceCraft::Ptr)

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        cerr << "Usage: asteroids <level-file>" << endl;
        return 1;
    }

    // register custom meta types so it is possible to signal them between
    // multiple threads. Also see Q_DECLARE_METATYPE macro above!
    qRegisterMetaType<Vector3f>();
    qRegisterMetaType<Quaternion>();
    qRegisterMetaType<SpaceCraft::Ptr>();

    // start app
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon("../img/asteroid.png"));

    asteroids::MainWindow mainWindow(argv[1]);
    mainWindow.show();

    int exec = QApplication::exec();

    return exec;
}
