#ifndef _NetworkInterface_HPP_
#define _NetworkInterface_HPP_

#include <QObject>
#include "rendering/SpaceCraft.hpp"
#include "math/Vector.hpp"
#include "datamodel/Enums.hpp"
#include "datamodel/Planet.hpp"

namespace asteroids
{
/**
 * @brief This singleton object is used to communicate with the other player
 * within the application.
 */
class NetworkInterface : public QObject
{
Q_OBJECT

public:

    /**
     * @brief Returns the Singleton instance.
     * @return instance Singleton instance
     */
    static NetworkInterface& instance()
    {
        static NetworkInterface instance;

        return instance;
    }

    /**
     * @brief Send spacecraft's position if fight is engaged
     * @param pos spacecraft's position
     */
    void sendStartFight(const Vector3f& pos);

    /**
     * @brief Sends the command to end a fight
     */
    void sendEndFight();

    /**
     * @brief Send if the fight is won
     */
    void sendWonFight();

    /**
     * @brief Send won-lost-acknowledgement
     */
    void sendWonLostAcknowledgement();

signals:

    /**
     * @brief Signals an incoming new enemy fighter position
     */
    void newFighterPos(const Vector3f& pos,
                       const Vector3f& xAxis,
                       const Vector3f& yAxis,
                       const Vector3f& zAxis,
                       const Quaternion& rot,
                       const Vector3f& dir,
                       bool isShooting);

    /**
     * @brief Signal is emitted if other player wanted to start a fight.
     */
    void signalStartFight(const Vector3f& pos);

    /**
     * @brief Signal is emitted if acknowledgement was successful, thus ending the fight.
     */
    void signalEndFight();

    /**
     * @brief Signal is emitted if other player send he lost.
     */
    void signalOtherLost();

    /**
     * @brief Signal is emitted if other player send he won.
     */
    void signalOtherWon();

    /**
     * @brief Signal is emitted if other send won/lost and this player is okay with it.
     */
    void signalWonLostAcknowledgement();

    /**
     * @brief Signals is emitted if new Planet information is incoming
     * @param p Planet ID
     * @param type Type of changes
     */
    void newPlanetUpdate(int p, BUILDINGTYPE type);

    /**
     * @brief Signals the skipping of a turn
     */
    void turnSkipped();

    /**
     * @brief Signal is emitted if Transport Ore information is incoming
     * @param p1 From
     * @param p2 To
     * @param amount Amount
     */
    void signalTransportOre(int p1, int p2, int amount);

    /**
     * @brief Signal is emitted if Transport Fighter information is incoming
     * @param p1 From
     * @param p2 To
     */
    void signalTransportFighter(int p1, int p2);

    /**
     * @brief Signal is emitted if Change Production information is incoming
     * @param p Planet ID
     */
    void signalChangeProduction(int p);

    /**
     * @brief signalReceiveMsg Called if a message is received
     * @param message Message that is received
     */
    void signalReceiveMsg(QString message);

public slots:

    /**
     * @brief Called if skip signal is emitted from MainWindow
     */
    void skipTurn();

    /**
     * @brief Parses the incoming data and delegates it to suitable helper methods
     */
    void readData(QByteArray& data);

    /**
     * @brief Method to send given fighter to other client
     */
    void sendFighterUpdate(const SpaceCraft::Ptr&, bool isShooting);

    /**
     * @brief Send an update Planet Protocol
     * @param p Planet
     * @param type Type of action
     */
    void updatePlanet(int p, BUILDINGTYPE type);


    /**
     * @brief Signal is emitted if Transport Ore information is incoming
     * @param p1 From
     * @param p2 To
     * @param amount Amount of ore
     */
    void slotTransportOre(int p1, int p2, int amount);

    /**
     * @brief Signal is emitted if Transport Fighter information is incoming
     * @param p1 From
     * @param p2 To
     */
    void slotTransportFighter(int p1, int p2);

    /**
     * @brief Signal is emitted if Change Production information is incoming
     * @param p Planet ID
     */
    void slotChangeProduction(int p);

    /**
     * @brief slotMsgSend Signal is emitted if a message is send
     * @param message
     */
    void slotMsgSend(QString message);


private:
    /**
     * @brief Helper method to parse fighter position. Also emits newFighterPos
     */
    void parseFighter(QDataStream& in);
    /**
     * @brief Helper method to parse planet. Also emits newPlanetUpdate
     */
    void parsePlanet(QDataStream& in);
    /**
     * @brief Helper method to parse transported ore. Also emits signalTransportOre
     */
    void parseTransportOre(QDataStream& in);
    /**
     * @brief Helper method to parse transported fighter. Also emits signalTransportFighter
     */
    void parseTransportFighter(QDataStream &in);
    /**
     * @brief Helper method to parse a change in shipyard production.
     *        Also emits signalChangeProduction
     */
    void parseChangeProduction(QDataStream &in);

    void parseMsg(QDataStream &in);

};
}


#endif //_NetworkInterface_HPP_
