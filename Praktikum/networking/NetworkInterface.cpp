#include "NetworkInterface.hpp"
#include "NetworkDataConstants.hpp"
#include "Connection.hpp"

#include <QThread>

namespace asteroids
{
void NetworkInterface::sendStartFight(const Vector3f& pos)
{
    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);

    out << NDC_START_FIGHT;
    out << pos[0];
    out << pos[1];
    out << pos[2];

    Connection::instance().sendByteArray(buf);
}

void NetworkInterface::sendEndFight()
{
    Connection::instance().sendCommand(NDC_END_FIGHT);
}

void NetworkInterface::sendWonFight()
{
    Connection::instance().sendCommand(NDC_3D_WON);
}

void NetworkInterface::sendWonLostAcknowledgement()
{
    Connection::instance().sendCommand(NDC_3D_WON_LOST_ACT);
}


void NetworkInterface::readData(QByteArray& data)
{
    QDataStream in(&data, QIODevice::ReadOnly);

    QString type;
    in >> type;

    if (type.startsWith(NDC_3D_FIGHTER))
    {
        parseFighter(in);
    }
    else if (type.startsWith(NDC_3D_LOST))
    {
        emit signalOtherLost();
    }
    else if (type.startsWith(NDC_3D_WON))
    {
        emit signalOtherWon();
    }
    else if (type.startsWith(NDC_3D_WON_LOST_ACT))
    {
        // Signal both players agreed that they won/lost
        // Player who initiated the fight should than send NDC_END_FIGHT
        emit signalWonLostAcknowledgement();
    }
    else if (type.startsWith(NDC_START_FIGHT))
    {
        float tmpX, tmpY, tmpZ;
        Vector3f tmpPos;

        in >> tmpX >> tmpY >> tmpZ;

        tmpPos[0] = tmpX;
        tmpPos[1] = tmpY;
        tmpPos[2] = tmpZ;

        emit signalStartFight(tmpPos);
    }
    else if (type.startsWith(NDC_END_FIGHT))
    {
        // Signal fight should end -> go back to 2D GUI
        // This should only be send by the player who initiated the fight
        emit signalEndFight();
    }
    else if (type.startsWith(NDC_MSG))
    {
        QString msg;
        in >> msg;
        qInfo() << msg;
    }
    else if (type.startsWith(NDC_2D_PLANET))
    {
        parsePlanet(in);
    }
    else if (type.startsWith(NDC_2D_SKIP))
    {
        emit turnSkipped();
    }
    else if (type.startsWith(NDC_2D_TRANS_ORE))
    {
        parseTransportOre(in);
    }
    else if (type.startsWith(NDC_2D_TRANS_FGH))
    {
        parseTransportFighter(in);
    }
    else if (type.startsWith(NDC_2D_CHG_PRD))
    {
        parseChangeProduction(in);
    }
    else if (type.startsWith(NDC_2D_MSG))
    {
        parseMsg(in);
    }
}

void NetworkInterface::sendFighterUpdate(const SpaceCraft::Ptr& fighter, bool isShooting)
{
    if (!Connection::instance().isConnected())
    {
        return;
    }

    if (fighter->getHp() == 0)
    {
        Connection::instance().sendCommand(NDC_3D_LOST);

        return;
    }

    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);

    out << NDC_3D_FIGHTER;
    // position
    out << fighter->getPosition()[0]
        << fighter->getPosition()[1]
        << fighter->getPosition()[2]
        // x-axis
        << fighter->getXAxis()[0]
        << fighter->getXAxis()[1]
        << fighter->getXAxis()[2]
        // y-axis
        << fighter->getYAxis()[0]
        << fighter->getYAxis()[1]
        << fighter->getYAxis()[2]
        // z-axis
        << fighter->getZAxis()[0]
        << fighter->getZAxis()[1]
        << fighter->getZAxis()[2]
        // rotation
        << fighter->getRotation()[0]
        << fighter->getRotation()[1]
        << fighter->getRotation()[2]
        << fighter->getRotation()[3]
        // direction
        << fighter->getDirection()[0]
        << fighter->getDirection()[1]
        << fighter->getDirection()[2]
        // shooting
        << isShooting;

    Connection::instance().sendByteArray(buf);
}

void NetworkInterface::updatePlanet(int p, BUILDINGTYPE type)
{
    if (!Connection::instance().isConnected())
    {
        return;
    }
    auto i_type = (qint32)type;

    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);

    out << NDC_2D_PLANET;
    // position
    out << p
        << i_type;

    Connection::instance().sendByteArray(buf);
}

void NetworkInterface::slotTransportOre(int p1, int p2, int amount)
{
    if (!Connection::instance().isConnected())
    {
        return;
    }

    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);

    out << NDC_2D_TRANS_ORE << p1 << p2 << amount;
    Connection::instance().sendByteArray(buf);

}

void NetworkInterface::slotTransportFighter(int p1, int p2) {

    if (!Connection::instance().isConnected())
    {
        return;
    }

    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);

    out << NDC_2D_TRANS_FGH << p1 << p2;
    Connection::instance().sendByteArray(buf);

}

void NetworkInterface::slotChangeProduction(int p) {

    if (!Connection::instance().isConnected())
    {
        return;
    }

    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);

    out << NDC_2D_CHG_PRD << p;
    Connection::instance().sendByteArray(buf);
}

void NetworkInterface::parseFighter(QDataStream& in)
{

    float tmpX, tmpY, tmpZ, tmpW;
    bool isShooting;
    Vector3f tmpPos, tmpXAxis, tmpYAxis, tmpZAxis, tmpDir;

    in >> tmpX >> tmpY >> tmpZ;
    tmpPos[0] = tmpX;
    tmpPos[1] = tmpY;
    tmpPos[2] = tmpZ;

    in >> tmpX >> tmpY >> tmpZ;
    tmpXAxis[0] = tmpX;
    tmpXAxis[1] = tmpY;
    tmpXAxis[2] = tmpZ;

    in >> tmpX >> tmpY >> tmpZ;
    tmpYAxis[0] = tmpX;
    tmpYAxis[1] = tmpY;
    tmpYAxis[2] = tmpZ;

    in >> tmpX >> tmpY >> tmpZ;
    tmpZAxis[0] = tmpX;
    tmpZAxis[1] = tmpY;
    tmpZAxis[2] = tmpZ;

    in >> tmpX >> tmpY >> tmpZ >> tmpW;
    Quaternion tmpRot(tmpX, tmpY, tmpZ, tmpW);

    in >> tmpX >> tmpY >> tmpZ;
    tmpDir[0] = tmpX;
    tmpDir[1] = tmpY;
    tmpDir[2] = tmpZ;

    in >> isShooting;

    emit newFighterPos(tmpPos, tmpXAxis, tmpYAxis, tmpZAxis, tmpRot, tmpDir, isShooting);
}

void NetworkInterface::parsePlanet(QDataStream &in)
{
    qint32 i_type;
    BUILDINGTYPE type;
    int planetId;

    in >> planetId >> i_type;


    type = (BUILDINGTYPE)i_type;
    emit newPlanetUpdate(planetId, type);
}

void NetworkInterface::skipTurn()
{
    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);
    out << NDC_2D_SKIP;
    Connection::instance().sendByteArray(buf);

}

void NetworkInterface::slotMsgSend(QString message)
{
    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);
    out << NDC_2D_MSG << message;
    Connection::instance().sendByteArray(buf);
}

void NetworkInterface::parseTransportOre(QDataStream &in)
{
    int planet1Id;
    int planet2Id;
    int amount;

    in >> planet1Id >> planet2Id >> amount;
    emit signalTransportOre(planet1Id, planet2Id, amount);

}

void NetworkInterface::parseTransportFighter(QDataStream &in)
{
    int planet1Id;
    int planet2Id;
    in >> planet1Id >> planet2Id;
    emit signalTransportFighter(planet1Id, planet2Id);

}

void NetworkInterface::parseChangeProduction(QDataStream &in)
{
    int planet;
    in >> planet;
    emit signalChangeProduction(planet);
}

void NetworkInterface::parseMsg(QDataStream &in)
{
    QString msg;
    in >>msg;
    emit signalReceiveMsg(msg);
}
}
