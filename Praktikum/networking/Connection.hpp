#ifndef _Connection_HPP_
#define _Connection_HPP_

#include <QObject>

#include <QTcpSocket>
#include <QTcpServer>
#include <QDebug>
#include <QDataStream>
#include <math/Vector.hpp>
#include <rendering/SpaceCraft.hpp>
#include <QString>

#include "networking/NetworkDataConstants.hpp"

namespace asteroids
{

class Connection : public QObject
{
Q_OBJECT

public:
    /**
     * @brief Singleton: returns instance.
     * @return instance Singletion instance
     */
    static Connection& instance();

    /**
     * @brief Check if connected to other.
     * @return connected True if connected
     */
    bool isConnected();

    /**
     * @brief Simply send given byte array.
     */
    void sendByteArray(QByteArray&);

    /**
     * @brief Send a simple command over the network. This command must be defined
     *        in the NetworkDataConstants.hpp, thus starting with NDC_.
     *        This method also accepts an optional message which can be
     *        appended to the command.
     */
    void sendCommand(const QString& cmd, const QString& msg = nullptr);

    /**
     * @brief Set IP to given QString
     * @param ip IP address
     */
    void setIp(QString ip);

    /**
     * @brief Disconnect Socket and Server
     */
    void disconnectSocket();

    /**
     * @brief Close the Server if open.
     */
    void closeServer();

    ~Connection() override;

public slots:

    /// Triggered by newConnection signal
    void gotServerConnection();

    /**
     * @brief Host a Server, so another player can connect.
     */
    void host();

    /**
     * @brief Connect to a Host.
     *
     * @return true if connection to server was successful.
     */
    bool connectTo();

    /**
     * @brief Read the incoming data and emit signalNewData with read data.
     */
    void readData();

private:
    /**
     * @brief Singleton: Private Constructor.
     */
    Connection();

    /// Saves the socket to send Data.
    QTcpSocket *m_sock;

    /// Saves server to destroy him later
    QTcpServer *m_server;

    /// IP Address to connect to Server
    QString m_ip;

signals:

    /// Signal new data
    void signalNewData(QByteArray&);

    /// Signal starting game
    void signalStartingGame(bool isPlayerOne);

private:

    /// The transfer datastream
    QDataStream in;
};

} // asteroids

#endif //_Connection_HPP_
