#include "Connection.hpp"
#include <QThread>


namespace asteroids
{

Connection::Connection()
= default;

Connection& Connection::instance()
{
    static Connection instance;
    return instance;
}

bool Connection::isConnected()
{
    return m_sock != nullptr && m_sock->state() == QAbstractSocket::ConnectedState;
}

void Connection::host()
{
    if (!isConnected())
    {
        // Open a new Server
        if(m_server == nullptr)
        {
            m_server = new QTcpServer;

            connect(m_server, SIGNAL(newConnection()), this, SLOT(gotServerConnection()));

            // Listen for other Clients to connect
            if (!m_server->listen(QHostAddress::Any, 42069)) {
                qDebug() << "Server could not start!";
            } else {
                qDebug() << "Starting to Host on Port 42069";
            }
        }
        else
        {
            qDebug() << "Server is already running!";
        }
    } else
    {
        qDebug() << "Already connected.";
    }
}

bool Connection::connectTo()
{
    if (!isConnected())
    {
        m_sock = new QTcpSocket;

        m_sock->connectToHost(m_ip, 42069);

        if (m_sock->waitForConnected(3000))
        {
            qDebug() << "Connected!";
            m_sock->setSocketOption(QAbstractSocket::LowDelayOption, 1);
            in.setDevice(m_sock);

            connect(m_sock, SIGNAL(readyRead()), this, SLOT(readData()));

            // sending to player 1. NOT US!
            sendCommand(NDC_MSG, "You are Player 1");

            emit signalStartingGame(false);
        } else
        {
            qDebug() << "Not Connected!";

            return false;
        }
    } else
    {
        qDebug() << "Already connected!";
    }

    return true;
}

void Connection::sendByteArray(QByteArray& arr)
{
    if (!isConnected())
    {
        return;
    }

    m_sock->write(arr);
    m_sock->waitForBytesWritten(30);
}


void Connection::setIp(QString ip)
{
    m_ip = ip;
}

void Connection::gotServerConnection()
{
    // Set the first incoming connection
    if (!isConnected())
    {
        m_sock = m_server->nextPendingConnection();
        m_sock->setSocketOption(QAbstractSocket::LowDelayOption, 1);

        in.setDevice(m_sock);

        //m_server->pauseAccepting();
        connect(m_sock, SIGNAL(readyRead()), this, SLOT(readData()));

        sendCommand(NDC_MSG, "You are Player 2");
        emit signalStartingGame(true);

        qDebug() << "player 2 connected";
    }
    else
    {
        QTcpSocket *s = m_server->nextPendingConnection();

        s->write("Game is already full\r\n");

        s->close();
    }
}

void Connection::disconnectSocket()
{
    if(isConnected())
    {
        qDebug() << "Disconnect From Host";
        m_sock->disconnectFromHost();
        qDebug() << "delete socket";
        delete m_sock;
        qDebug() << "set sock to nullptr";
        m_sock = nullptr;
        closeServer();
    }
}

void Connection::closeServer()
{
    if(m_server != nullptr && !isConnected())
    {
        qDebug() << "Closing Server";
        m_server->close();
        delete m_server;
        m_server = nullptr;
    }
}

void Connection::readData()
{
    QByteArray buffer;
    while (m_sock->bytesAvailable() > 0)
    {
        buffer.append(m_sock->readAll());
    }

    emit signalNewData(buffer);
}

void Connection::sendCommand(const QString& cmd, const QString& msg)
{
    if (!cmd.startsWith("NDC_") || !isConnected())
    {
        return;
    }

    QByteArray buf;
    QDataStream out(&buf, QIODevice::WriteOnly);

    out << cmd;

    if (msg != nullptr)
    {
        out << msg;
    }

    m_sock->write(buf);
}

Connection::~Connection()
{
    if (isConnected())
    {
        in.unsetDevice();

        m_sock->close();

        delete m_sock;
    }

    if (m_server != nullptr)
    {
        m_server->close();
        delete m_server;
    }
}

}