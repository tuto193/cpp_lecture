#ifndef _NetworkDataConstants_H_
#define _NetworkDataConstants_H_

#include <QString>

/**
 * Defines constants which are used to send different types of data
 * over the network.
 * Constants should be prefixed with 'NDC_'. If the constant is responsible
 * for things in the 3D view than use 'NDC_3D_' and for the 2D simply 'NDC_2D_'.
 * Everything else may only use the default prefix.
 *
 * To create an new command use the NDC_CMD_STRING macro.
 */
#define NDC_CMD_STRING(name) QString("NDC_" #name)

/////////////////
/// 3D constants
////////////////

/// update for the fighter position, including isShooting
#define NDC_3D_FIGHTER          NDC_CMD_STRING("3D_FGT")
/// send if player thinks he won
#define NDC_3D_WON              NDC_CMD_STRING("3D_WON")
/// send if player thinks he lost
#define NDC_3D_LOST             NDC_CMD_STRING("3D_LST")
/// send if player agrees to other players choice of won/los
#define NDC_3D_WON_LOST_ACT     NDC_CMD_STRING("3D_WL_ACT")


/////////////////
/// 2D constants
////////////////

/// send if player skips his turn
#define NDC_2D_SKIP             NDC_CMD_STRING("2D_SKIP")
/// send if player changes planet properties
#define NDC_2D_PLANET           NDC_CMD_STRING("2D_PLA")
/// send if player transports ore
#define NDC_2D_TRANS_ORE        NDC_CMD_STRING("2D_TO")
/// send if player transports a fighter
#define NDC_2D_TRANS_FGH        NDC_CMD_STRING("2D_TF")
/// send if player changes a shipyard's production mode
#define NDC_2D_CHG_PRD          NDC_CMD_STRING("2D_CP")
#define NDC_2D_MSG              NDC_CMD_STRING("2D_MSG")

/////////////////
/// Misc constants
////////////////
/// send if a message is send
#define NDC_MSG                 NDC_CMD_STRING("MSG")
/// send by player who wants to starts a fight
#define NDC_START_FIGHT         NDC_CMD_STRING("START_FGT")
/// send by one player after both agreed to the fight outcome
#define NDC_END_FIGHT           NDC_CMD_STRING("END_FGT")

#endif //_NetworkDataConstants_H_
