/**
  * Soll geoeffnet werden wenn ein Planet ausgewaehlt wird.
  * Es werden die Moeglichkeiten des Planeten angezeigt, d.h.
  * es koennen Minen, Werften ect. gebaut werden.
  *
  * @author: Helena Eckhardt
  **/
#include <iostream>
#include "planet_opt.h"
#include "ui_planet_opt.h"
#include "msgbox.h"
#include "existingbuildingerrormsg.h"
#include "nothingselectederror.h"
#include <QString>
#include <list>
#include "networking/NetworkInterface.hpp"
#include "../audio/GameSounds.hpp"

int planet_opt::x = 0;
int planet_opt::y = 0;

planet_opt::planet_opt(asteroids::Planet::Ptr planet, QWidget *parent) :
    QDialog(parent), ui(new Ui::planet_opt)
{
    this->gamePlanet = planet;
    ui->setupUi(this);
    init();
    auto gw = planet->getGameWorld();
    if(gw->getIsPlayerOne())
    {

    }

    // Connect the signals to the functions actually updating stats
    connect(this, &planet_opt::buildBuilding, &NetworkInterface::instance(), &NetworkInterface::updatePlanet);
    connect(this, &planet_opt::transportOre, &NetworkInterface::instance(), &NetworkInterface::slotTransportOre);
    connect(this, &planet_opt::transportShip, &NetworkInterface::instance(), &NetworkInterface::slotTransportFighter);
    connect(this, &planet_opt::changeProduction, &NetworkInterface::instance(), &NetworkInterface::slotChangeProduction);
}

void planet_opt::init()
{
    GameSounds::instance().play_2d_planetClick();
    std::string title = gamePlanet->getName();
    ui->planetLabel_2->setText(QString::fromStdString(title));
    ui->oreLbl_2->setText(QString::number(gamePlanet->getOreMined()));

    std::string mode;
    std::shared_ptr<asteroids::Building> building = gamePlanet->getBuilding();

    if(building)
    {
        switch(building->getType())
        {
            case asteroids::BUILDINGTYPE::MINE:
                ui->buildingLbl_2->setText(QString::fromStdString("Mine"));
                ui->buildMineBtn->hide();
                ui->buildShipyardBtn->show();
                ui->yardModeLbl_2->setText("No Shipyard");
                ui->changeModeBtn->hide();
                break;

            case asteroids::BUILDINGTYPE::SHIPYARD:
                ui->buildingLbl_2->setText(QString::fromStdString("Shipyard"));
                ui->buildMineBtn->show();
                ui->buildShipyardBtn->hide();
                if(std::dynamic_pointer_cast<asteroids::Shipyard>(building)->getMode() == asteroids::FIGHTER)
                {
                    mode = "Fighter";
                }
                else
                {
                    mode = "Transporter";
                }

                ui->yardModeLbl_2->setText(QString::fromStdString(mode));
                break;

            default:
                ui->buildingLbl_2->setText(QString::fromStdString("Default (None)"));
                break;
        }
    }
    else
    {
        ui->yardModeLbl_2->setText("No Shipyard");
        ui->changeModeBtn->hide();
        ui->buildingLbl_2->setText(QString::fromStdString("None"));
        ui->buildMineBtn->show();
        ui->buildShipyardBtn->show();
    }

    if(gamePlanet->getTransporterNumber() < 1)
    {
        ui->transportOreEdit->hide();
        ui->transportOreBtn->hide();
        ui->transportFighterBtn->hide();
        ui->transportToCbx->hide();
    }
    if(gamePlanet->getFighterNumber() < 1)
    {
        ui->transportFighterBtn->hide();
    }

    ui->transporterNumberLbl_2->setText(QString::number(gamePlanet->getTransporterNumber()));
    ui->fighterNumberLbl_2->setText(QString::number(gamePlanet->getFighterNumber()));

    std::list<std::shared_ptr<asteroids::Planet>> adjacents = gamePlanet->getAdjacents();
    std::list<std::shared_ptr<asteroids::Planet>>::iterator it;

    for(it = adjacents.begin(); it != adjacents.end(); it++)
    {
        ui->transportToCbx->addItem(QString::fromStdString((*it)->getName()));
    }

}

void planet_opt::on_transportFighterBtn_clicked()
{
    int selected = ui->transportToCbx->currentIndex();
    if(selected == -1)
    {
        // TODO show error window that there is nothing selected
        nothingSelectedError errWindow;
        errWindow.setModal(true);
        errWindow.exec();
    }
    try
    {
        auto adjacents = gamePlanet->getAdjacents().begin();
        std::advance(adjacents, selected);

        asteroids::Planet::Ptr selectedP = *adjacents;
        emit transportShip(gamePlanet->getId(), selectedP->getId());

        gamePlanet->getGameWorld()->turnTransportShip(gamePlanet, selectedP);
    }
    catch (const std::string& error)
    {

    }

    this->close();
}

planet_opt::~planet_opt()
{
    delete ui;
}

void planet_opt::on_closePlOptBtn_clicked()
{
    this->close();
}

void planet_opt::on_buildMineBtn_clicked()
{
    // There was a building already
    if(gamePlanet->getBuilding() != nullptr)
    {
        // The building was not a mine
        if(gamePlanet->getBuilding()->getType() != asteroids::MINE)
        {
            GameSounds::instance().play_2d_buildingRebuild();
            gamePlanet->getGameWorld()->turnBuildMine(gamePlanet);
            emit buildBuilding(gamePlanet->getId(), BUILDINGTYPE::MINE);
        }
        else
        {
            // There was already a mine there
            // Creates error window because there is already a mine on the planet
            existingBuildingErrorMsg errMsg(asteroids::MINE);
            errMsg.setModal(true);
            errMsg.exec();
        }

    }
    else
    {
        GameSounds::instance().play_2d_buildingBuild();
        gamePlanet->getGameWorld()->turnBuildMine(gamePlanet);
        std::cout << "End of call inside button click" << std::endl;
        emit buildBuilding(gamePlanet->getId(), BUILDINGTYPE::MINE);

    }

    this->close();
}

void planet_opt::on_buildShipyardBtn_clicked(){

    if(gamePlanet->getBuilding() != nullptr)
    {
        if(gamePlanet->getBuilding()->getType() != asteroids::SHIPYARD)
        {
            GameSounds::instance().play_2d_buildingRebuild();
            gamePlanet->getGameWorld()->turnBuildShipyard(gamePlanet);
            emit buildBuilding(gamePlanet->getId(), BUILDINGTYPE::SHIPYARD);
        }
        else
        {
            // Creates error window because there is already a shipyard on the planet
            existingBuildingErrorMsg errMsg(asteroids::SHIPYARD);
            errMsg.setModal(true);
            errMsg.exec();
        }

    }
    else
    {
        GameSounds::instance().play_2d_buildingBuild();
        gamePlanet->getGameWorld()->turnBuildShipyard(gamePlanet);
        std::cout << "End of call inside button click" << std::endl;
        emit buildBuilding(gamePlanet->getId(), BUILDINGTYPE::SHIPYARD);

    }

    this->close();
}

void planet_opt::on_transportOreBtn_clicked()
{
    int selected = ui->transportToCbx->currentIndex();
    if(selected == -1)
    {
        // TODO show error window that there is nothing selected
        nothingSelectedError errWindow;
        errWindow.setModal(true);
        errWindow.exec();
    }
    try
    {
        auto adjacents = gamePlanet->getAdjacents().begin();
        std::advance(adjacents, selected);
        asteroids::Planet::Ptr selectedP = *adjacents;
        gamePlanet->getGameWorld()->turnTransportOre(gamePlanet, selectedP, ui->transportOreEdit->text().toInt());
        emit transportOre(gamePlanet->getId(), selectedP->getId(), ui->transportOreEdit->text().toInt());
    } catch (const std::string& error)
    {

    }

    this->close();
}

void planet_opt::on_changeModeBtn_clicked()
{
    gamePlanet->getGameWorld()->turnChangeMode(gamePlanet);
    emit changeProduction(gamePlanet->getId());
    this->close();
}

void planet_opt::update()
{

}
