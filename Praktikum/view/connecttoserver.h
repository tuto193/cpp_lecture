#ifndef CONNECTTOSERVER_H
#define CONNECTTOSERVER_H

#include <QWidget>

namespace Ui {
class ConnectToServer;
}

class ConnectToServer : public QWidget
{
    Q_OBJECT

public:
    explicit ConnectToServer(QWidget *parent = nullptr);
    ~ConnectToServer();

private slots:
    /// Triggered if connect button is clicked
    void on_connect_button_clicked();

    /// Triggered if host button is clicked
    void on_server_field_returnPressed();

private:
    /// The Ui
    Ui::ConnectToServer *ui;

    /**
     * @brief Tries to connect to a given server.
     */
    void tryConnect() const;
};

#endif // CONNECTTOSERVER_H
