//
// Created by boubakr on 21.03.19.
//
#include "GraphicPlanet.hpp"
#include "Edge.hpp"
#include "Mapwidget.hpp"
#include "../datamodel/Gameworld.hpp"

namespace asteroids
{

MapWidget::MapWidget(QWidget *parent, Gameworld *gameWorld)
    : QGraphicsView(parent)
{
    pickedP = NULL;
    auto *graphicScene = new QGraphicsScene(this);

    /// Set graphic scene rectangle
    graphicScene->setSceneRect(parent->x(),
                               parent->y(),
                               parent->rect().width(),
                               parent->rect().height());
    setScene(graphicScene);
    setWindowTitle("Map");
    scale(qreal(0.7), qreal(0.7));
    setMinimumSize(parent->width(), parent->height());
    setBackgroundBrush(QImage("../img/planetmap.png"));
    setCacheMode(QGraphicsView::CacheBackground);
    setRenderHint(QPainter::Antialiasing);

    this->setMouseTracking(true);
    this->gameWorld = gameWorld;

    // Get planets from game world
    int i = 0;
    for (auto& planet : gameWorld->getPlanets())
    {
        int x = planet->getPosition()[0];
        int y = planet->getPosition()[1];
        QVector2D v(x, y);
        game_wPtr gp1 = make_shared<asteroids::GraphicPlanet>(this, v, planet);
        id_planet_map.insert(make_pair(i++, gp1));
    }

    /* Add edges to the scene */
    for (auto& e : gameWorld->getEdges())
    {
        Edge *edge = new Edge(id_planet_map.at(e.first), id_planet_map.at(e.second));
        graphicScene->addItem(edge);
    }

    colorPlanets();

    /* Add planets to the scene */
    for (auto const& v: id_planet_map)
    {
        graphicScene->addItem(v.second.get());
    }

    this->infoCard = new InfoCard;
}

void MapWidget::pick(GraphicPlanet *picked)
{
    pickedP = picked;
    pickedP->update();
}

void MapWidget::showInfoCard(asteroids::Planet::Ptr planet)
{
    QString name = QString::fromStdString(planet->getName());
    int num_t = planet->getTransporterNumber();
    int num_fighters = planet->getFighterNumber();
    int ore_tomine = planet->getOreToMine();
    int mined_ore = planet->getOreMined();
    QString building;
    if(planet->getBuilding() != nullptr)
    {
        if(planet->getBuilding()->getType() == asteroids::MINE)
        {
            building = QString::fromUtf8("Mine");
        }
        else
        {
            building = QString::fromUtf8("Shipyard");
        }
        
    }
    else
    {
        building = QString::fromUtf8("None");
    }
    

    this->infoCard->setInfoCardInfos(name, num_t, num_fighters, ore_tomine,
                                     mined_ore, building);

    QPoint p = this->infoCard->parentWidget()->mapFromGlobal(QCursor::pos());
    this->infoCard->setGeometry(QRect(p.x() + 20, p.y(), 0, 0));

    this->infoCard->show();
}

void MapWidget::hideInfoCard()
{
    this->infoCard->hide();
}

void MapWidget::colorPlanets()
{
    for (const auto& entry : id_planet_map)
    {
        auto& graphPlanet = entry.second;

        if (graphPlanet->getGamePlanet()->getOwner() == gameWorld->getP1())
        {
            graphPlanet->updateColor(Qt::green, Qt::darkGreen);
        }
        else if (graphPlanet->getGamePlanet()->getOwner() == gameWorld->getP2())
        {
            graphPlanet->updateColor(Qt::red, Qt::darkRed);
        }
    }
}

void MapWidget::refresh()
{
    colorPlanets();
    this->update();
}

MapWidget::~MapWidget()
{
    // Not deleting gameWorld here, since it is instantiated in mainWindow
    //delete pickedP;
    delete infoCard;
}
}