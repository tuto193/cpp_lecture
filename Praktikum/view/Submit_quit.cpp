#include "Submit_quit.h"
#include "ui_Submit_quit.h"

Submit_quit::Submit_quit(QWidget *parent) :
    QDialog(parent), ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Submit_quit::~Submit_quit()
{
    delete ui;
}

void Submit_quit::on_pushButton_clicked()
{
    close();
}

void Submit_quit::on_pushButton_2_clicked()
{
    exit(0);
}
