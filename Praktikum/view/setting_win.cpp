#include "setting_win.h"
#include "../build/ui_setting_win.h"

namespace asteroids
{

setting_win::setting_win(MainWindow *parent) :
    QDialog(parent), ui(new Ui::setting_win)
{
    ui->setupUi(this);

    // Saving the main window variable
    mw = parent;

    // Checking the current resolution
    if (mw->getWindowWidth() == 1152)
    {
        ui->res_1152x648->setChecked(true);
    }
    if (mw->getWindowWidth() == 1280)
    {
        ui->res_1280x720->setChecked(true);
    }
    if (mw->getWindowWidth() == 1600)
    {
        ui->res_1600x900->setChecked(true);
    }
    if (mw->getWindowWidth() == 1920)
    {
        ui->res_1920x1080->setChecked(true);
    }

    ui->soundOpt->setChecked(GameSounds::instance().getVolume() > 0.f);
    if (GameSounds::instance().getVolume() <= 0.f)
    {
        ui->soundScrollBar->setDisabled(true);
        ui->soundScrollBar->setValue(0);
    }
    else {
        ui->soundScrollBar->setValue(static_cast<int>(GameSounds::instance().getVolume() * 100));
    }
}

setting_win::~setting_win()
{
    delete ui;
}

void setting_win::on_backBtn_clicked()
{
    this->close();
}

void setting_win::on_applyBtn_clicked()
{
    if (ui->res_1920x1080->isChecked())
    {
        // Uncheck other buttons
        ui->res_1280x720->setChecked(false);
        ui->res_1600x900->setChecked(false);
        ui->res_1152x648->setChecked(false);

        // Give factor and new sizes to mainwindow
        mw->resizeWindow(1920/mw->getWindowWidth(), 1920, 1080);
    }

    if (ui->res_1600x900->isChecked())
    {
        ui->res_1280x720->setChecked(false);
        ui->res_1920x1080->setChecked(false);
        ui->res_1152x648->setChecked(false);
        mw->resizeWindow(1600/mw->getWindowWidth(), 1600, 900);
    }

    if (ui->res_1280x720->isChecked())
    {
        ui->res_1920x1080->setChecked(false);
        ui->res_1600x900->setChecked(false);
        ui->res_1152x648->setChecked(false);
        mw->resizeWindow(1280/mw->getWindowWidth(), 1280, 720);
    }

    if (ui->res_1152x648->isChecked())
    {
        // Uncheck other buttons
        ui->res_1280x720->setChecked(false);
        ui->res_1600x900->setChecked(false);
        ui->res_1920x1080->setChecked(false);

        // Give factor and new sizes to mainwindow
        mw->resizeWindow(1152/mw->getWindowWidth(), 1152, 648);
    }
}

void setting_win::on_soundOpt_clicked(bool toggle)
{
    if (!toggle)
    {
        value_Bef = ui->soundScrollBar->value();
        GameSounds::instance().adjustVolume(0.f);
        ui->soundScrollBar->setValue(0);
        ui->soundScrollBar->setDisabled(true);
    }
    else
    {
        ui->soundScrollBar->setValue(value_Bef);
        ui->soundScrollBar->setDisabled(false);
        GameSounds::instance().adjustVolume(ui->soundScrollBar->value() / 100.f);
    }
}

void setting_win::on_soundScrollBar_valueChanged(int value)
{
    GameSounds::instance().adjustVolume(value / 100.f);
    ui->volume_lbl->setText(QString::fromStdString(std::to_string(value) + "%"));
}

} //asteroids
