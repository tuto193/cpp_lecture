#include "help.h"
#include "../build/ui_help.h"

namespace asteroids
{

Help::Help(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Help)
{
    ui->setupUi(this);
}

Help::~Help()
{
    delete ui;
}

void Help::on_okBtn_clicked()
{
    this->close();
}

} // asteroids
