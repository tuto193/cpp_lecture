#ifndef SUBMIT_HOME_H
#define SUBMIT_HOME_H

#include <QDialog>
#include "MainWindow.hpp"

namespace Ui {
class Submit_Home;
}

namespace asteroids
{

/**
 * @brief Window which opens when pressed on "back to home" button
 *        to submit the decision of going back to the main menu
 */
class Submit_Home : public QDialog
{
    Q_OBJECT

public:
    explicit Submit_Home(MainWindow *parent = nullptr);
    ~Submit_Home();

private slots:

    /// triggered when clicked on "yes" button
    void on_yes_clicked();

    /// triggered when clicked on "no" button
    void on_no_clicked();

private:
    Ui::Submit_Home *ui;
    MainWindow* mw;
    //std::shared_ptr<MainWindow> mw;
};

} //asteroids
#endif // SUBMIT_HOME_H
