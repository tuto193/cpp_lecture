//
// Created by boubakr on 21.03.19.
//


#include "GraphicPlanet.hpp"
#include "Edge.hpp"
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include "iostream"

namespace asteroids {


	using namespace std;

	Edge::Edge(GraphicPlanet::Ptr from,GraphicPlanet::Ptr to)
	{
		source = from;
		dest = to;
	}

	void Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
	{

		float x1 = source->getCoords().x() + 25;
		float y1 = source->getCoords().y() + 25;
		float x2 = dest->getCoords().x() + 25;
		float y2 = dest->getCoords().y() + 25;

		painter->setRenderHint(QPainter::Antialiasing);
		QLineF line(x1, y1, x2, y2);

		painter->setPen(QPen(Qt::white, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter->drawLine(line);
	}

	QRectF Edge::boundingRect() const
	{
		return QRectF();
	}

}  // namespace asteroids