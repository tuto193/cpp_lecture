/*
 *  GLWidget.hpp
 *
 *  Created on: Jan. 14 2019
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef __GLWIDGET_HPP__
#define __GLWIDGET_HPP__

#include <string>

#include <GL/glew.h>
#include <QOpenGLWidget>
#include <QTimer>
#include <QPainter>

#include "view/Camera.hpp"
#include "rendering/SpaceCraft.hpp"
#include "rendering/Skybox.hpp"
#include "util/AsteroidField.hpp"
#include "physics/PhysicsEngine.hpp"

using namespace asteroids;
using std::shared_ptr;

/**
 * @brief   Implements the OpenGL View in a QT Widget
 *
 */
class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    GLWidget(QWidget* parent = NULL);

    virtual ~GLWidget();

    /// Parses a level file and loads all supported objects
    void setLevelFile(const std::string& file);

    /// Handles keyboard input and updates the physics engine
    void step(map<Qt::Key, bool>& keyStates);

    void startSync();

    void stopSync();

    SpaceCraft::Ptr& getActor() { return m_actor; }
    SpaceCraft::Ptr& getEnemy() { return m_enemy; }

public slots:
    /**
     * @brief Slot for updating the enemy spacecraft send by the other client.
     */
    void updateEnemy(const Vector3f& pos,
                     const Vector3f& xAxis,
                     const Vector3f& yAxis,
                     const Vector3f& zAxis,
                     const Quaternion& rot,
                     const Vector3f& dir,
                     bool isShooting);

signals:
    /**
     * @brief Signal actor if shooting
     * @param isShooting True if shooting
     */
    void signalActor(const SpaceCraft::Ptr&, bool isShooting);

    /**
     * @brief Signal is send when the fight has ended.
     */
    void signalFightEnded();

protected:

    /// Init OpenGL
    virtual void initializeGL() override;

    /// Render scene
    virtual void paintGL() override;

    /// Handle new window dimenions
    virtual void resizeGL(int w, int h) override;

private:

    /// Name of the given level file
    string                      m_levelFile;

    /// The virtual camera
    Camera						m_camera;

    /// A pointer to the Actor
    SpaceCraft::Ptr  	        m_actor;

    /**
     * Is set to true if actor is shooting. Then used to send to other player
     */
    bool m_actorShooting = false;

    /// A pointer to the enemy actor
    SpaceCraft::Ptr  	        m_enemy;

    /// A skybox for the scene
    Skybox::Ptr			        m_skybox;

    /// Pointer to the asteroid field
    AsteroidField::Ptr          m_asteroidField;
    
    /// Physics 
    PhysicsEngine::Ptr          m_physicsEngine;

    /**
     * Timer for sending actor data to other player
     */
    QTimer* m_timer;

    /**
     * Toggle switch for shooting. This is used to throttle the shooting
     */
    bool m_shootEnabled;

    /**
     * @brief Draws actors and enemies health bar
     */
    void drawHealthbars(QPainter& p) const;

    /**
     * @brief Draws the cross hair
     */
    void drawCrosshair(QPainter& p) const;

    /**
     * @brief Draws the offscreen indicator
     */
    void drawEnemyPosHint(QPainter &p) const;

    /**
     * @brief True if the end of a fight is emitted
     */
    bool m_endFightEmitted = false;
};

#endif
