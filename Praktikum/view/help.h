#ifndef HELP_H
#define HELP_H

#include <QDialog>

namespace Ui {
class Help;
}

namespace asteroids
{

/**
 * @brief The Help class opens a window to get some information about the game
 */
class Help : public QDialog
{
    Q_OBJECT

public:
    explicit Help(QWidget *parent = nullptr);
    ~Help();

private slots:

    /// Triggered in case the ok button is clicked
    void on_okBtn_clicked();

private:
    Ui::Help *ui;
};

} //asteroids
#endif // HELP_H
