#ifndef GAMEOVERWINDOW_H
#define GAMEOVERWINDOW_H

#include <QDialog>

namespace Ui {
class gameOverWindow;
}

/**
 * @brief The gameOverWindow class  Window to appear at the end of the game
 */
class gameOverWindow : public QDialog
{
    Q_OBJECT

public:
    explicit gameOverWindow(QWidget *parent = 0, bool lose = true);
    ~gameOverWindow();

private slots:

    void on_mainMenuBtn_clicked();

private:
    Ui::gameOverWindow *ui;
};

#endif // GAMEOVERWINDOW_H
