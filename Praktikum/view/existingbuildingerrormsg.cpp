#include "existingbuildingerrormsg.h"
#include "../build/ui_existingbuildingerrormsg.h"

#include "QString"


existingBuildingErrorMsg::existingBuildingErrorMsg(asteroids::BUILDINGTYPE type, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::existingBuildingErrorMsg)
{
    ui->setupUi(this);

        switch(type)
        {
            case asteroids::BUILDINGTYPE::MINE:
                ui->buildingLbl->setText(QString::fromStdString("a Mine!"));
                break;
            case asteroids::BUILDINGTYPE::SHIPYARD:
                ui->buildingLbl->setText(QString::fromStdString("a Shipyard!"));
                break;
            default:
                ui->buildingLbl->setText(QString::fromStdString("None"));
                break;
        }
}

existingBuildingErrorMsg::~existingBuildingErrorMsg()
{
    delete ui;
}

void existingBuildingErrorMsg::on_okBtn_clicked()
{
    this->close();
}


