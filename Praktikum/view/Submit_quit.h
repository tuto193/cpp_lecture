#ifndef Submit_quit_H
#define Submit_quit_H

#include <QDialog>

namespace Ui {
class Dialog;
}

/**
 * @brief Window which opens when pressing on any exit button
 *        to submit the decision of exiting the game
 */
class Submit_quit : public QDialog
{
    Q_OBJECT

public:
    // Ctor
    explicit Submit_quit(QWidget *parent = nullptr);
    // Dtor
    ~Submit_quit();

private slots:

    /// Triggered if "yes" clicked
    void on_pushButton_clicked();

    /// Triggered if "no" clicked
    void on_pushButton_2_clicked();

private:
    /// The dialog window
    Ui::Dialog *ui;
};

#endif // Submit_quit_H
