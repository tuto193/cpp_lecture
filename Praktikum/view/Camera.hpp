/*
 *  Camera.hpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

#include "math/Vector.hpp"
#include "rendering/SpaceCraft.hpp"
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define PI 3.14159265
#define PH 1.57079632

namespace asteroids
{


/**
 * @brief Class to represent a virtual camera using gluLookAt
 *
 */
class Camera
{
public:
    /**
     * @brief Enumeration to encode types of camera movements
     */
    enum CameraMovement
    {
	    FORWARD,
	    BACKWARD,
	    LEFT,
	    RIGHT,
	    UP,
	    DOWN
    };

    /**
     * @brief Construct a new Camera object at (0, 0, 0) with
     *        upward orientation and lookAt at (0, 0, -1)
     *
     */
    Camera();

    /**
     * @brief Construct a new Camera object with upward orientation
     *        and lookAt at (0, 0, -1)
     *
     * @param position      Initial position
     * @param turnSpeed     Turning speed in radians per call
     * @param moveSpeed     Move speed in world units per call
     */
    Camera(SpaceCraft::Ptr craft);

    /**
     * Sets SpaceShip to follow
     * @param craft
     */
    void initializeCamera(SpaceCraft::Ptr craft);

    /**
     * @brief Moves the camera according to given direction
     *
     * @param dir           Moving direction
     */
    void move(const CameraMovement& dir);

     /**
     * @brief turns the camera according to given direction
     *
     * @param dir           Moving direction
     */
    void turn(const CameraMovement& dir);

    /**
     * @brief Calls gluLookAt with the internal parameters
     *
     */
    void apply();

    /**
     * @brief Get the Camera Position object
     *
     * @return Vector3f
     */
    Vector3f getPosition() const;

	/**
	 * Resets Camera position to follow the SpaceShip
	 * @param ship
	 */
	void followSpaceCraft();

    /**
     * @brief Set the turn speed  of the camera
     *
     * @param speed         The new turn speed
     */
    void setTurnSpeed(const float& speed) { m_turnSpeed = speed;}

    /**
     * @brief Set the move speed  of the camera
     *
     * @param speed         The new move speed
     */
    void setMoveSpeed(const float& speed) { m_moveSpeed = speed;}

    /**
     * @brief checks wether a vector is in the actor's view
     * @param vector 3d point in space
     * @param w width
     * @param h height
     * @return  a vector with  -1 <= x <= 1 and -1 <= y <= 1 if the vector is the view
     */
    glm::vec4 worldToViewport(Vector3f &vector, int w, int h) const;
    /**
     * @brief convert from viewport to screen coordinates
     * @param vector 3d point in space (opponent)
     * @param w width of the screen
     * @param h heigt of the screen
     * @return 2d in screen to draw the indicator
     */
    glm::vec2 viewportToScreen(glm::vec2 viewport, int w, int h) const;

private:
    /// View up vector
    Vector3f  m_up;

    /// Translation
    Vector3f  m_trans;

    /// Look at Vector
    Vector3f  m_l;

    /// Rotation angles encoded in Vector, i.e., x is the
    /// rotation around the x-axis and so on
    Vector3f  m_rot;

    /// Turn speed in radians per call
    float   m_turnSpeed;

    /// Move speed in world units per call
    float   m_moveSpeed;

    /// Spacecraft
    SpaceCraft::Ptr m_ship;
};

} // namespace asteroids

#endif
