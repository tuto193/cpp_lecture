#ifndef MSGBOX_H
#define MSGBOX_H

#include <QDialog>

namespace Ui {
class msgBox;
}

class msgBox : public QDialog
{
    Q_OBJECT

public:
    // Ctor
    explicit msgBox(QWidget *parent = 0);

    // Dtor
    ~msgBox();

    /**
     * @brief Shows message if mine is built
     */
    void updateMine();
    /**
     * @brief Shows message if shipyard is built
     */
    void updateWerft();

private slots:

    /// Triggered if ok button is clicked
    void on_OkBtn_clicked();

private:
    /// The message box
    Ui::msgBox *ui;
};

#endif // MSGBOX_H
