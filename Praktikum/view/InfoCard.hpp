#ifndef INFOCARD_HPP
#define INFOCARD_HPP

#include <QWidget>

namespace Ui {
class InfoCard;
}

class InfoCard : public QWidget
{
    Q_OBJECT

public:

    explicit InfoCard(QWidget *parent = nullptr);

    ~InfoCard();

    /**
     * Sets Infocard's fields
     */
    void setInfoCardInfos(QString name,int transporters,int fighters,
						  int oreToMine,int minedOre,QString ships);

private:
    /// The info card
    Ui::InfoCard *ui;
};

#endif // INFOCARD_HPP
