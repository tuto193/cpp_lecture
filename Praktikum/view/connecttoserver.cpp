#include "connecttoserver.h"
#include "ui_connecttoserver.h"
#include <QDesktopWidget>
#include "networking/Connection.hpp"

ConnectToServer::ConnectToServer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConnectToServer)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());
    const QRect screen = QApplication::desktop()->screenGeometry();
    this->move(screen.center() - this->rect().center());
}

ConnectToServer::~ConnectToServer()
{
    delete ui;
}

void ConnectToServer::on_connect_button_clicked()
{
    tryConnect();
}

void ConnectToServer::on_server_field_returnPressed()
{
    tryConnect();
}

void ConnectToServer::tryConnect() const
{
    if (ui->server_field->text().isEmpty())
    {
        ui->server_error->setText("<font color='#CC0000'>No Server IP found...</font>");
    } else
    {
        asteroids::Connection::instance().setIp(ui->server_field->text());
        bool connected = asteroids::Connection::instance().connectTo();

        if (connected)
        {
            ui->server_error->setText("<font color='#00CC00'>Connecting...</font>");
        }
        else
        {
            ui->server_error->setText("<font color='#CC0000'>Could not connect to Server...</font>");
        }
    }
}
