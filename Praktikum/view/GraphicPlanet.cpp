//
// Created by boubakr on 21.03.19.
//

#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include "Mapwidget.hpp"
#include "GraphicPlanet.hpp"
#include "planet_opt.h"
#include <QRadialGradient>
#include "../audio/GameSounds.hpp"

namespace asteroids
{
using namespace std;

GraphicPlanet::GraphicPlanet(MapWidget *_mapWidget, QVector2D _coords, Planet::Ptr gameP)
    : mapWidget(_mapWidget), coords(_coords)
{
    this->color = Qt::gray;
    this->darkerColor = Qt::darkGray;
    this->setAcceptHoverEvents(true);
    gamePlanet = gameP;
}

GraphicPlanet::GraphicPlanet(const asteroids::GraphicPlanet& other)
{
    this->gamePlanet = other.gamePlanet;
    this->mapWidget = other.mapWidget;
    this->coords = other.coords;
    this->color = other.color;
    this->darkerColor = other.darkerColor;
}

void GraphicPlanet::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRadialGradient gradient(this->coords.x() + 12, this->coords.y() + 12, 25);
    gradient.setColorAt(0, color);
    gradient.setColorAt(1, darkerColor);
    painter->setBrush(gradient);

    if (gamePlanet->isHomePlanet())
    {
        painter->setPen(QPen(Qt::white, 8));
    }
    else
    {
        painter->setPen(QPen(Qt::white, 2));
    }

    painter->drawEllipse(static_cast<int>(this->coords.x()), static_cast<int>(this->coords.y()), 50, 50);
    painter->setFont(QFont("Times", 16, 10, true));
    painter->setPen(QPen(Qt::yellow, 50, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter->drawText(coords.x() - 50, coords.y() - 10, QString::fromStdString(gamePlanet->getName()));
}

QRectF GraphicPlanet::boundingRect() const
{
    return QRectF(coords.x() + 5, coords.y() + 3, 40, 45);
}

void GraphicPlanet::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        mapWidget->pick(this);
        if(this->gamePlanet->getOwner() == this->gamePlanet->getGameWorld()->getMe() && this->gamePlanet->getGameWorld()->getState())
        {
            planet_opt options(this->gamePlanet);
            options.setModal(true);
            options.exec();
        }
    }
}

QVector2D GraphicPlanet::getCoords()
{
    return coords;
}

void GraphicPlanet::setCoords(QVector2D _coords)
{
    coords = _coords;
}

void GraphicPlanet::updateColor(const QColor& color, const QColor& darker)
{
    this->color = color;
    this->darkerColor = darker;
    this->update();
}

void GraphicPlanet::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(Qt::PointingHandCursor);
    GameSounds::instance().play_2d_planetHover();
    mapWidget->showInfoCard(this->gamePlanet);
}

void GraphicPlanet::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    mapWidget->hideInfoCard();
}

}  // namespace asteroids