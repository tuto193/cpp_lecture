#ifndef HOSTCONNECT_H
#define HOSTCONNECT_H


#include <QDialog>
#include "connecttoserver.h"

namespace Ui {
class hostConnect;
}

/**
 * @brief The hostConnect class Window to connect the players to play
 */
class hostConnect : public QDialog
{
    Q_OBJECT

public:

    /**
     * @brief Constructor
     */
    explicit hostConnect(QWidget *parent = nullptr);

    //Dtor
    ~hostConnect();

public slots:

    /// Triggered if "connect to server" is clicked
    void on_connect_ToServer_clicked();

    /// Triggered if "host" is clicked
    void on_host_clicked();

private:

    Ui::hostConnect *ui;

    ConnectToServer *connect_server;

protected:
    /**
     * @brief Closes the server when the window is closed an server is opened.
     */
    void closeEvent(QCloseEvent *event) override;
};

#endif // HOSTCONNECT_H

