/*
 *  MainWindow.hpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#ifndef __MAINWINDOW_HPP__
#define __MAINWINDOW_HPP__

#include <string>

#include <QMainWindow>

#define GL3_PROTOTYPES 1
#include <GL/glew.h>

#include "GLWidget.hpp"

#include "ui_MainWindow.h"
#include "planet_opt.h"
#include "hostconnect.h"

#include "Mapwidget.hpp"
#include <memory>
#include "../audio/GameSounds.hpp"
#include <QEvent>
#include <QWidget>
#include <QDebug>

namespace asteroids
{
/**
 * @brief   Represents the main window of the game. This
 *          class contains the main loop, handles all
 *          user input and renders all objects
 *
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /**
     * @brief Construct a new Main Window object
     *
     * @param plyname  A .ply file to render
     */
    MainWindow(const std::string& plyname, QWidget* parent = NULL);

    /**
     * @brief Destroys the Main Window object
     *
     */
    ~MainWindow();


    /**
     * @brief Hides the menu buttons
     */
    void hideMenuButtons();

    /**
     * @brief Resizes the window to scale, width and height
     * @param scaleResize Scale
     * @param windowWidth Width
     * @param windowHeight Height
     */
    void resizeWindow(float scaleResize, int windowWidth, int windowHeight);

    /**
     * @brief sets the current page (title screen, game screen, main menu)
     * @param index
     */
    void setCurrentIndex (int index);

    /// Returns window width
    float getWindowWidth();

public Q_SLOTS:

    /// Handle input
    void handleInput();

protected:

    /// Called if a key was pressed
    virtual void keyPressEvent(QKeyEvent* event) override;

    /// Called if a key was released
    virtual void keyReleaseEvent(QKeyEvent* event) override;

    /// Called if mouse key is pressed
    virtual void mousePressEvent(QMouseEvent *event) override;

    /// Sets Title and Menu Screen
    void setTitleAndMainMenuScreen(bool ot);

    /**
     * @brief Listen for the close event to gracefully exit the application
     */
    void closeEvent(QCloseEvent *event) override;

private slots:

    /// Triggered if  menu button is clicked
    void on_menue_Btn_clicked();

    /// Triggered if quit button is clicked
    void on_quit_Btn_clicked();

    /// Triggered if  skip button is clicked
    void on_skipButton_clicked();

    /// Triggered if title screen button is clicked
    void on_titleScr_Btn_clicked();

    /// Triggered if start button is clicked
    void on_start_Btn_clicked();

    /// Triggered if exit button is clicked
    void on_exit_Btn_clicked();

    /// Triggered if settings button 2 is clicked
    void on_settings_Btn_2_clicked();

    /// Triggered if settings button is clicked
    void on_settings_Btn_clicked();

    /// Triggered if home button is clicked
    void on_homeBtn_clicked();

    /// Triggered if help button is clicked
    void on_help_Btn_clicked();

    /// Triggered if the round count goes up
    void on_signalRoundCountUp();

    /// Triggered if turn is skipped
    void slotTurnSkipped();

    /// Triggered if new planet updates available
    void slotNewPlanetUpdate(int p, BUILDINGTYPE type);

    /// Triggered if ore is transported
    void slotSignalTransportOre(int p1, int p2, int amount);

    /// Triggered if fighter is transported
    void slotSignalTransportFighter(int p1, int p2);

    /// Triggered if production of a shipyard changes
    void slotChangeProduction(int p);

    void slotNextTurn(bool state);

    /// Triggered by start fight signal
    void slotStartFight();

    /// Triggered by GLWidget if fight won
    void slotWinFight();

    /// Triggered by NetworkInterface if fight lost
    void slotLoseFight();

    /**
     * @brief updateLabels  Updates the labels that shows the number of the items from a player on
     *                      the MainWindow
     * @param trans     number of transporters the player owns
     * @param ship      number of ships the player owns
     * @param ore       number of ore the player owns
     */
    void updateLabels(int trans, int ship, int ore);
    /**
     * @brief Slot to start the Game
     * @param isPlayerOne
     */
    void slotStartGame(bool isPlayerOne);

    /**
     * @brief Slot to End game with a Win
     */
    void slotWinGame();

    /**
     * @brief Slot to End game with a lose
     */
    void slotLoseGame();

    /**
     * @brief on_lineEdit_returnPressed  Sends the message quick message to other player
     */
    void on_lineEdit_returnPressed();

    ///Triggered if a Message was send
    void slotReceiveMsg(QString message);

signals:

    /// Called if skip button is clicked
    void skipButton_clicked();

    /// Called if a Message should be send
    void signalMsgSend(QString message);


private:
    /**
     * @brief Start the fight, thus hiding the planet map and showing
     *        the glWidget
     */
    void fight(const Vector3f& myPos, bool signaled = false);

    /**
     * @brief End the fight, showing the planet map and destroying the widget
     */
    void endFight();

    /**
     * @brief Sends "It's your turn" to the info box, if it's your turn
     */
    void itsYourTurn();

    /// Default window size
    float m_windowWidth = 1280;

    /// Default window height
    float m_windowHeight = 720;

    QString getPath(bool title, bool ot);

    /// QT UI of the window
    Ui::MainWindow* ui;

    /// Path to the level file
    std::string m_levelFilePath;

    /// Gl widget
    GLWidget*       m_widget;

    /// Map with the keys and their states
    map<Qt::Key, bool>          m_keyStates;

    /// 60 fps timer
    shared_ptr<QTimer>          m_timer;

    /// Boolean to open and close the menue
    bool menueClicked = false;

    /// Is set to true if the glWidget is active
    bool m_isFightRunning = false;

    /// Button or Enter were clicked to send data (to the other player? Chat? Cheat-Codes?)
    void send();

    /// Ends the game and tidies up
    void endGame(bool lose);

    /// The planet map
    MapWidget *planetMap;

    /// The game world
    std::shared_ptr<Gameworld> gameWorld;

    /// Host connect window
    hostConnect m_connectModal;

        /**
		 * @brief class CustomEventFilter overrides the eventFilter function
		 */
		class CustomEventFilter : public QObject {
		public:
			bool eventFilter(QObject *object, QEvent *event) {
				if (event->type() == QEvent::HoverEnter)
				{
					GameSounds::instance().play_2d_menuHover();
					return true;
				}
				else
				{
					return QObject::eventFilter(object, event);
				}
			}
		};

    /// easteregg variable
    bool thomasModus = false;

    /// path to background image
    QString pathToBackground = "../img/planetmap.png";

};

}

#endif
