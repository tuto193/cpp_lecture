/*
 *  Camera.cpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include <stdio.h>
#include <math.h>

#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

#include "view/Camera.hpp"

namespace asteroids
{

Camera::Camera(){}

Camera::Camera(SpaceCraft::Ptr craft)
{

    // Camera now moves with the Spacecraft
    m_trans  = craft->getPosition();

    /* Init look up vector */
    m_up[0]  = 0.0f;
    m_up[1]  = 1.0f;
    m_up[2]  = 0.0f;

    /* Init look at vector */
    m_l[0]   = 0.0f;
    m_l[1]   = 0.0f;
    m_l[2]   = 0.0f;

    /* Init rotation angles */
    m_rot[0] = 0.0f;
    m_rot[1] = 0.0f;
    m_rot[2] = 0.0f;

}

void Camera::initializeCamera(SpaceCraft::Ptr craft)
{
    m_ship = craft;
}


void Camera::move(const CameraMovement& dir)
{
    switch(dir)
    {
        case FORWARD:
            m_trans[0] += m_moveSpeed * sin(m_rot[1]);
            m_trans[2] += m_moveSpeed * cos(m_rot[1]);
            break;
        case BACKWARD:
            m_trans[0] -= m_moveSpeed * sin(m_rot[1]);
            m_trans[2] -= m_moveSpeed * cos(m_rot[1]);
            break;
        case LEFT:
            m_trans[0] -= m_moveSpeed * sin(PH - m_rot[1]);
            m_trans[2] -= m_moveSpeed * cos(PH - m_rot[1]);
            break;
        case RIGHT:
            m_trans[0] += m_moveSpeed * sin(PH + m_rot[1]);
            m_trans[2] += m_moveSpeed * cos(PH + m_rot[1]);
            break;
        case UP:
            m_trans[1] += m_moveSpeed;
            break;
        case DOWN:
            m_trans[1] -= m_moveSpeed;
            break;
    }
}


void Camera::turn(const CameraMovement& dir)
{
    if(dir == UP)
    {
        if(m_rot[0] < PH)
        {
            m_rot[0] -= m_turnSpeed;
        }
    }
    else if(dir == DOWN)
    {
        if(m_rot[0] < PH)
        {
            m_rot[0] += m_turnSpeed;
        }
    }
    else if(dir == LEFT)
    {
        m_rot[1] -= m_turnSpeed;
    }

    else if(dir == RIGHT)
    {
        m_rot[1] += m_turnSpeed;
    }
}

void Camera::apply()
{
    /* Clear matrix stack */
    glLoadIdentity();

    /* Apply transformation */
    gluLookAt(m_trans[0], m_trans[1], m_trans[2],
              m_l[0], m_l[1], m_l[2],
              m_up[0], m_up[1], m_up[2]);
}

void Camera::followSpaceCraft()
{
    m_trans = m_ship->getPosition() - m_ship->getDirection()*100 + m_ship->getZAxis()*40;

    m_l = m_ship->getPosition() + m_ship->getDirection()*1000;

    m_rot = m_ship->getXAxis();

    m_up = m_ship->getZAxis();
}

Vector3f Camera::getPosition () const
{
    return m_trans;
}


glm::vec2 Camera::viewportToScreen(glm::vec2 viewport, int w, int h) const {

  // convert the vector in screen coordinates
  glm::vec2 screen;
  screen.x = w*(viewport.x + 1)/2;
  screen.y = h*(1.0f - (viewport.y + 1)/2);
  // make sure the indicator stays on the screen ! clamp
  screen.x = glm::clamp(screen.x, 0.0f, 1.0f*w - 30); // -30 for drawing purposes
  screen.y = glm::clamp(screen.y, 0.0f, 1.0f*h - 30);

  return glm::vec2(screen.x, screen.y);
}
glm::vec4 Camera::worldToViewport(Vector3f &vector, int w, int h) const {
  // using MVP , model-view-projection
  // projection matrix
  float ratio = w*1.0f/h;
  glm::mat4 projection = glm::perspective(glm::radians(45.0f), ratio, 1.0f, 10000.0f);

  //camera matrix
  glm::mat4 view = glm::lookAt(
      glm::vec3(m_trans[0], m_trans[1], m_trans[2]),  // position
      glm::vec3(m_l[0], m_l[1], m_l[2]),              // look at
      glm::vec3(m_up[0], m_up[1], m_up[2])            // up
  );
  // model matrix
  glm::mat4 model = glm::mat4(1.0);
  glm::vec4 vec4(vector[0], vector[1], vector[2], 1);

  glm::vec4 temp = projection*view*model*vec4;
  // dehomogenize the vector
  temp.x /= temp.w;
  temp.y /= temp.w;
  return temp;
}

} // namespace asteroids
