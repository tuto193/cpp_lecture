//
// Created by boubakr on 21.03.19.
//

#ifndef CPP18_ASTEROIDS_MAPWIDGET_HPP
#define CPP18_ASTEROIDS_MAPWIDGET_HPP

#include <QGraphicsView>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <memory>
#include <map>
#include "InfoCard.hpp"
#include "../datamodel/Planet.hpp"

using namespace std;

namespace asteroids
{

class GraphicPlanet;

class Edge;

class Gameworld;

class MapWidget : public QGraphicsView
{

Q_OBJECT

    using game_wPtr = std::shared_ptr<GraphicPlanet>;

public:

    MapWidget(QWidget *parent = nullptr, Gameworld *gameWorld = nullptr);

    /**
     * @return the Gameworld
     */
    Gameworld *getGameWorld() { return gameWorld; }

    /**
     * @brief Picks the respective planet
     * @param picked Planet
     */
    void pick(GraphicPlanet *picked);

    GraphicPlanet *getPicked() { return pickedP; }

    /**
     * @brief Display info card
     */
    void showInfoCard(asteroids::Planet::Ptr planet);

    /**
     * @brief Hide Info card
     */
    void hideInfoCard();

    /**
     * @brief Update the colors of the graphic planets according to their ownership.
     */
    void colorPlanets();

    /**
     * @brief Colors the planets and updates the scene to reflect the changes.
     */
    void refresh();

    // Dtor
    virtual ~MapWidget();

private:

    /// Picked planet
    GraphicPlanet *pickedP;

    /// Map with the pair < id , planet >
    map<int, game_wPtr> id_planet_map;

    /// The gameworld
    Gameworld *gameWorld;

    /// The infocard
    InfoCard *infoCard;

};
}


#endif //CPP18_ASTEROIDS_MAPWIDGET_HPP