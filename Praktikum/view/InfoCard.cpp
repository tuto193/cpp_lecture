#include "InfoCard.hpp"
#include "ui_infocard.h"

InfoCard::InfoCard(QWidget *parent) :
		QWidget(parent), ui(new Ui::InfoCard)
{
	ui->setupUi(this);
	this->setFixedSize(this->size());
	this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
}

InfoCard::~InfoCard()
{
	delete ui;
}

void InfoCard::setInfoCardInfos(QString name, int transporters, int fighters,
								int oreToMine, int minedOre, QString building)
{
	ui->nameLabel->setText(name);
	ui->l_transporters->setText(QString::number(transporters));
	ui->l_fighters->setText(QString::number(fighters));
	ui->l_ore->setText(QString::number(oreToMine));
	ui->l_minedOre->setText(QString::number(minedOre));
	ui->l_building->setText(building);
}

