#ifndef NOTHINGSELECTEDERROR_H
#define NOTHINGSELECTEDERROR_H

#include <QDialog>

namespace Ui {
class nothingSelectedError;
}

class nothingSelectedError : public QDialog
{
    Q_OBJECT

public:
    // Ctor
    explicit nothingSelectedError(QWidget *parent = 0);
    // Dtor
    ~nothingSelectedError();

private slots:
    /// Triggered if ok button is clicked
    void on_okBtn_clicked();

private:
    /// Nothin selected Error
    Ui::nothingSelectedError *ui;
};

#endif // NOTHINGSELECTEDERROR_H
