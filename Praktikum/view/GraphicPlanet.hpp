//
// Created by boubakr on 21.03.19.
//

#ifndef CPP18_ASTEROIDS_GRAPHICPLANET_HPP
#define CPP18_ASTEROIDS_GRAPHICPLANET_HPP


#include <QGraphicsItem>
#include <QVector2D>
#include <string>
#include <memory>
#include <QGraphicsSceneMouseEvent>
#include "InfoCard.hpp"
#include <QColor>
#include "../datamodel/Planet.hpp"


using namespace std;


namespace asteroids {
    class MapWidget;

    class GraphicPlanet : public QGraphicsItem
    {

	public:

		using Ptr = std::shared_ptr<GraphicPlanet>;

		/* Constructors */
		/**
		 * @brief Construct a GraphicPlanet from name and coordinates and the parent widget
		 * @param _mapWidget widget that will contain this GraphicPlanet
		 * @param name name of the GraphicPlanet
		 * @param coords coordinates of the GraphicPlanet
		 */
		GraphicPlanet(MapWidget *_mapWidget,QVector2D coords, Planet::Ptr gameP);

		/**
		 * @brief Copy-Constructor
		 * @param other Planet to be copied
		 */
		GraphicPlanet(const GraphicPlanet &other);

		/**
		 * @brief Defines the outer bounds of the item as a rectangle
		 * @return the outer bounds of the item as  rectangle QRectF
		 */
		QRectF boundingRect() const override;

		/**
		 * @brief Paints the content of an item in local coordinates
		 * @param painter QPainter
		 * @param option QStyleOptionGraphicsItem
		 * @param widget QWidget
		 */
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

		/**
		 * @brief Set the name of the GraphicPlanet
		 * @param name the new name
		 */
		void setName(string name);

		/**
		 * @brief Set the coordinates of the GraphicPlanet
		 * @param coords the new coordinates
		 */
		void setCoords(QVector2D coords);

        /**
         * @brief getGamePlanet get the active planet
         * @return Pointer of the active planet
         */
		std::shared_ptr<Planet> getGamePlanet() { return gamePlanet; }

		/**
		 * @brief getCoords
		 * @return coordinates of the GraphicPlanet
		 */
		QVector2D getCoords();

		/**
		 * Update the planet's color
		 * @param color
		 * @param darker
		 */
		void updateColor(const QColor &color, const QColor &darker);

	protected:

        /**

         * @brief What happens in case of a mouse event
         * @param event Mouse Event
         */
		void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

		/**
	     * @brief Displays Planet info on hover
	     * @param event Hover Event
	     */
		void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;

		/**
		 * @brief Hides planet Info
		 * @param event Hover Event
         */
		void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

	private:

		/// Map widget
		MapWidget *mapWidget;

        /// The game planet
		Planet::Ptr gamePlanet;

		/// The coordinates of a planet
		QVector2D coords;

		/// Planet's color
		QColor color;

		/// Planet's darker color
		QColor darkerColor;
	};

} // namespace asteroids

#endif //CPP18_ASTEROIDS_GRAPHICPLANET_HPP
