#ifndef EXISTINGBUILDINGERRORMSG_H
#define EXISTINGBUILDINGERRORMSG_H

#include <QDialog>
#include "../datamodel/Enums.hpp"



namespace Ui {
class existingBuildingErrorMsg;
}

/**
 * @brief The existingBuildingErrorMsg class    Window appears if the player
 *          wants to build a building on a planet that already has a building
 *          of the same type.
 */
class existingBuildingErrorMsg : public QDialog
{
    Q_OBJECT

public:
    explicit existingBuildingErrorMsg(asteroids::BUILDINGTYPE type, QWidget *parent = 0);
    ~existingBuildingErrorMsg();


private slots:

    /**
     * @brief on_okBtn_clicked  Closes the window if clicked on Ok
     */
    void on_okBtn_clicked();

private:
    Ui::existingBuildingErrorMsg *ui;


};

#endif // EXISTINGBUILDINGERRORMSG_H

