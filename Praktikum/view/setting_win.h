#ifndef SETTING_WIN_H
#define SETTING_WIN_H

#include <QDialog>
#include "MainWindow.hpp"

namespace Ui {
class setting_win;
}

namespace asteroids
{

class setting_win : public QDialog
{
    Q_OBJECT

public:
    // Ctor
    explicit setting_win(MainWindow *parent = nullptr);
    // Dtor
    ~setting_win() override;

private slots:
    /// Triggered if back button is clicked
    void on_backBtn_clicked();

    /// Triggered if apply button is clicked
    void on_applyBtn_clicked();

    /// sets the volume to 0 and disables the slider
    void on_soundOpt_clicked(bool toggle);
    /// sets the volume to the value of the slider
    void on_soundScrollBar_valueChanged(int value);

private:
    /// The settings window
    Ui::setting_win *ui;

    /// The main window
    MainWindow* mw;

    int value_Bef = 100;
};

} //asteroids
#endif // SETTING_WIN_H
