#include "submit_home.h"
#include "ui_submit_home.h"

namespace asteroids
{

Submit_Home::Submit_Home(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::Submit_Home)
{
    ui->setupUi(this);
    //mw = std::shared_ptr<MainWindow>(parent);
    mw = parent;
}

Submit_Home::~Submit_Home()
{
    delete ui;
}

void Submit_Home::on_yes_clicked()
{
    mw->setCurrentIndex(2);
    close();
}

void Submit_Home::on_no_clicked()
{
    close();
}

} //asteroids
