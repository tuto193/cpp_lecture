#include "msgbox.h"
#include "ui_msgbox.h"

msgBox::msgBox(QWidget *parent) :
    QDialog(parent), ui(new Ui::msgBox)
{
    ui->setupUi(this);
}

msgBox::~msgBox()
{
    delete ui;
}

void msgBox::on_OkBtn_clicked()
{
    this->close();
}

void msgBox::updateMine()
{
    ui->bauLabel->setText("Your Mine will be built!");
}

void msgBox::updateWerft()
{
    ui->bauLabel->setText("Your Shipyard will be built!");
}
