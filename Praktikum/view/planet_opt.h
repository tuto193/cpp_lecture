#ifndef PLANET_OPT_H
#define PLANET_OPT_H


#include <QDialog>
#include "../datamodel/Planet.hpp"
#include "../datamodel/Gameworld.hpp"
#include "datamodel/Enums.hpp"
#include "../datamodel/Shipyard.hpp"

using namespace asteroids;

namespace Ui
{
class planet_opt;
}

class planet_opt : public QDialog
{
    Q_OBJECT

public:

    explicit planet_opt(asteroids::Planet::Ptr planet, QWidget *parent = 0);

    ~planet_opt();

    /**
     * @brief Updates planet's options
     */
    void update();

signals:
    /**
     * @brief Signaling to Build a building on a Planet
     * @param p Planet
     * @param type Building type
     */
    void buildBuilding(int p, BUILDINGTYPE type);

    /**
     * @brief Signaling to Transport Ore
     * @param p1 From
     * @param p2 To
     * @param amount Amount
     */
    void transportOre(int p1, int p2, int amount);

    /**
     * @brief Signaling to Transport Fighter
     * @param p1 From
     * @param p2 To
     */
    void transportShip(int p1, int p2);

    /**
     * @brief Change the Production of Shipyard
     * @param p Planet
     */
    void changeProduction(int p);

private slots:
    /// Triggered if close button is clicked
    void on_closePlOptBtn_clicked();

    /// Triggered if build mine is clicked
    void on_buildMineBtn_clicked();

    /// Triggered if build shipyard is clicked
    void on_buildShipyardBtn_clicked();

    /// Triggered if transport fighter is clicked
    void on_transportFighterBtn_clicked();

    /// Triggered if transport ore is clicked
    void on_transportOreBtn_clicked();

    /// Triggered if change building mode is clicked
    void on_changeModeBtn_clicked();


private:

    /**
     * @brief Initializes planet opt
     */
    void init();

    /// The planet opt window
    Ui::planet_opt *ui;

    /// Planet coordinates
    static int x;
    static int y;

    /// The planet
    asteroids::Planet::Ptr gamePlanet;
};

#endif // PLANET_OPT_H
