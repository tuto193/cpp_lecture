//
// Created by boubakr on 21.03.19.
//

#ifndef CPP18_ASTEROIDS_EDGE_HPP
#define CPP18_ASTEROIDS_EDGE_HPP

#include <QGraphicsItem>
#include <memory>

namespace asteroids {

	class GraphicPlanet;

	class Edge : public QGraphicsItem {
	public:
		using Ptr = std::shared_ptr<Edge>;
		using game_wPtr = std::shared_ptr<GraphicPlanet>;

		/**
		 * @brief Constructs an edge from start GraphicPlanet and destination GraphicPlanet
		 * @param from start GraphicPlanet
		 * @param to end GraphicPlanet
		 */
		Edge(game_wPtr from, game_wPtr to);

		/**
		 * @brief Defines the outer bounds of the item as a rectangle
		 * @return the outer bounds of the item as  rectangle QRectF
		 */
		QRectF boundingRect() const override;
		/**
		 * @brief Paints the contet of an item in local coordinates
		 * @param painter QPainter
		 * @param option QStyleOptionGraphicsItem
		 * @param widget QWidget
		 */
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
				QWidget *widget) override;
	private:
		/// Source planet
		game_wPtr source;
		/// Destination planet
		game_wPtr dest;
	};

}
#endif //CPP18_ASTEROIDS_EDGE_HPP