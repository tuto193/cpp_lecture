#include "GLWidget.hpp"
#include "../io/LevelParser.hpp"
#include "../io/TextureFactory.hpp"
#include <QMouseEvent>
#include <networking/NetworkInterface.hpp>
#include "view/MainWindow.hpp"

#include <QTimer>
#include <QThread>
#include <QDebug>
#include <SDL2/SDL.h>
#include <audio/GameSounds.hpp>
#include "math/Vector.hpp"

GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent),
      m_camera(),
      m_shootEnabled(true)
{
}


void GLWidget::setLevelFile(const std::string& file)
{
    m_levelFile = file;
}

void GLWidget::initializeGL()
{
#ifndef __APPLE__
    glewExperimental = GL_TRUE;
    glewInit();
#endif
    // Init OpenGL projection matrix
    glClearColor(0.0, 0.0, 0.0, 1.0);
    float ratio = width() * 1.0 / height();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width(), height());
    gluPerspective(45, ratio, 1, 10000);

    // Enter model view mode
    glMatrixMode(GL_MODELVIEW);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    // Setup two light sources
    float light0_position[4];
    float light0_ambient[4];
    float light0_diffuse[4];

    float light1_position[4];
    float light1_ambient[4];
    float light1_diffuse[4];

    light0_position[0] = 1.0f;
    light0_ambient[0] = 0.8f;
    light0_diffuse[0] = 0.8f;
    light0_position[1] = 1.0f;
    light0_ambient[1] = 0.8f;
    light0_diffuse[1] = 0.8f;
    light0_position[2] = 0.0f;
    light0_ambient[2] = 0.8f;
    light0_diffuse[2] = 0.8f;
    light0_position[3] = 1.0f;
    light0_ambient[3] = 0.1f;
    light0_diffuse[3] = 1.0f;

    light1_position[0] = 0.0f;
    light1_ambient[0] = 0.1f;
    light1_diffuse[0] = 0.5f;
    light1_position[1] = -1.0f;
    light1_ambient[1] = 0.1f;
    light1_diffuse[1] = 0.5f;
    light1_position[2] = 0.0f;
    light1_ambient[2] = 0.1f;
    light1_diffuse[2] = 0.5f;
    light1_position[3] = 1.0f;
    light1_ambient[3] = 1.0f;
    light1_diffuse[3] = 1.0f;

    // Light 1
    glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
    glEnable(GL_LIGHT0);

    // Light 2
    glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
    glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
    glEnable(GL_LIGHT1);

    // Enable lighting
    glEnable(GL_LIGHTING);

    // Enable z buffer and gouroud shading
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glShadeModel(GL_SMOOTH);


    // Set our OpenGL version.
    // SDL_GL_CONTEXT_CORE gives us only the newer version, deprecated functions are disabled
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // 3.2 is part of the modern versions of OpenGL,
    // but most video cards whould be able to run it
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    // Turn on double buffering with a 24bit Z buffer.
    // You may need to change this to 16 or 32 for your system
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // This makes our buffer swap syncronized with the monitor's vertical refresh
    SDL_GL_SetSwapInterval(1);

    // Load level
    LevelParser lp(m_levelFile, m_actor, m_enemy, m_skybox, m_asteroidField);

    // Setup physics
    m_physicsEngine = make_shared<PhysicsEngine>();

    // Initialize Camera with actor
    m_camera.initializeCamera(m_actor);

    // Add players' spacecrafts to physics engine
    m_physicsEngine->addActor(m_actor);
    m_physicsEngine->addEnemy(m_enemy);

    // Add asteroids to physics engine
    std::list<Asteroid::Ptr> asteroids;
    m_asteroidField->getAsteroids(asteroids);
    for (auto& asteroid : asteroids)
    {
        PhysicalObject::Ptr p = std::static_pointer_cast<PhysicalObject>(asteroid);
        m_physicsEngine->addDestroyable(p);
    }
}

void GLWidget::startSync()
{
// Create timer and run with given interval
    m_timer = new QTimer(nullptr);
    m_timer->start(static_cast<int>(1000 / 60.0));

    // Connect timer update to getting actor positions
    connect(
        m_timer, &QTimer::timeout,
        this, [this]()
        { emit signalActor(m_actor, m_actorShooting); },
        Qt::DirectConnection);

    // Then send this to the network interface
    connect(this, &GLWidget::signalActor, &NetworkInterface::instance(), &NetworkInterface::sendFighterUpdate);

    // Connect the sending fighter pos from the interface to the update method here
    connect(&NetworkInterface::instance(), &NetworkInterface::newFighterPos, this, &GLWidget::updateEnemy);

    // Connect to other player send lost the fight
    connect(&NetworkInterface::instance(), &NetworkInterface::signalOtherLost,
            this, [this]()
            {
                if (m_actor->getHp() > 0)
                {
                    NetworkInterface::instance().sendWonFight();
                }
            }
    );

    // Connect to other player send won the fight
    connect(&NetworkInterface::instance(), &NetworkInterface::signalOtherWon,
            this, [this]()
            {
                if (m_actor->getHp() == 0)
                {
                    NetworkInterface::instance().sendWonLostAcknowledgement();
                }
            }
    );

    // Connect to other player send won/lost was okay
    connect(&NetworkInterface::instance(), &NetworkInterface::signalWonLostAcknowledgement,
            this, [this]()
            {
                if (!m_endFightEmitted && m_actor->getHp() > 0 && m_enemy->getHp() == 0)
                {
                    NetworkInterface::instance().sendEndFight();

                    emit signalFightEnded();

                    // use this so we do not spam emits
                    m_endFightEmitted = true;
                }
            }
    );

    GameSounds::instance().play_3d_shipStart();
}

void GLWidget::stopSync()
{
    // Disconnects all slots / signals
    this->disconnect();

    m_timer->stop();
    delete m_timer;
    m_timer = nullptr;
}

void GLWidget::updateEnemy(const Vector3f& pos,
                           const Vector3f& xAxis,
                           const Vector3f& yAxis,
                           const Vector3f& zAxis,
                           const Quaternion& rot,
                           const Vector3f& dir,
                           bool isShooting)
{
    m_enemy->setPosition(pos);
    m_enemy->setAxis(xAxis, yAxis, zAxis);
    m_enemy->setRotation(rot);
    m_enemy->setDirection(dir);

    if (isShooting)
    {
        Bullet::Ptr bullet = make_shared<Bullet>(Bullet(m_enemy->getPosition(), m_enemy->getDirection()));
        m_physicsEngine->addBullet(bullet);

        GameSounds::instance().play_3d_shoot(m_enemy->getPosition().distance(m_actor->getPosition()));
    }
}

void GLWidget::paintGL()
{
    // Clear bg color and enable depth test (z-Buffer)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_camera.apply();

    glDepthMask(GL_FALSE);
    glDisable(GL_DEPTH_TEST);
    // Render stuff
    m_skybox->render(m_camera);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);

    // Render all physical objects
    m_physicsEngine->render();

    m_enemy->render();
    m_actor->render();

    glClear(GL_DEPTH_BUFFER_BIT);

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    drawHealthbars(p);
    drawCrosshair(p);
    drawEnemyPosHint(p);
}

void GLWidget::drawCrosshair(QPainter& p) const
{
    QPainterPath crosshair;
    // up
    crosshair.addRect(QRectF(width() / 2 - 5, height() / 2 - 30, 10, 20));
    // left
    crosshair.addRect(QRectF(width() / 2 - 30, height() / 2 - 5, 20, 10));
    // right
    crosshair.addRect(QRectF(width() / 2 + 10, height() / 2 - 5, 20, 10));
    // down
    crosshair.addRect(QRectF(width() / 2 - 5, height() / 2 + 10, 10, 20));
    p.drawPath(crosshair);
}

void GLWidget::drawHealthbars(QPainter& p) const
{
    //Actor Outline
    QPainterPath actor_hp_outline;
    actor_hp_outline.addRect(QRectF(10, 10, 200, 20));
    QPen actor_outline_pen(Qt::white, 1);
    p.setPen(actor_outline_pen);
    p.fillPath(actor_hp_outline, Qt::transparent);
    p.drawPath(actor_hp_outline);

    //Actor HP bar (1Hp = 20px)
    QPainterPath actor_hp_bar;
    actor_hp_bar.addRect(QRectF(10, 10, m_actor->getHp() * 20, 20));
    p.fillPath(actor_hp_bar, Qt::green);
    p.drawPath(actor_hp_bar);

    //Enemy Outline
    QPainterPath enemy_hp_outline;
    enemy_hp_outline.addRect(QRectF(width() - 210, 10, 200, 20));
    QPen enemy_pen(Qt::white, 1);
    p.setPen(enemy_pen);
    p.fillPath(enemy_hp_outline, Qt::transparent);
    p.drawPath(enemy_hp_outline);

    //Enemy HP bar (1Hp = 20px)
    QPainterPath enemy_hp_bar;
    enemy_hp_bar.addRect(QRectF(width() - 210, 10, m_enemy->getHp() * 20, 20));
    p.fillPath(enemy_hp_bar, Qt::red);
    p.drawPath(enemy_hp_bar);
}

void GLWidget::drawEnemyPosHint(QPainter &p) const {

    Vector3f enemyPos = m_enemy->getPosition();
    glm::vec4 vec = m_camera.worldToViewport(enemyPos, width(), height());

    if ((abs(vec.x) > 1) || (abs(vec.y) > 1))
    {
        glm::vec2 ind_pos = m_camera.viewportToScreen(glm::vec2(vec.x,vec.y), width(), height());
        QRectF rectangle(ind_pos.x,ind_pos.y,20,20);
        QRadialGradient gradient(ind_pos.x + 8, ind_pos.y + 8, 10);
        gradient.setColorAt(0, Qt::yellow);
        gradient.setColorAt(1, Qt::darkYellow);
        p.setBrush(gradient);
        p.drawEllipse(rectangle);
        p.setPen(QPen(Qt::black, 0));
    }
}
void GLWidget::step(map<Qt::Key, bool>& keyStates)
{
    m_actorShooting = false;
    // Get keyboard states and handle model movement
    m_physicsEngine->process();

    if (keyStates[Qt::Key_Up])
    {
        m_actor->rotate(Transformable::PITCH_RIGHT);
    }
    if (keyStates[Qt::Key_Down])
    {
        m_actor->rotate(Transformable::PITCH_LEFT);
    }
    if (keyStates[Qt::Key_Left])
    {
        m_actor->rotate(Transformable::ROLL_LEFT);
    }
    if (keyStates[Qt::Key_Right])
    {
        m_actor->rotate(Transformable::ROLL_RIGHT);
    }

    if (keyStates[Qt::Key_A])
    {
        m_actor->rotate(Transformable::YAW_LEFT);
    }
    if (keyStates[Qt::Key_D])
    {
        m_actor->rotate(Transformable::YAW_RIGHT);
    }

    if (keyStates[Qt::Key_W])
    {
        m_actor->move(Transformable::FORWARD);
        GameSounds::instance().play_3d_shipOn();
    }
    if (keyStates[Qt::Key_S])
    {
        m_actor->move(Transformable::BACKWARD);
        GameSounds::instance().play_3d_shipOn();
    }
    if (keyStates[Qt::Key_E])
    {
        m_actor->move(Transformable::STRAFE_LEFT);
        GameSounds::instance().play_3d_shipOn();
    }
    if (keyStates[Qt::Key_Q])
    {
        m_actor->move(Transformable::STRAFE_RIGHT);
        GameSounds::instance().play_3d_shipOn();
    }

    // Add a bullet to physics engine
    if (keyStates[Qt::Key_Space] && m_shootEnabled)
    {
        // Add delay to shots
        m_shootEnabled = false;
        m_actorShooting = true;
        Bullet::Ptr bullet = make_shared<Bullet>(Bullet(m_actor->getPosition(), m_actor->getDirection()));
        m_physicsEngine->addBullet(bullet);
        GameSounds::instance().play_3d_shoot();

        QTimer::singleShot(500, [this]()
        { m_shootEnabled = true; });
    }

    // Update camera to follow ship
    m_camera.followSpaceCraft();
    m_skybox->render(m_camera);

    // Trigger update, i.e., redraw via paintGL()
    this->update();
}


void GLWidget::resizeGL(int w, int h)
{
    float ratio = width() * 1.0 / height();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width(), height());
    gluPerspective(45, ratio, 1, 10000);

    glMatrixMode(GL_MODELVIEW);
}

GLWidget::~GLWidget()
{
    if (m_timer != nullptr)
    {
        m_timer->stop();
    }

    this->disconnect();

    delete m_timer;
}
