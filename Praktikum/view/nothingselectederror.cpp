#include "nothingselectederror.h"
#include "../build/ui_nothingselectederror.h"

nothingSelectedError::nothingSelectedError(QWidget *parent) :
    QDialog(parent), ui(new Ui::nothingSelectedError)
{
    ui->setupUi(this);
}

nothingSelectedError::~nothingSelectedError()
{
    delete ui;
}

void nothingSelectedError::on_okBtn_clicked()
{
    this->close();
}
