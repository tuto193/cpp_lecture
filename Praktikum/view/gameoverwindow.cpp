#include "gameoverwindow.h"
#include "ui_gameoverwindow.h"

gameOverWindow::gameOverWindow(QWidget *parent, bool lose) :
    QDialog(parent),
    ui(new Ui::gameOverWindow)
{
    ui->setupUi(this);

    if (!lose)
    {
        ui->win_lose_lbl->setText("You WIN!");
    }
}

void gameOverWindow::on_mainMenuBtn_clicked()
{
    this->close();
}

gameOverWindow::~gameOverWindow()
{
    delete ui;
}
