#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include <QMainWindow>
#include "connecttoserver.h"

namespace Ui {
class StartWindow;
}

class StartWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StartWindow(QWidget *parent = nullptr);
    ~StartWindow();
public slots:
    void on_connect_ToServer_clicked();
private:
    Ui::StartWindow *ui;
    ConnectToServer *connect_server;
};

#endif // STARTWINDOW_H
