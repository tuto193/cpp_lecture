#include "hostconnect.h"
#include "ui_hostconnect.h"
#include "connecttoserver.h"
#include "networking/Connection.hpp"

#include <QDebug>
#include "../build/ui_hostconnect.h"
#include "../build/ui_connecttoserver.h"
#include <QDesktopWidget>

hostConnect::hostConnect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::hostConnect)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());
    const QRect screen = QApplication::desktop()->screenGeometry();
    this->move( screen.center() - this->rect().center() );
    connect_server = new ConnectToServer;

    // Set pictures for buttons
    QPixmap pixmap("../img/connect.png");
    QIcon ButtonIcon(pixmap);
    ui->connect_ToServer->setIcon(ButtonIcon);
    ui->connect_ToServer->setIconSize(QSize(50,50));

    QPixmap pixmap2("../img/host.png");
    QIcon ButtonIcon2(pixmap2);
    ui->host->setIcon(ButtonIcon2);
    ui->host->setIconSize(QSize(50,50));

    ui->host_waiting_label->hide();
    ui->host_waiting_label_2->hide();
}

hostConnect::~hostConnect()
{
    delete ui;
}

void hostConnect::on_connect_ToServer_clicked()
{
    asteroids::Connection::instance().closeServer();
    connect_server->show();
    connect_server->activateWindow();
    connect_server->raise();

    ui->host_waiting_label->hide();
    ui->host_waiting_label_2->hide();

    this->hide();
}

void hostConnect::on_host_clicked()
{
    asteroids::Connection::instance().host();

    ui->host_waiting_label->show();
    ui->host_waiting_label_2->show();
}

void hostConnect::closeEvent(QCloseEvent *event)
{
    QDialog::closeEvent(event);
    asteroids::Connection::instance().closeServer();
    connect_server->close();

    ui->host_waiting_label->hide();
    ui->host_waiting_label_2->hide();
}

