/*
 *  MainWindow.cpp
 *
 *  Created on: Nov. 04 2018
 *      Author: Thomas Wiemann
 *
 *  Copyright (c) 2018 Thomas Wiemann.
 *  Restricted usage. Licensed for participants of the course "The C++ Programming Language" only.
 *  No unauthorized distribution.
 */

#include "MainWindow.hpp"
#include "../io/LevelParser.hpp"
#include "../io/TextureFactory.hpp"

#include <iostream>
#include <QTimer>
#include <QKeyEvent>
#include <networking/NetworkInterface.hpp>
#include "Submit_quit.h"
#include "submit_home.h"
#include "gameoverwindow.h"
#include "help.h"
#include "planet_opt.h"
#include "setting_win.h"
#include "../datamodel/Gameworld.hpp"
#include "networking/Connection.hpp"
#include "../audio/GameSounds.hpp"
#include <QDesktopWidget>
#include <QThread>

namespace asteroids
{

MainWindow::MainWindow(const std::string& file, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow()),
    m_levelFilePath(file),
    m_timer(new QTimer()),
    m_connectModal(this)
{
    ///Set up game world
    // load our sound files
    GameSounds::instance().init();
    GameSounds::instance().play_music();

    // Setup user interface
    ui->setupUi(this);

    //set standard size of window
    resizeWindow(1.0, m_windowWidth, m_windowHeight);

    // hide menu buttons
    ui->quit_Btn->hide();
    ui->settings_Btn->hide();
    ui->homeBtn->hide();

    // Create a timer object to trigger the main loop
    connect(m_timer.get(), SIGNAL(timeout()), this, SLOT(handleInput()));

    // Set title screen picture
    QPixmap pixmap("../img/titlescreen/1280x720_ot.png");
    QIcon ButtonIcon(pixmap);
    ui->titleScr_Btn->setIcon(ButtonIcon);
    ui->titleScr_Btn->setIconSize(pixmap.rect().size());

    // Set main menu picture
    QPixmap pixmapP("../img/mainmenuscreen/1280x720_ot.png");
    QIcon ButtonIconP(pixmapP);
    ui->titleScr_Btn_2->setIcon(ButtonIconP);
    ui->titleScr_Btn_2->setIconSize(pixmapP.rect().size());

    //Set menu button icons
    QPixmap pixmap1("../img/menu.png");
    QIcon ButtonIcon1(pixmap1);
    ui->menue_Btn->setIcon(ButtonIcon1);

    QPixmap pixmap2("../img/settings.png");
    QIcon ButtonIcon2(pixmap2);
    ui->settings_Btn->setIcon(ButtonIcon2);

    QPixmap pixmap3("../img/logout.png");
    QIcon ButtonIcon3(pixmap3);
    ui->quit_Btn->setIcon(ButtonIcon3);

    QPixmap pixmap4("../img/home.png");
    QIcon ButtonIcon4(pixmap4);
    ui->homeBtn->setIcon(ButtonIcon4);

    setMinimumSize(m_windowWidth, m_windowHeight);
    setMaximumSize(m_windowWidth, m_windowHeight);

    QObject::connect(
        &Connection::instance(), &Connection::signalNewData,
        &NetworkInterface::instance(), &NetworkInterface::readData,
        Qt::DirectConnection
    );
    connect(&Connection::instance(), &Connection::signalStartingGame, this, &MainWindow::slotStartGame);

    connect(&NetworkInterface::instance(), &NetworkInterface::signalStartFight,
            this, [this](const Vector3f& pos)
            {
                this->fight(pos, true);
            });

    // Activated if 3D fight ends
    connect(&NetworkInterface::instance(), &NetworkInterface::signalEndFight, this, &MainWindow::slotLoseFight);
    // Activated if 2D turn is skipped
    connect(this, &MainWindow::skipButton_clicked, &NetworkInterface::instance(), &NetworkInterface::skipTurn);

    // Connections to handle the output of enemy's actions to the infobox
    connect(&NetworkInterface::instance(), &NetworkInterface::turnSkipped, this, &MainWindow::slotTurnSkipped);
    connect(&NetworkInterface::instance(), &NetworkInterface::newPlanetUpdate, this, &MainWindow::slotNewPlanetUpdate);
    connect(&NetworkInterface::instance(), &NetworkInterface::signalTransportOre, this, &MainWindow::slotSignalTransportOre);
    connect(&NetworkInterface::instance(), &NetworkInterface::signalTransportFighter, this, &MainWindow::slotSignalTransportFighter);
    connect(&NetworkInterface::instance(), &NetworkInterface::signalChangeProduction, this, &MainWindow::slotChangeProduction);
    // connections for sending and receiving messages
    connect(this, &MainWindow::signalMsgSend, &NetworkInterface::instance(), &NetworkInterface::slotMsgSend);
    connect(&NetworkInterface::instance(), &NetworkInterface::signalReceiveMsg, this, &MainWindow::slotReceiveMsg);

    ui->wait_label->hide();
}

void MainWindow::handleInput()
{
    if (m_widget != nullptr && m_isFightRunning)
    {
        m_widget->step(m_keyStates);
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    // Save which key was pressed
    m_keyStates[(Qt::Key) event->key()] = true;
    if (event->key() == Qt::Key_Escape)
    {
        // switching from title screen to main menu
        if (ui->mainStack->currentIndex() == 1)
        {
            if (menueClicked)
            {
                hideMenuButtons();
            }
            else
            {
                on_menue_Btn_clicked();
            }
        }
        else
        {
            ui->mainStack->setCurrentIndex(0);
        }
        //ui->titleScr_Btn->setVisible(true);
    }
    else if (event->key() == Qt::Key_T)
    {
        if(ui->mainStack->currentIndex() == 0 || ui->mainStack->currentIndex() == 2)
        {
            if(!thomasModus)
            {
                setTitleAndMainMenuScreen(false);
                pathToBackground = "../img/thomas1x.png";
                thomasModus = true;
            }
        }
    }
    else if (event->key() == Qt::Key_N)
    {
        if(ui->mainStack->currentIndex() == 0 || ui->mainStack->currentIndex() == 2)
        {
            if(thomasModus)
            {
                setTitleAndMainMenuScreen(true);
                pathToBackground = "../img/planetmap.png";
                thomasModus = false;
            }
        }
    }

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        hideMenuButtons();
    }
}

void MainWindow::hideMenuButtons()
{
    if (menueClicked)
    {
        // Hide menu buttons
        ui->homeBtn->setVisible(false);
        ui->settings_Btn->setVisible(false);
        ui->quit_Btn->setVisible(false);
        menueClicked = !menueClicked;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    // Save which key was released
    m_keyStates[(Qt::Key) event->key()] = false;
}

MainWindow::~MainWindow()
{
    if(m_widget != nullptr)
    {
        delete m_widget;
        m_widget = nullptr;
    }
    if(planetMap != nullptr)
    {
        delete planetMap;
        planetMap = nullptr;
    }
    if(ui != nullptr)
    {
        delete ui;
        ui = nullptr;
    }
}

void MainWindow::slotStartFight()
{
    hideMenuButtons();
    ui->skipButton->hide();
    fight(Vector3f(0, 0, 0));
}

void MainWindow::endFight()
{
    m_timer->stop();

    disconnect(m_widget, &GLWidget::signalFightEnded, this, &MainWindow::endFight);
    m_widget->disconnect();

    // wait a small amount of time.. just because..
    QThread::msleep(100);

    ui->play_window->setCurrentWidget(planetMap);
    m_isFightRunning = false;

    ui->skipButton->show();
    ui->lineEdit->setDisabled(false);

    m_widget->stopSync();

    delete m_widget;
}

void MainWindow::slotWinFight()
{
    gameWorld->gotFightFinishInformation(true);
    endFight();
}

void MainWindow::slotLoseFight()
{
    gameWorld->gotFightFinishInformation(false);
    endFight();
}

void MainWindow::slotStartGame(bool isPlayerOne)
{
    qDebug() << "Starting game";
    if (!isPlayerOne)
    {
        ui->wait_label->show();
    }

//    gameWorld.reset();
    qDebug() << "Creating gameWorld";
    gameWorld = std::make_shared<Gameworld>(isPlayerOne);
    qDebug() << "Creating planetMap";
    planetMap = new MapWidget(ui->play_window, this->gameWorld.get());
    planetMap->setBackgroundBrush(QImage(pathToBackground));
    ui->play_window->addWidget(planetMap);
    ui->ore_lbl->setText(QString::number(gameWorld->getMe()->totalOre()));
    ui->ships_lbl->setText(QString::number(gameWorld->getMe()->totalFightships()));
    ui->trans_lbl->setText(QString::number(gameWorld->getMe()->totalTransporters()));

    updateLabels(gameWorld->getP1()->totalTransporters(),
                 gameWorld->getP1()->totalFightships(),
                 gameWorld->getP1()->totalOre());

    // Activated if 2D engage a fight
    connect(gameWorld.get(), &Gameworld::signalStartFight, this, &MainWindow::slotStartFight);
    // Activated if round count goes up
    connect(gameWorld.get(), &Gameworld::signalRoundCountUp, this, &MainWindow::on_signalRoundCountUp);

    //Update Labels on the top
    connect(gameWorld.get(), &Gameworld::signalChangePlayerOwningsLabels, this, &MainWindow::updateLabels);
    connect(gameWorld.get(), &Gameworld::signalNextTurn, this, &MainWindow::slotNextTurn);
    connect(gameWorld.get(), &Gameworld::signalWinGame, this, &MainWindow::slotWinGame);
    connect(gameWorld.get(), &Gameworld::signalLoseGame, this, &MainWindow::slotLoseGame);

    ui->round_Lbl->setText("1");
    ui->feed_txt->clear();

    ui->play_window->setCurrentWidget(planetMap);
    m_connectModal.close();
    ui->mainStack->setCurrentIndex(1);
}

void MainWindow::slotWinGame()
{
    endGame(false);
}

void MainWindow::slotLoseGame()
{
    endGame(true);
}

void MainWindow::endGame(bool lose)
{
    gameOverWindow gameOverWindow(nullptr, lose);
    gameOverWindow.setModal(true);
    gameOverWindow.exec();

    qDebug() << "disconnect from enemy";
    qDebug() << "end game gw:" << gameWorld.get();
    gameWorld->blockSignals(true);
    gameWorld->disconnect();
    planetMap->disconnect();

    Connection::instance().disconnectSocket();


    qDebug() << "entering main menu";
    ui->mainStack->setCurrentIndex(2);
    qDebug() << "delete planetmap" << planetMap;
    ui->play_window->removeWidget(planetMap);
//    delete planetMap;
//    planetMap = nullptr;
    qDebug() << "after planetmap" << planetMap;
    qDebug() << "delete gameworld" << gameWorld.get();
    gameWorld.reset();
}

void MainWindow::slotNextTurn(bool state)
{
    // if state is not ours, show wait_label on top
    if (state)
    {
        ui->wait_label->show();
    }

    planetMap->refresh();
}

void MainWindow::on_menue_Btn_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    if (!menueClicked)
    {
        ui->homeBtn->show();
        ui->settings_Btn->show();
        ui->quit_Btn->show();
        menueClicked = true;
    } else
    {
        hideMenuButtons();
    }
}

void MainWindow::on_quit_Btn_clicked()
{
    hideMenuButtons();
    Submit_quit d;
    d.setModal(true);
    d.exec();
}

void MainWindow::send()
{

}

void MainWindow::on_skipButton_clicked()
{
    if(gameWorld->getState())
    {
        GameSounds::instance().play_2d_menuClick();
        gameWorld->turnSkip();
        emit skipButton_clicked();
        hideMenuButtons();
    }
}


void MainWindow::on_titleScr_Btn_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    // switching from title screen to main menu
    ui->mainStack->setCurrentIndex(2);
}

void MainWindow::on_start_Btn_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    m_connectModal.setModal(true);
    m_connectModal.exec();
}

void MainWindow::on_exit_Btn_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    Submit_quit d;
    d.setModal(true);
    d.exec();
}

void MainWindow::on_settings_Btn_2_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    on_settings_Btn_clicked();
}

void asteroids::MainWindow::on_settings_Btn_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    hideMenuButtons();
    setting_win sw(this);
    sw.setModal(true);
    sw.exec();
}

void MainWindow::on_signalRoundCountUp()
{
    int prev_round = ui->round_Lbl->text().toInt();
    prev_round++;
    QString next_round = QString::number(prev_round);
    ui->round_Lbl->setText(next_round);
}

void MainWindow::fight(const Vector3f& myPos, bool signaled)
{
    ui->lineEdit->setDisabled(true);

    // Open Fight widget
    m_widget = new GLWidget(ui->play_window);
    m_widget->setObjectName("3d_game");
    ui->play_window->addWidget(m_widget);

    // Set level
    m_widget->setLevelFile(m_levelFilePath);

    m_timer->start(1000 / 60.0);

    connect(m_widget, &GLWidget::signalFightEnded, this, &MainWindow::slotWinFight);

    ui->play_window->setCurrentWidget(m_widget);
    m_widget->startSync();
    m_isFightRunning = true;

    // Refocus this widget to gather the key strokes..
    // May be refactored into the GLWidget
    this->setFocus();

    // Set this actor to send position
    m_widget->getActor()->setPosition(myPos);

    // hide waiting window on both sides if fighting
    ui->wait_label->hide();

    if (!signaled)
    {
        // We initiated the fight, so we dictate the position of the enemy
        NetworkInterface::instance().sendStartFight(m_widget->getEnemy()->getPosition());
    }
}

float MainWindow::getWindowWidth()
{
    return m_windowWidth;
}

void MainWindow::resizeWindow(float scaleResize, int windowWidth, int windowHeight)
{
    setMinimumSize(windowWidth, windowHeight);
    setMaximumSize(windowWidth, windowHeight);
    m_windowWidth = windowWidth;
    m_windowHeight = windowHeight;
    // Test change-size
    this->resize(windowWidth, windowHeight);
    ui->centralWidget->resize(windowWidth, windowHeight);
    ui->mainStack->resize(windowWidth, windowHeight);

    // Set menu button sizes
    ui->menue_Btn->setGeometry(0, 0, ui->menue_Btn->width() * scaleResize, ui->menue_Btn->height() * scaleResize);
    ui->homeBtn->setGeometry(0, ui->menue_Btn->y() + ui->menue_Btn->height(), ui->homeBtn->width() * scaleResize,
                             ui->homeBtn->height() * scaleResize);
    ui->settings_Btn->setGeometry(0, ui->homeBtn->y() + ui->homeBtn->height(), ui->settings_Btn->width() * scaleResize,
                                  ui->settings_Btn->height() * scaleResize);
    ui->quit_Btn->setGeometry(0, ui->settings_Btn->y() + ui->settings_Btn->height(),
                              ui->quit_Btn->width() * scaleResize, ui->quit_Btn->height() * scaleResize);

    // Set fight und skip button
    //ui->pushButton->setGeometry(ui->pushButton->x(), ui->pushButton->y(), ui->pushButton->width() * scaleResize, ui->pushButton->height() * scaleResize);

    
    

    int Btn_height = ui->skipButton->height() * scaleResize;
    int Btn_width = ui->skipButton->width() * scaleResize;
    int Btn_y = windowHeight - 19 - Btn_height - Btn_height;
    int Btn_x = windowWidth - 19 - Btn_width;

    ui->skipButton->setGeometry(Btn_x, Btn_y, Btn_width, Btn_height);

    // Set lineEdit size and position
    Btn_height = ui->lineEdit->height() * scaleResize;
    Btn_width = ui->lineEdit->width() * scaleResize;

    ui->lineEdit->setGeometry(ui->lineEdit->x(), windowHeight - 15 - Btn_height, Btn_width, Btn_height);

    // Set feed size and position
    Btn_height = ui->feed_txt->height() * scaleResize;
    Btn_width = ui->feed_txt->width() * scaleResize;
    ui->feed_txt->setGeometry(ui->feed_txt->x(), ui->lineEdit->y() - Btn_height, Btn_width, Btn_height);

    // Set label size and position
    Btn_height = ui->round_Lbl->height() * scaleResize;
    Btn_width = ui->round_Lbl->width() * scaleResize;
    ui->round_Lbl->setGeometry(ui->round_Lbl->x(), windowHeight - 20 - Btn_height, Btn_width, Btn_height);

    Btn_height = ui->lbl1->height() * scaleResize;
    Btn_width = ui->lbl1->width() * scaleResize;
    ui->lbl1->setGeometry(ui->lbl1->x(), windowHeight - 20 - ui->round_Lbl->height() - Btn_height + (10 * scaleResize), Btn_width, Btn_height);

    // Set openglwidget position and size
    int hoehe = ui->feed_txt->y() - 12 - ui->menue_Btn->height();
    ui->play_window->setGeometry(0, ui->menue_Btn->height(), windowWidth, hoehe);
    ui->wait_label->setGeometry(0, ui->menue_Btn->height(), windowWidth, hoehe);

    // Set info-label positions and sizes
    ui->label_3->setGeometry(ui->label_3->x(), ui->label_3->y(), ui->label_3->width() * scaleResize,
                             ui->label_3->height() * scaleResize);
    ui->trans_lbl->setGeometry(ui->trans_lbl->x(), ui->trans_lbl->y(), ui->trans_lbl->width() * scaleResize,
                               ui->trans_lbl->height() * scaleResize);
    ui->label->setGeometry(ui->label->x(), ui->label->y(), ui->label->width() * scaleResize,
                           ui->label->height() * scaleResize);
    ui->ships_lbl->setGeometry(ui->ships_lbl->x(), ui->ships_lbl->y(), ui->ships_lbl->width() * scaleResize,
                               ui->ships_lbl->height() * scaleResize);
    ui->label_2->setGeometry(ui->label_2->x(), ui->label_2->y(), ui->label_2->width() * scaleResize,
                             ui->label_2->height() * scaleResize);
    ui->ore_lbl->setGeometry(ui->ore_lbl->x(), ui->ore_lbl->y(), ui->ore_lbl->width() * scaleResize,
                             ui->ore_lbl->height() * scaleResize);

    int pixelWidth_Label =
        ui->label_3->width() + 8 + ui->trans_lbl->width() + 21 + ui->label->width() + 8 + ui->ships_lbl->width() + 21 +
        ui->label_2->width() + 8 + ui->ore_lbl->width();
    int eachSide = (windowWidth - pixelWidth_Label) / 2;

    // Center info labels
    ui->label_3->setGeometry(eachSide, ui->label_3->y(), ui->label_3->width(), ui->label_3->height());
    ui->trans_lbl->setGeometry(eachSide + ui->label_3->width() + 8, ui->trans_lbl->y(), ui->trans_lbl->width(),
                               ui->trans_lbl->height());
    ui->label->setGeometry(eachSide + ui->label_3->width() + 8 + ui->trans_lbl->width() + 21, ui->label->y(),
                           ui->label->width(), ui->label->height());
    ui->ships_lbl->setGeometry(
        eachSide + ui->label_3->width() + 8 + ui->trans_lbl->width() + 21 + ui->label->width() + 8, ui->ships_lbl->y(),
        ui->ships_lbl->width(), ui->ships_lbl->height());
    ui->label_2->setGeometry(
        eachSide + ui->label_3->width() + 8 + ui->trans_lbl->width() + 21 + ui->label->width() + 8 +
        ui->ships_lbl->width() + 21, ui->label_2->y(), ui->label_2->width(), ui->label_2->height());
    ui->ore_lbl->setGeometry(
        eachSide + ui->label_3->width() + 8 + ui->trans_lbl->width() + 21 + ui->label->width() + 8 +
        ui->ships_lbl->width() + 21 + ui->label_2->width() + 8, ui->ore_lbl->y(), ui->ore_lbl->width(),
        ui->ore_lbl->height());

    // Resize title screen
    ui->titleScr_Btn->resize(windowWidth, windowHeight);
    // Resize main menu screen
    ui->titleScr_Btn_2->resize(windowWidth, windowHeight);

    //set title and main menu
    setTitleAndMainMenuScreen(!thomasModus);

    // Calculate distance to window-bottom
    int distanceToBottom = (windowHeight / scaleResize) - ui->exit_Btn->y() - ui->exit_Btn->height();
    distanceToBottom = distanceToBottom * scaleResize;

    // Center main menu buttons
    Btn_width = ui->exit_Btn->width() * scaleResize;
    eachSide = (windowWidth - Btn_width) / 2;

    //resize main menu buttons
    ui->exit_Btn->setGeometry(eachSide, windowHeight - distanceToBottom - (ui->exit_Btn->height() * scaleResize), ui->exit_Btn->width() * scaleResize, ui->exit_Btn->height() * scaleResize);
    ui->help_Btn->setGeometry(eachSide, ui->exit_Btn->y() - 10 - (ui->help_Btn->height() * scaleResize), ui->help_Btn->width() * scaleResize, ui->help_Btn->height() * scaleResize);
    ui->settings_Btn_2->setGeometry(eachSide, ui->help_Btn->y() - 10 - (ui->settings_Btn_2->height() * scaleResize), ui->settings_Btn_2->width() * scaleResize, ui->settings_Btn_2->height() * scaleResize);
    ui->start_Btn->setGeometry(eachSide, ui->settings_Btn_2->y() - 10 - (ui->start_Btn->height() * scaleResize), ui->start_Btn->width() * scaleResize, ui->start_Btn->height() * scaleResize);

    //calculate size of font for main menu buttons and round labels
    double size = ((3*windowWidth*windowHeight) / 1152000) + 20.0;

    //set font of round info label
    QFont font = ui->lbl1->font();
    font.setPointSize(size);
    ui->lbl1->setFont(font);

    //set font of round counter label
    font = ui->round_Lbl->font();
    font.setPointSize(size);
    ui->round_Lbl->setFont(font);

    //set font of main menu button 'start_Btn'
    font = ui->start_Btn->font();
    font.setPointSize(size);
    ui->settings_Btn_2->setFont(font);

    //set font of main menu button 'settings_Btn_2'
    font = ui->settings_Btn_2->font();
    font.setPointSize(size);
    ui->settings_Btn_2->setFont(font);

    //set font of main menu button 'help_Btn'
    font = ui->help_Btn->font();
    font.setPointSize(size);
    ui->help_Btn->setFont(font);

    //set font of main menu button 'exit_Btn'
    font = ui->exit_Btn->font();
    font.setPointSize(size);
    ui->exit_Btn->setFont(font);

    //calculate size of font for DropDownMenu buttons
    size = ((4*windowWidth*windowHeight) / 1152000) + 8.0;

    //set font of DropDownMenu button 'menue_Btn'
    font = ui->menue_Btn->font();
    font.setPointSize(size);
    ui->menue_Btn->setFont(font);

    //set font of DropDownMenu button 'homeBtn'
    font = ui->homeBtn->font();
    font.setPointSize(size);
    ui->homeBtn->setFont(font);

    //set font of DropDownMenu button 'settings_Btn'
    font = ui->settings_Btn->font();
    font.setPointSize(size);
    ui->settings_Btn->setFont(font);

    //set font of DropDownMenu button 'quit_Btn'
    font = ui->quit_Btn->font();
    font.setPointSize(size);
    ui->quit_Btn->setFont(font);

    //calculate size of font for info labels
    size = ((6*windowWidth*windowHeight) / 1152000) + 11.0;

    //set font of info label 'trans_lbl'
    font = ui->trans_lbl->font();
    font.setPointSize(size);
    ui->trans_lbl->setFont(font);

    //set font of info label 'ships_lbl'
    font = ui->ships_lbl->font();
    font.setPointSize(size);
    ui->ships_lbl->setFont(font);

    //set font of info label 'ore_lbl'
    font = ui->ore_lbl->font();
    font.setPointSize(size);
    ui->ore_lbl->setFont(font);

    //calculate size of font right buttons
    size = ((5*windowWidth*windowHeight) / 1152000) + 9.0;

    //set font of button 'skipButton'
    font = ui->skipButton->font();
    font.setPointSize(size);
    ui->skipButton->setFont(font);
}

QString MainWindow::getPath(bool title, bool ot)
{
    std::string path = "../img/";
    if (title)
    {
        path = path + "titlescreen/";
    } else
    {
        path = path + "mainmenuscreen/";
    }
    path = path + std::to_string(int(m_windowWidth)) + "x" + std::to_string(int(m_windowHeight));
    if (ot)
    {
        path = path + "_ot";
    }
    path = path + ".png";
    return QString::fromStdString(path);
}

void MainWindow::setTitleAndMainMenuScreen(bool ot)
{
    QPixmap pixmapNewTitlescreen(getPath(true, ot));
    QIcon titlescreenIcon(pixmapNewTitlescreen);
    ui->titleScr_Btn->setIcon(titlescreenIcon);
    ui->titleScr_Btn->setIconSize(pixmapNewTitlescreen.rect().size());
    QPixmap pixmapNewMenu(getPath(false, ot));
    QIcon mainMenuIcon(pixmapNewMenu);
    ui->titleScr_Btn_2->setIcon(mainMenuIcon);
    ui->titleScr_Btn_2->setIconSize(pixmapNewMenu.rect().size());
}

void MainWindow::on_help_Btn_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    Help h(this);
    h.setModal(true);
    h.exec();
}

void MainWindow::on_homeBtn_clicked()
{
    GameSounds::instance().play_2d_menuClick();
    hideMenuButtons();
    Submit_Home sh(this);
    sh.setModal(true);
    sh.exec();
    //ui->mainStack->setCurrentIndex(1);
}

void MainWindow::setCurrentIndex (int index)
{
    ui->mainStack->setCurrentIndex(index);
}

void MainWindow::updateLabels(int trans, int ship, int ore)
{
    ui->trans_lbl->setText(QString::number(trans));
    ui->ships_lbl->setText(QString::number(ship));
    ui->ore_lbl->setText(QString::number(ore));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    on_exit_Btn_clicked();

    // preventing window from being closed if pressed no in exit window
    event->ignore();
}

void MainWindow::slotTurnSkipped()
{
    ui->feed_txt->append(QString::fromStdString("Enemy skipped turn!"));
    itsYourTurn();
}

void MainWindow::slotNewPlanetUpdate(int p, BUILDINGTYPE type)
{
    std::string pName = gameWorld->getPlanetNameById(p);

    switch(type)
    {
        case MINE:
            ui->feed_txt->append(QString::fromStdString("Enemy built Mine on " + pName + "!"));
            break;
        case SHIPYARD:
            ui->feed_txt->append(QString::fromStdString("Enemy built Shipyard on " + pName + "!"));
            break;
        default:
            qDebug() << "Buildingtype not found!";
            break;
    }
    itsYourTurn();
}

void MainWindow::slotSignalTransportOre(int p1, int p2, int amount)
{
    std::string p1Name = gameWorld->getPlanetNameById(p1);
    std::string p2Name = gameWorld->getPlanetNameById(p2);

    ui->feed_txt->append(QString::fromStdString("Enemy transported " + std::to_string(amount) +
                                                " Ore from " + p1Name + " to " + p2Name + "!"));
    itsYourTurn();

}

void MainWindow::slotSignalTransportFighter(int p1, int p2)
{
    std::string p1Name = gameWorld->getPlanetNameById(p1);
    std::string p2Name = gameWorld->getPlanetNameById(p2);

    ui->feed_txt->append(QString::fromStdString("Enemy transported a Fighter from " + p1Name + " to " + p2Name + "!"));
    itsYourTurn();

}

void MainWindow::slotChangeProduction(int p)
{
    std::string pName = gameWorld->getPlanetNameById(p);

    ui->feed_txt->append(QString::fromStdString("Enemy changed Production Mode for " + pName + "'s Shipyard!"));
    itsYourTurn();

}

void MainWindow::itsYourTurn()
{
    ui->feed_txt->append(QString::fromStdString("It's your turn!"));
    ui->wait_label->hide();
}

void MainWindow::on_lineEdit_returnPressed()
{
    QString message = ui->lineEdit->text();
    if (message != "")
    {
        QString you = "You: " + message;
        ui->feed_txt->append("<p style='color: green'>" + you + "</p>");
        ui->lineEdit->clear();
        emit signalMsgSend(message);
    }
}

void MainWindow::slotReceiveMsg(QString message)
{
    QString enemy = "Enemy: " + message;
    ui->feed_txt->append("<p style='color: red'>" + enemy + "</p>");
}

} // namespace asteroids
