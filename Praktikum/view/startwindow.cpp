#include "startwindow.h"
#include "ui_startwindow.h"
#include <QTimer>
#include <QDesktopWidget>
#include "ui_connecttoserver.h"
StartWindow::StartWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StartWindow)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());
    const QRect screen = QApplication::desktop()->screenGeometry();
    this->move( screen.center() - this->rect().center() );
    connect_server = new ConnectToServer;
    connect(ui->connect_ToServer,SIGNAL(clicked()),
            connect_server,SLOT(on_connect_ToServer_clicked()));
}

StartWindow::~StartWindow()
{
    delete ui;
}
void StartWindow::on_connect_ToServer_clicked()
{
    connect_server->show();
    this->hide();
}
