#ifndef GAMESOUNDS_HPP
#define GAMESOUNDS_HPP

#include <map>

#include <QSoundEffect>
#include <memory>

namespace asteroids
{

/**
 * @brief Class for playing the sound effects of the game
 *
 */
class GameSounds : public QSoundEffect
{
    Q_OBJECT
public:

    /**
     * @brief get an instance of the game sounds manager
     *
     * @return GameSounds& the static instance of this Object
     */
    static GameSounds& instance()
    {
        static GameSounds i;
        return i;
    }

    /**
     * @brief Set the Source Folder attribute of our sounds
     * where we will poll our files from
     *
     * @param fPath the path to the folder with the sound files
     * @return true     when the folder wasn't set before
     * @return false    when the folder was already set
     */
    bool setSourceFolder( QString& fPath );

    /**
     * @brief Load all the sound files into a list
     *
     */
    void init();

    /**
     * @brief Play sound for hovering over a menu button
     *
     */
    void play_2d_menuHover();

    /**
     * @brief Play sound for clicking a menu button
     *
     */
    void play_2d_menuClick();

    /**
     * @brief Play sound for when hovering over a planet
     *
     */
    void play_2d_planetHover();

    /**
     * @brief Play sound for when clicking a planet
     *
     */
    void play_2d_planetClick();

    /**
     * @brief Play sound for when a building is being built
     *
     */
    void play_2d_buildingBuild();

    /**
     * @brief Play sound for when a Building is being RE-Built
     *
     */
    void play_2d_buildingRebuild();

    /**
     * @brief Play sound for the end of a round
     * (when all buildings produce)
     *
     */
    void play_produce();

    /**
     * @brief Play sound for when shooting with the spacecraft
     *
     */
    void play_3d_shoot(float distance = -1);

    /**
     * @brief Play sound for when getting hit by a bullet
     *
     */
    void play_3d_getShot(float distance = -1);

    /**
     * @brief Play sound for when this player dies
     *
     */
    void play_3d_death();

    /**
     * @brief Play sound for when this player kills the enemy
     *
     */
    void play_3d_kill(float distance = -1);

    /**
     * @brief Get the Source Folder attribute
     *
     * @return std::string  the path to the source folder
     */
    QString getSourceFolder();

    /**
     * @brief change the volume of all the sounds that we have saved in our map
     *
     * @param v   a number from 0.0(muted) to 1.0(loudest)
     */
    void adjustVolume( float v );

    /**
     * @brief Get the master volume of these sounds
     *
     * @return float    the master volume
     */
    float getVolume() { return t_master_volume; }

    /**
     * @brief Sound for when the ship is just running
     *
     * @param distance  the distance of the given ship, if it's not the player's
     */
    void play_3d_shipOn( float distance = -1 );

    /**
     * @brief Sound for when the ship accelerates
     *
     * @param distance  the distance of the ship, it it's not the player's
     */
    void play_3d_shipAccelerate( float distance = -1 );

    /**
     * @brief Sound for one the ship is just started (beginning of a fight)
     *
     */
    void play_3d_shipStart();

    /**
     * @brief Sound for when the ship is moving
     *
     * @param distance
     */
    void play_3d_shipFast( float distance = -1 );

    /**
     * @brief Get the Sound requested from the sound map
     *
     * @param name the name of the sound
     * @return QSoundEffect*  the pointer to the sound effect
     */
    QSoundEffect* getSound( std::string name ) { return t_sounds[name]; }

    /**
     * @brief start playing the game music on an infinite loop
     *
     */
    void play_music();

    /**
     * @brief Destroy the Game Sounds object
     *
     */
    ~GameSounds();

private:

    /**
     * Maximum distance to which the sound is lowered
     */
    const float DISTANCE_MAX = 2000;

    /**
     * Minimum distance to which the sound is louder
     */
    const float DISTANCE_MIN = 500;

    /**
     * @brief Construct a new Game Sounds object
     * ... Private because it is a singleton
     *
     */
    explicit GameSounds( QWidget* parent = nullptr) {};

    /**
     * @brief Play the given sound and also adjust the volume based on the given distance
     */
    void playSound(float distance, QSoundEffect* toPlay) const;

    float t_master_volume = 1.f;

    /// the source folder of the files
    QString sourceFolder;


    /// the map with the sounds that are loaded
    std::map<std::string, QSoundEffect*> t_sounds;
};

} // namespace asteroids

#endif