#include "GameSounds.hpp"
#include <iostream>
#include <QDebug>
#include <cmath>

namespace asteroids
{

bool GameSounds::setSourceFolder(QString& fPath)
{
    if (sourceFolder.isEmpty())
    {
        sourceFolder = fPath;
        return true;
    }
    return false;
}

void GameSounds::init()
{
    std::map<std::string, std::shared_ptr<QSoundEffect>> sound_map;
    QString sf = QString("../sound_assets/");
    if (!setSourceFolder(sf))
    {
        throw std::runtime_error("Wasn't able to initialise Source Folder");
    }

    // The QSoundEffects for our sounds
    t_sounds["menuHover"] = new QSoundEffect();
    t_sounds["menuClick"] = new QSoundEffect();
    t_sounds["planetHover"] = new QSoundEffect();
    t_sounds["planetClick"] = new QSoundEffect();
    t_sounds["buildingBuild"] = new QSoundEffect();
    t_sounds["buildingRebuild"] = new QSoundEffect();
    t_sounds["produce"] = new QSoundEffect();
    t_sounds["shoot"] = new QSoundEffect();
    t_sounds["getShot"] = new QSoundEffect();
    t_sounds["death"] = new QSoundEffect();
    t_sounds["kill"] = new QSoundEffect();
    t_sounds["shipStart"] = new QSoundEffect();
    t_sounds["shipOn"] = new QSoundEffect();
    t_sounds["shipAccelerate"] = new QSoundEffect();
    t_sounds["shipFast"] = new QSoundEffect();
    t_sounds["WrongRiteTheme"] = new QSoundEffect();

    // Their respective paths
    t_sounds["WrongRiteTheme"]->setSource(QUrl::fromLocalFile( sf + "Wrong Rite Theme.wav" ) );
    t_sounds["menuHover"]->setSource(QUrl::fromLocalFile(sf + "menu_hover.wav"));
    t_sounds["menuClick"]->setSource(QUrl::fromLocalFile(sf + "menu_click.wav"));
    t_sounds["planetHover"]->setSource(QUrl::fromLocalFile(sf + "planet_hover.wav"));
    t_sounds["planetClick"]->setSource(QUrl::fromLocalFile(sf + "planet_click.wav"));
    t_sounds["buildingBuild"]->setSource(QUrl::fromLocalFile(sf + "building_build.wav"));
    t_sounds["buildingRebuild"]->setSource(QUrl::fromLocalFile(sf + "building_rebuild.wav"));
    t_sounds["produce"]->setSource(QUrl::fromLocalFile(sf + "round_start.wav"));
    t_sounds["shoot"]->setSource(QUrl::fromLocalFile(sf + "bullet_shoot.wav"));
    t_sounds["getShot"]->setSource(QUrl::fromLocalFile(sf + "player_shot.wav"));
    t_sounds["death"]->setSource(QUrl::fromLocalFile(sf + "player_die.wav"));
    t_sounds["kill"]->setSource(QUrl::fromLocalFile(sf + "enemy_kill.wav"));
    t_sounds["shipOn"]->setSource(QUrl::fromLocalFile(sf + "ship_on.wav" ) );
    t_sounds["shipAccelerate"]->setSource(QUrl::fromLocalFile(sf + "ship_accelerate.wav" ) );
    t_sounds["shipStart"]->setSource(QUrl::fromLocalFile(sf + "ship_start.wav" ) );
    t_sounds["shipFast"]->setSource(QUrl::fromLocalFile(sf + "ship_fast.wav" ) );

    // "Fast" and "On" should be infinitely looped

    adjustVolume(1.0);

    // Our soundtrack shall loop ininitely
    t_sounds["WrongRiteTheme"]->setLoopCount(QSoundEffect::Infinite);
}

void GameSounds::play_music()
{
    QSoundEffect* toPlay = t_sounds["WrongRiteTheme"];
    if( !toPlay->isPlaying() )
    {
        toPlay->play();
    }
}

void GameSounds::play_2d_menuHover()
{
    //menu_hover.wav
    QSoundEffect* toPlay = t_sounds["menuHover"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_2d_menuClick()
{
    //menu_click.wav
    QSoundEffect* toPlay = t_sounds["menuClick"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_2d_planetHover()
{
    //planet_hover.wav
    QSoundEffect* toPlay = t_sounds["planetHover"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_2d_planetClick()
{
    //planet_click.wav
    QSoundEffect* toPlay = t_sounds["planetClick"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_2d_buildingBuild()
{
    //building_build.wav
    QSoundEffect* toPlay = t_sounds["buildingBuild"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_2d_buildingRebuild()
{
    //building_rebuild.wav
    QSoundEffect* toPlay = t_sounds["buildingRebuild"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_produce()
{
    //round_start.wav
    QSoundEffect* toPlay = t_sounds["produce"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_3d_shoot(float distance)
{
    //bullet_shoot.wav
    QSoundEffect* toPlay = t_sounds["shoot"];

    playSound(distance, toPlay);
}

void GameSounds::play_3d_getShot(float distance)
{
    //player_shot.wav
    QSoundEffect* toPlay = t_sounds["getShot"];

    playSound(distance, toPlay);
}

void GameSounds::play_3d_death()
{
    //player_die.wav
    QSoundEffect* toPlay = t_sounds["death"];
    if (!toPlay->isPlaying())
    {
        toPlay->play();
    }
}

void GameSounds::play_3d_kill(float distance)
{
    //enemy_kill.wav
    QSoundEffect* toPlay = t_sounds["kill"];

    playSound(distance, toPlay);
}

QString GameSounds::getSourceFolder()
{
    return sourceFolder;
}

void GameSounds::adjustVolume(float v)
{
    this->t_master_volume = v;
    t_sounds["menuHover"]->setVolume        (t_master_volume * 0.6f);
    t_sounds["menuClick"]->setVolume        (t_master_volume * 0.8f);
    t_sounds["planetHover"]->setVolume      (t_master_volume);
    t_sounds["planetClick"]->setVolume      (t_master_volume);
    t_sounds["buildingBuild"]->setVolume    (t_master_volume);
    t_sounds["buildingRebuild"]->setVolume  (t_master_volume);
    t_sounds["produce"]->setVolume          (t_master_volume);
    t_sounds["shoot"]->setVolume            (t_master_volume * 0.68f);
    t_sounds["getShot"]->setVolume          (t_master_volume * 0.4f);
    t_sounds["death"]->setVolume            (t_master_volume);
    t_sounds["kill"]->setVolume             (t_master_volume);
    t_sounds["shipOn"]->setVolume           (t_master_volume * 0.2f);
    t_sounds["shipAccelerate"]->setVolume   (t_master_volume * 0.3f);
    t_sounds["shipStart"]->setVolume        (t_master_volume * 0.6f);
    t_sounds["shipFast"]->setVolume         (t_master_volume * 0.5f);
    t_sounds["WrongRiteTheme"]->setVolume   ( t_master_volume * 0.4f );
}

void GameSounds::playSound(float distance, QSoundEffect* toPlay) const
{
    qreal oldVol = toPlay->volume();

    if (distance > -1.f)
    {
        float adjust;
        if (distance <= DISTANCE_MIN)
        {
            adjust = 1.f;
        }
        else if (distance >= DISTANCE_MAX)
        {
            adjust = 0.2f;
        }
        else
        {
            adjust = 1 - ((distance - DISTANCE_MIN) / (DISTANCE_MAX - DISTANCE_MIN));
        }

        toPlay->setVolume(t_master_volume * adjust);
    }

    if (!toPlay->isPlaying())
    {
        toPlay->play();

        QSoundEffect::connect(toPlay, &QSoundEffect::playingChanged, this, [toPlay, oldVol]()
        {
            if (!toPlay->isPlaying())
            {
                toPlay->setVolume(oldVol);

                toPlay->disconnect();
            }
        });
    }
}

GameSounds::~GameSounds()
{
    for (auto& entry : t_sounds)
    {
        entry.second->stop();
    }

    t_sounds.clear();
}

void GameSounds::play_3d_shipOn( float distance )
{
    QSoundEffect* toPlay = t_sounds["shipOn"];
    playSound( distance, toPlay );
}

void GameSounds::play_3d_shipAccelerate( float distance )
{

    QSoundEffect* toPlay = t_sounds["shipAccelerate"];
    playSound( distance, toPlay );
}

void GameSounds::play_3d_shipStart()
{
    QSoundEffect* toPlay = t_sounds["shipStart"];
    if( !toPlay->isPlaying() )
    {
        toPlay->play();
    }
}

void GameSounds::play_3d_shipFast( float distance )
{
    QSoundEffect* toPlay = t_sounds["shipFast"];
    playSound( distance, toPlay );
}


}