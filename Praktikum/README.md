# ASTEROIDS

## Dependencies

* QT 5.xx
* glm
* boost

## How to build

* Make build folder: `mkdir build && cd build`
* Run cmake `cmake ..`
* Run build command: `make -j4`

## How to run

* Start the game with command `./asteroids ../models/level.xml
* Click "Start Game" and choose if host or join.


### Known bugs:

* Bullets not always synced on both sides. Missing bullets
* Crash sometimes after game was finished
* Crash if new game is started after one is finished
* Multiple fights can cause rendering bugs


COPYRIGHT NOTICE:
"Wrong Rite Theme" by Pierre Bondoerffer ( Twitter Handle @pbondoer)