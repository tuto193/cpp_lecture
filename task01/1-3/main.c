#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_randist.h>

int main(void) {
    double s = 1.0;
    double l = -5.0;
    double u = 5.0;
    double st = 0.1;
    double result;
    int i;

    for( i = 0; l < u; i++ ) {
        result = gsl_ran_gaussian_pdf( l, s );
        printf( "%0.5f %0.5f \n",l, result );
        l+=st;
    }
    // Make sure to free memory after use
    return 0;
}
