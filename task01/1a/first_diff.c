#include <stdio.h>
#include "first_diff.h"

/**
 * @brief prints a row (int), where two files differ. 0 if they are equal to the very end
 * and the corresponding rows
 * 
 * @param f1 the first filename
 * @param f2 the second filename
 */
int compare( char *f1, char *f2 ) {
    FILE *file1 = fopen( f1, "r" );
    FILE *file2 = fopen( f2, "r" );
    int row = 1;
    int c1 = 0;
    int c2 = 0;
    int i = 0;
    int j;
    char rowString1[100];
    char rowString2[100];
    int terminate = 0;
    do{
        c1 = fgetc(file1);
        c2 = fgetc(file2);
        rowString1[i] = c1;
        rowString2[i] = c2;
        if( c1 != c2 ) {
            terminate = 1;
        }
        if( c1 == '\n' ) {
            // Terminate the loop just when we're ready with the row
            if( terminate == 1 ) {
                j = 0;
                // Make the files \0 terminated
                rowString1[i+1] = '\0';
                rowString2[i+1] = '\0';
                break;
            } else {
                row++;
                // Reset our strings if the row wasn't wrong
                for( j = 0; j < i + 1; j++ ) {
                    rowString1[j] = 0;
                    rowString2[j] = 0;
                }
                //reset i
                i = -1;
            }
        }
        i++;
    } while( c1 != EOF && c2 != EOF );

    // Make sure to always close all opened files
    fclose( file1 );
    fclose( file2 );

    if( terminate == 1 ) {
        printf( "The files differ on row: %d.\nFile1's row is:\n%s\nWhile File2's row is:\n%s\n", row, rowString1, rowString2 );
    } else {
        printf( "The files are perfectly equal." );
    }
}

/**
 * @brief outputs corresponging error messaegs. Returns 0 (false) if c != 3
 * 
 * @param c the amount of arguments given by the user on the command line
 * @return int the validity of the ammount of commands. 0 if c != 3
 */
int controlArgs( int c ) {
    if( c == 1 ) {
        printf("No arguments were given. No Files to be compared.\n");
    } else if( c == 2 ) {
        printf( "Error: need another filename to compare!\n" );
    } else if( c == 3 ) {
        return 1;
    } else {
        printf( "Error: too many arguments given!\n" );
    }
    return 0;
}

int main( int argc, char *args[] ) {
    if( controlArgs( argc ) != 0 ) {
        compare( args[1], args[2] );
    }
    return 0;
}