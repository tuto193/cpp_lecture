#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {
    char c = 0;
    // Pointer to our string
    char *pString = (char*) malloc(101*(sizeof(char))); //Strings of max. 100 chars
    // Pointer to the beginning of our string
    char *pStringBegin = pString;

    // ------- INPUT --------------------------------------------////
    printf( "Enter a character or just press ENTER to END: \n" );
    do{
        /*
        Get a char, put it into our aray (from the beginning) and move our pointer one position up
        */
        c = getchar();
        *pString = c;
        pString++;
        if( c != '\n' ) {
            getchar();
        }
    } while( c != '\n' );

    //Strings in C need to be \0 terminated
    *pString = '\0';

    // ------- OUTPUT -------------------------------------////
    printf( "You entered the following string(s):\n" );
    while( pStringBegin < pString ) {
        c = *pStringBegin;
        putchar( c );
        if( c == '\0' ) {
            putchar( '\n' );
            break;
        } else if( c == ' ' ) {
            putchar( '\n' );
        }
        pStringBegin++;
    }
    return 0;
}