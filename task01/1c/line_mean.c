/*
* line_mean.c
*
*  Created on: 24.10.2018
*      Author: aluebsen
*/

#include <stdio.h>

/**
 * @brief returns an int row, with the ammunt of rows that a File has. -1, if the file doesn't consist
 * of only integers separated by spaces and EOLs
 * 
 * @param numbers the file
 * @return int amount of rows that the file has
 */
int testinput(FILE *numbers){

    char c = 0;
    int rows = 0;
    int valid = 1;

    do {
        c = fgetc(numbers);
        if(!(c == ' ' || (c >= 48 && c <= 57) || c == '\n' || c == -1)){
            valid = 0;
        }
        if(c == '\n')
            rows++;

    } while(c != EOF);
    if(valid)
        return rows;
    else
        return -1;
}

int main( int argc, char *argv[] ){
    if( argc != 2 ) {
        printf( "Error. You must give exactly one argument: the name of the file you want to check!\n" );
        return 1;
    }
    FILE *numbers = fopen( argv[1], "r");
    char c = 0;
    int cint = 0;
    int tempc = 0;
    int sum = 0;
    float mean = 0;
    int rows = 0;

    rows = testinput(numbers);

    rewind(numbers);
    float values[5][rows];

    int i = 0;
    int j = 0;
    int k, l;
    if(rows > 0) {
        do {
            c = fgetc(numbers);
            if(c >= 48 && c <= 57) {
                tempc = c - 48;
                cint = tempc + (cint*10);
            }
            else if(c == ' ') {
                sum = sum + cint;
                values[i][j] = (float) cint;
                i++;
                cint = 0;
                tempc= 0;
            }
            else if (c == '\n'){
                    values[i][j] = (float) cint;
                    i++;
                    sum = sum + cint;
                    float sumfloat = (float) sum;
                    mean = sumfloat / 4.0;
                    values[i][j] = mean;
                    i = 0;
                    j++;
                    cint = 0;
                    tempc= 0;
                    sum = 0;
                    mean = 0;
            }

            

        } while (c != EOF);
        fclose(numbers);
        
        for(k = 0; k < rows; k++){
            for(l = 0; l < 5; l++){
                printf("%.2f ", values[l][k]);
            }
            printf("\n");
        }
    } else {
        printf("Invalid input!\n");
    }
    return 0;
}