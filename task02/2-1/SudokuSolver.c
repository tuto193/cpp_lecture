#ifndef SDTIO_H
#define STDIO_H
#include <stdio.h>
#endif
#include "SudokuFunctions.h"


int main( void ) {
    /*  Board from the task     */
    int sudoku[9][9] ={{0,0,0,0,0,8,0,3,0},
                       {0,3,0,5,0,0,4,7,1},
                       {2,0,0,1,0,0,6,9,0},
                       {5,0,0,0,0,2,1,0,0},
                       {1,2,4,0,0,0,9,6,3},
                       {0,0,6,4,0,0,0,0,2},
                       {0,8,9,0,0,5,0,0,7},
                       {3,5,2,0,0,9,0,4,0},
                       {0,1,0,3,0,0,0,0,0}};
    printf( "Starting board:\n" );
    print_sudoku(sudoku);
    printf("\n");
    printf( "Solved board:\n" );
    if( solve_sudoku(0, 0, sudoku ) ) {
        print_sudoku( sudoku );
    } else {
        printf( "No solution was found :( \n");
    }
    return 0;
}