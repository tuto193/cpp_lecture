#ifndef STDIO_H
#define STDIO_H
#include <stdio.h>
#endif

/*
 * Resets an array
 */
void resetCount(int arr[10]){
	int i;
	for(i = 0; i < 10; i++)
		arr[i] = 0;
}

/*
 * @brief Checks whether this sudoku-move is valid
 * 
 * @param z The number to be put into the sudoku
 * @param i The row of the field the number is to be put into
 * @param j The column of the field the number is to be put into
 * @param sudoku The sudoku field
 * 
 * @return 0 if the move is invalid, 1 if the move is valid
 */

int is_valid(int z, int i, int j, int sudoku[9][9]){
	int yfield = j / 3;		/*y-coordinate of the 3x3 cuadrant*/
	int xfield = i / 3;		/*x-coordinate of the 3x3 cuadrant*/
	int loop = 0;			/*loop-variable to count*/
	int countX[10];			/*array to count which numbers are in the same row*/
	int countY[10];			/*array to count which numbers are in the same column*/
	int countSquare[10];
    int currField;
    int inFieldX, inFieldY;
	resetCount(countX);
	resetCount(countY);
	resetCount(countSquare);
	for(loop = 0; loop < 9; loop++){
		/*
		 * Checks every field in the same row
		 */
		currField = sudoku[loop][i];
		if((currField != z) && (countY[currField] == 0  ) ) {
            if( currField != 0 ) {
			    countY[currField] = 1;
            }
		} else {
			return 0;
		}
		/*
		 * Checks every field in the same column
		 */
		currField = sudoku[j][loop];
		if((currField != z) && (countX[currField] == 0 )) {
            if( currField != 0 ) {
			    countX[currField] = 1;
            }
		} else {
			return 0;
       	}
		/*
		 * Checks every field in the same 3x3 square
		 */
		inFieldX = loop % 3 + (3 * xfield);
		inFieldY = loop / 3 + (3 * yfield);
		currField = sudoku[inFieldY][inFieldX];
		if((currField != z) && (countSquare[currField] == 0 )){
            if( currField != 0 ) {
			    countSquare[currField] = 1;
            }
		} else {
			return 0;
		}
	}
	return 1;
}

/*
 * @brief Prints a two-dimensional 9x9 array as a sudoku
 * @param sudoku The sudoku to be printed
 */

void print_sudoku(int sudoku[9][9]) {
	int i, j;
	for(i = 0; i < 9; i++){
		for(j = 0; j < 9; j++){
			printf(" %d ",sudoku[i][j]);
			if(j != 8 && j % 3 == 2){
				printf("|");
            }
		}
        printf( "\n" );
		if(i != 8 && i % 3 == 2) {
			printf("---------+---------+---------\n");
		}
	}
    printf("\n");
}

/*
 * @brief Recursively solves a given sudoku starting at a given field
 * @param i The row of the given field
 * @param j The column of the given field
 * @param sudoku The sudoku to be solved
 * @return 1 if the sudoku can be solved starting from this field, 0 otherwise
 */
int solve_sudoku(int i, int j, int sudoku[9][9]){
    /*
    i = columns
    j = rows
    */
	int x;
    if( sudoku[j][i] != 0 ) {
        if( i == 8 && j == 8 ) {
            /*  Solution was found  */
            return 1;
        }else if( i == 8 ) {
            j++;
            i = 0;
        } else {
            i++;
        }
        return solve_sudoku( i, j, sudoku );
    }
    /*  Try to find a value that fits in that spot  */
    for( x = 1; x < 10; x++ ) {
        if( is_valid( x, i, j, sudoku ) ) {
            sudoku[j][i] = x; 
            if( solve_sudoku( i, j, sudoku ) ) {
                return 1;
            } else {
                sudoku[j][i] = 0;
            }
        }
    }
    /*  No solution was found   */
    return 0;
}